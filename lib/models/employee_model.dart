class EmployeeModel {
  EmployeeModel({
    required this.id,
    required this.email,
    required this.emailVerifiedAt,
    required this.role,
    required this.createdAt,
    required this.updatedAt,
    required this.employeeId,
    required this.userId,
    required this.firstName,
    required this.lastName,
    required this.nik,
    required this.nip,
    required this.placeOfBirth,
    required this.dateOfBirth,
    required this.gender,
    required this.address,
    required this.phone,
    required this.religion,
    required this.position,
    required this.workshiftId,
    required this.image,
  });

  int id;
  String email;
  dynamic emailVerifiedAt;
  String role;
  DateTime createdAt;
  DateTime updatedAt;
  int employeeId;
  int userId;
  String firstName;
  String lastName;
  String nik;
  String nip;
  String placeOfBirth;
  DateTime dateOfBirth;
  String gender;
  String address;
  String phone;
  String religion;
  String position;
  int workshiftId;
  dynamic image;

  factory EmployeeModel.fromJson(Map<String, dynamic> json) => EmployeeModel(
        id: json["id"],
        email: json["email"],
        emailVerifiedAt: json["email_verified_at"],
        role: json["role"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        employeeId: json["employee_id"],
        userId: json["user_id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        nik: json["nik"],
        nip: json["nip"],
        placeOfBirth: json["place_of_birth"],
        dateOfBirth: DateTime.parse(json["date_of_birth"]),
        gender: json["gender"],
        address: json["address"],
        phone: json["phone"],
        religion: json["religion"],
        position: json["position"],
        workshiftId: json["workshift_id"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "email_verified_at": emailVerifiedAt,
        "role": role,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "employee_id": employeeId,
        "user_id": userId,
        "first_name": firstName,
        "last_name": lastName,
        "nik": nik,
        "nip": nip,
        "place_of_birth": placeOfBirth,
        "date_of_birth":
            "${dateOfBirth.year.toString().padLeft(4, '0')}-${dateOfBirth.month.toString().padLeft(2, '0')}-${dateOfBirth.day.toString().padLeft(2, '0')}",
        "gender": gender,
        "address": address,
        "phone": phone,
        "religion": religion,
        "position": position,
        "workshift_id": workshiftId,
        "image": image,
      };
}

class EmployeeNotWorkModel {
  EmployeeNotWorkModel({
    required this.employeeId,
    required this.firstName,
    required this.lastName,
    required this.workshiftId,
    required this.position,
    required this.image,
  });

  int employeeId;
  String firstName;
  String lastName;
  String position;
  int workshiftId;
  dynamic image;

  factory EmployeeNotWorkModel.fromJson(Map<String, dynamic> json) =>
      EmployeeNotWorkModel(
        employeeId: json["employee_id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        position: json["position"],
        workshiftId: json["workshift_id"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "employee_id": employeeId,
        "first_name": firstName,
        "last_name": lastName,
        "position": position,
        "workshift_id": workshiftId,
        "image": image,
      };
}

class UserStatusModel {
  UserStatusModel({
    required this.email,
    required this.passwordOld,
  });

  String email;
  String passwordOld;

  factory UserStatusModel.fromJson(Map<String, dynamic> json) =>
      UserStatusModel(
        email: json["email"],
        passwordOld: json["password_old"],
      );

  Map<String, dynamic> toJson() => {
        "email": email,
        "password_old": passwordOld,
      };
}
