class CompanyModel {
  CompanyModel({
    required this.companyId,
    required this.companyName,
    required this.companyAddress,
    required this.latitude,
    required this.longitude,
    required this.distance,
    required this.createdAt,
    required this.updatedAt,
  });

  int companyId;
  String companyName;
  String companyAddress;
  String latitude;
  String longitude;
  String distance;
  DateTime createdAt;
  DateTime updatedAt;

  factory CompanyModel.fromJson(Map<String, dynamic> json) => CompanyModel(
        companyId: json["company_id"],
        companyName: json["company_name"],
        companyAddress: json["company_address"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        distance: json["distance"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "company_id": companyId,
        "company_name": companyName,
        "company_address": companyAddress,
        "latitude": latitude,
        "longitude": longitude,
        "distance": distance,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
