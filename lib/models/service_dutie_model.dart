class ServiceDutieModel {
  ServiceDutieModel({
    required this.serviceDutyId,
    required this.employeeId,
    required this.dutyFromDate,
    required this.dutyToDate,
    required this.dutyDate,
    required this.purpose,
    required this.time,
    required this.place,
    required this.abandonedJob,
    required this.attachment,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
  });

  int serviceDutyId;
  int employeeId;
  DateTime dutyFromDate;
  DateTime dutyToDate;
  DateTime dutyDate;
  String purpose;
  DateTime time;
  String place;
  String abandonedJob;
  String attachment;
  String status;
  DateTime createdAt;
  DateTime updatedAt;

  factory ServiceDutieModel.fromJson(Map<String, dynamic> json) =>
      ServiceDutieModel(
        serviceDutyId: json["service_duty_id"],
        employeeId: json["employee_id"],
        dutyFromDate: DateTime.parse(json["duty_from_date"]),
        dutyToDate: DateTime.parse(json["duty_to_date"]),
        dutyDate: DateTime.parse(json["duty_date"]),
        purpose: json["purpose"],
        time: DateTime.parse(json["time"]),
        place: json["place"],
        abandonedJob: json["abandoned_job"],
        attachment: json["attachment"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "service_duty_id": serviceDutyId,
        "employee_id": employeeId,
        "duty_from_date": dutyFromDate.toIso8601String(),
        "duty_to_date": dutyToDate.toIso8601String(),
        "duty_date":
            "${dutyDate.year.toString().padLeft(4, '0')}-${dutyDate.month.toString().padLeft(2, '0')}-${dutyDate.day.toString().padLeft(2, '0')}",
        "purpose": purpose,
        "time": time.toIso8601String(),
        "place": place,
        "abandoned_job": abandonedJob,
        "attachment": attachment,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}

class ServiceDutieAllModel {
  ServiceDutieAllModel({
    required this.firstName,
    required this.lastName,
    required this.image,
    required this.serviceDutyId,
    required this.dutyFromDate,
    required this.dutyToDate,
    required this.purpose,
    required this.time,
    required this.place,
    required this.abandonedJob,
    required this.attachment,
    required this.status,
  });

  String firstName;
  String lastName;
  String image;
  int serviceDutyId;
  DateTime dutyFromDate;
  DateTime dutyToDate;
  String purpose;
  DateTime time;
  String place;
  String abandonedJob;
  String attachment;
  String status;

  factory ServiceDutieAllModel.fromJson(Map<String, dynamic> json) =>
      ServiceDutieAllModel(
        firstName: json["first_name"],
        lastName: json["last_name"],
        image: json["image"],
        serviceDutyId: json["service_duty_id"],
        dutyFromDate: DateTime.parse(json["duty_from_date"]),
        dutyToDate: DateTime.parse(json["duty_to_date"]),
        purpose: json["purpose"],
        time: DateTime.parse(json["time"]),
        place: json["place"],
        abandonedJob: json["abandoned_job"],
        attachment: json["attachment"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
        "image": image,
        "service_duty_id": serviceDutyId,
        "duty_from_date": dutyFromDate.toIso8601String(),
        "duty_to_date": dutyToDate.toIso8601String(),
        "purpose": purpose,
        "time": time.toIso8601String(),
        "place": place,
        "abandoned_job": abandonedJob,
        "attachment": attachment,
        "status": status,
      };
}
