import 'package:flutter/material.dart';

class NewsModel {
  NewsModel({
    required this.id,
    required this.title,
    required this.description,
    required this.author,
    required this.image,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String title;
  String description;
  String author;
  String image;
  DateTime createdAt;
  DateTime updatedAt;

  factory NewsModel.fromJson(Map<String, dynamic> json) => NewsModel(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        author: json["author"],
        image: json["image"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "author": author,
        "image": image,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
