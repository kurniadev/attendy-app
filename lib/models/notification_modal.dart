class NotificationModel {
  NotificationModel({
    required this.id,
    required this.employeeId,
    required this.title,
    required this.message,
    required this.isRead,
    required this.type,
    required this.image,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  int employeeId;
  String title;
  String message;
  String isRead;
  String type;
  String image;
  DateTime createdAt;
  DateTime updatedAt;

  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      NotificationModel(
        id: json["id"],
        employeeId: json["employee_id"],
        title: json["title"],
        message: json["message"],
        isRead: json["is_read"],
        type: json["type"],
        image: json["image"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "employee_id": employeeId,
        "title": title,
        "message": message,
        "is_read": isRead,
        "type": type,
        "image": image,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}

class CountNotificationModel {
  CountNotificationModel({
    required this.count,
  });

  int count;

  factory CountNotificationModel.fromJson(Map<String, dynamic> json) =>
      CountNotificationModel(
        count: json["count"],
      );

  Map<String, dynamic> toJson() => {
        "count": count,
      };
}
