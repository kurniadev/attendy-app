class LeavePermitModel {
  LeavePermitModel({
    required this.leavePermitId,
    required this.employeeId,
    required this.leaveTypeId,
    required this.leaveFromDate,
    required this.leaveToDate,
    required this.leaveDate,
    required this.proof,
    required this.abandonedJob,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.leaveTypeName,
  });

  int leavePermitId;
  int employeeId;
  int leaveTypeId;
  DateTime leaveFromDate;
  DateTime leaveToDate;
  DateTime leaveDate;
  String proof;
  String abandonedJob;
  String status;
  DateTime createdAt;
  DateTime updatedAt;
  String leaveTypeName;

  factory LeavePermitModel.fromJson(Map<String, dynamic> json) =>
      LeavePermitModel(
        leavePermitId: json["leave_permit_id"],
        employeeId: json["employee_id"],
        leaveTypeId: json["leave_type_id"],
        leaveFromDate: DateTime.parse(json["leave_from_date"]),
        leaveToDate: DateTime.parse(json["leave_to_date"]),
        leaveDate: DateTime.parse(json["leave_date"]),
        proof: json["proof"],
        abandonedJob: json["abandoned_job"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        leaveTypeName: json["leave_type_name"],
      );

  Map<String, dynamic> toJson() => {
        "leave_permit_id": leavePermitId,
        "employee_id": employeeId,
        "leave_type_id": leaveTypeId,
        "leave_from_date": leaveFromDate.toIso8601String(),
        "leave_to_date": leaveToDate.toIso8601String(),
        "leave_date":
            "${leaveDate.year.toString().padLeft(4, '0')}-${leaveDate.month.toString().padLeft(2, '0')}-${leaveDate.day.toString().padLeft(2, '0')}",
        "proof": proof,
        "abandoned_job": abandonedJob,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "leave_type_name": leaveTypeName,
      };
}

class LeaveAllModel {
  LeaveAllModel({
    required this.leavePermitId,
    required this.leaveFromDate,
    required this.leaveToDate,
    required this.leaveDate,
    required this.proof,
    required this.abandonedJob,
    required this.status,
    required this.leaveTypeName,
    required this.firstName,
    required this.lastName,
    required this.image,
  });

  int leavePermitId;
  DateTime leaveFromDate;
  DateTime leaveToDate;
  DateTime leaveDate;
  String proof;
  String abandonedJob;
  String status;
  String leaveTypeName;
  String firstName;
  String lastName;
  String image;

  factory LeaveAllModel.fromJson(Map<String, dynamic> json) => LeaveAllModel(
        leavePermitId: json["leave_permit_id"],
        leaveFromDate: DateTime.parse(json["leave_from_date"]),
        leaveToDate: DateTime.parse(json["leave_to_date"]),
        leaveDate: DateTime.parse(json["leave_date"]),
        proof: json["proof"],
        abandonedJob: json["abandoned_job"],
        status: json["status"],
        leaveTypeName: json["leave_type_name"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "leave_permit_id": leavePermitId,
        "leave_from_date": leaveFromDate.toIso8601String(),
        "leave_to_date": leaveToDate.toIso8601String(),
        "leave_date":
            "${leaveDate.year.toString().padLeft(4, '0')}-${leaveDate.month.toString().padLeft(2, '0')}-${leaveDate.day.toString().padLeft(2, '0')}",
        "proof": proof,
        "abandoned_job": abandonedJob,
        "status": status,
        "leave_type_name": leaveTypeName,
        "first_name": firstName,
        "last_name": lastName,
        "image": image,
      };
}

class LeaveTypeModel {
  LeaveTypeModel({
    required this.leaveTypeId,
    required this.leaveTypeName,
    required this.createdAt,
    required this.updatedAt,
  });

  int leaveTypeId;
  String leaveTypeName;
  DateTime createdAt;
  DateTime updatedAt;

  factory LeaveTypeModel.fromJson(Map<String, dynamic> json) => LeaveTypeModel(
        leaveTypeId: json["leave_type_id"],
        leaveTypeName: json["leave_type_name"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "leave_type_id": leaveTypeId,
        "leave_type_name": leaveTypeName,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
