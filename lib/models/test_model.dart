List<Datum> stockModelFromJson(List data) => List<Datum>.from(
      data.map(
        (x) => Datum.fromJson(x),
      ),
    );

class Datum {
  Datum({
    required this.domain,
    required this.measure,
  });

  String domain;
  double measure;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        domain: json["domain"],
        measure: json["measure"]?.toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "domain": domain,
        "measure": measure,
      };
}
