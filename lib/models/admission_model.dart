class AdmissionModel {
  AdmissionModel({
    required this.data,
  });

  Data data;

  factory AdmissionModel.fromJson(Map<String, dynamic> json) => AdmissionModel(
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data.toJson(),
      };
}

class Data {
  Data({
    required this.employeeId,
    required this.userId,
    required this.firstName,
    required this.lastName,
    required this.nik,
    required this.nip,
    required this.placeOfBirth,
    required this.dateOfBirth,
    required this.gender,
    required this.address,
    required this.religion,
    required this.phone,
    required this.position,
    required this.image,
    required this.status,
  });

  int employeeId;
  int userId;
  String firstName;
  String lastName;
  String nik;
  String nip;
  String placeOfBirth;
  String dateOfBirth;
  String gender;
  String address;
  String religion;
  String phone;
  String position;
  dynamic image;
  String status;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        employeeId: json["employee_id"],
        userId: json["user_id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        nik: json["nik"],
        nip: json["nip"],
        placeOfBirth: json["place_of_birth"],
        dateOfBirth: json["date_of_birth"],
        gender: json["gender"],
        address: json["address"],
        religion: json["religion"],
        phone: json["phone"],
        position: json["position"],
        image: json["image"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "employee_id": employeeId,
        "user_id": userId,
        "first_name": firstName,
        "last_name": lastName,
        "nik": nik,
        "nip": nip,
        "place_of_birth": placeOfBirth,
        "date_of_birth": dateOfBirth,
        "gender": gender,
        "address": address,
        "religion": religion,
        "phone": phone,
        "position": position,
        "image": image,
        "status": status,
      };
}

class AdmissionImage {
  AdmissionImage({
    required this.image,
  });

  String image;

  factory AdmissionImage.fromJson(Map<String, dynamic> json) => AdmissionImage(
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "image": image,
      };
}
