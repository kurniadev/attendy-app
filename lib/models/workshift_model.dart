class WorkshiftModel {
  WorkshiftModel({
    required this.workshiftId,
    required this.shiftName,
    required this.startTime,
    required this.endTime,
    required this.maxArrival,
    required this.diff,
  });

  int workshiftId;
  String shiftName;
  String startTime;
  String endTime;
  String maxArrival;
  String diff;

  factory WorkshiftModel.fromJson(Map<String, dynamic> json) => WorkshiftModel(
        workshiftId: json["workshift_id"],
        shiftName: json["shift_name"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        maxArrival: json["max_arrival"],
        diff: json["diff"],
      );

  Map<String, dynamic> toJson() => {
        "workshift_id": workshiftId,
        "shift_name": shiftName,
        "start_time": startTime,
        "end_time": endTime,
        "max_arrival": maxArrival,
        "diff": diff,
      };
}

class WorkshiftAdminModel {
  WorkshiftAdminModel({
    required this.workshiftId,
    required this.shiftName,
    required this.startTime,
    required this.endTime,
    required this.maxArrival,
  });

  int workshiftId;
  String shiftName;
  String startTime;
  String endTime;
  String maxArrival;

  factory WorkshiftAdminModel.fromJson(Map<String, dynamic> json) =>
      WorkshiftAdminModel(
        workshiftId: json["workshift_id"],
        shiftName: json["shift_name"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        maxArrival: json["max_arrival"],
      );

  Map<String, dynamic> toJson() => {
        "workshift_id": workshiftId,
        "shift_name": shiftName,
        "start_time": startTime,
        "end_time": endTime,
        "max_arrival": maxArrival,
      };
}
