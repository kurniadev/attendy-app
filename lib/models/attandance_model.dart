class HistoryAllAttandanceModel {
  HistoryAllAttandanceModel({
    required this.id,
    required this.employeeId,
    required this.date,
    required this.checkIn,
    required this.checkOut,
    required this.imageCheckIn,
    required this.imageCheckOut,
    required this.longitude,
    required this.latitude,
    required this.deviceInfo,
    required this.islate,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  int employeeId;
  String date;
  DateTime checkIn;
  dynamic checkOut;
  String imageCheckIn;
  dynamic imageCheckOut;
  String longitude;
  String latitude;
  String deviceInfo;
  String islate;
  dynamic status;
  DateTime createdAt;
  DateTime updatedAt;

  factory HistoryAllAttandanceModel.fromJson(Map<String, dynamic> json) =>
      HistoryAllAttandanceModel(
        id: json["id"],
        employeeId: json["employee_id"],
        date: json["date"],
        checkIn: DateTime.parse(json["check_in"]),
        checkOut: json["check_out"],
        imageCheckIn: json["image_check_in"],
        imageCheckOut: json["image_check_out"],
        longitude: json["longitude"],
        latitude: json["latitude"],
        deviceInfo: json["device_info"],
        islate: json["islate"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "employee_id": employeeId,
        "date": date,
        "check_in": checkIn.toIso8601String(),
        "check_out": checkOut,
        "image_check_in": imageCheckIn,
        "image_check_out": imageCheckOut,
        "longitude": longitude,
        "latitude": latitude,
        "device_info": deviceInfo,
        "islate": islate,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}

class AttandanceEmployeeModel {
  AttandanceEmployeeModel({
    required this.id,
    required this.employeeId,
    required this.date,
    required this.checkIn,
    this.checkOut,
    required this.imageCheckIn,
    this.imageCheckOut,
    required this.longitude,
    required this.latitude,
    required this.deviceInfo,
    required this.islate,
    this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.userId,
    required this.firstName,
    required this.lastName,
    required this.nik,
    required this.nip,
    required this.placeOfBirth,
    required this.dateOfBirth,
    required this.gender,
    required this.address,
    required this.phone,
    required this.religion,
    required this.position,
    required this.workshiftId,
    required this.image,
  });

  int id;
  int employeeId;
  String date;
  DateTime checkIn;
  dynamic checkOut;
  String imageCheckIn;
  dynamic imageCheckOut;
  String longitude;
  String latitude;
  String deviceInfo;
  String islate;
  dynamic status;
  DateTime createdAt;
  DateTime updatedAt;
  int userId;
  String firstName;
  String lastName;
  String nik;
  String nip;
  String placeOfBirth;
  DateTime dateOfBirth;
  String gender;
  String address;
  String phone;
  String religion;
  String position;
  int workshiftId;
  String image;

  factory AttandanceEmployeeModel.fromJson(Map<String, dynamic> json) =>
      AttandanceEmployeeModel(
        id: json["id"],
        employeeId: json["employee_id"],
        date: json["date"],
        checkIn: DateTime.parse(json["check_in"]),
        checkOut: json["check_out"],
        imageCheckIn: json["image_check_in"],
        imageCheckOut: json["image_check_out"],
        longitude: json["longitude"],
        latitude: json["latitude"],
        deviceInfo: json["device_info"],
        islate: json["islate"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        userId: json["user_id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        nik: json["nik"],
        nip: json["nip"],
        placeOfBirth: json["place_of_birth"],
        dateOfBirth: DateTime.parse(json["date_of_birth"]),
        gender: json["gender"],
        address: json["address"],
        phone: json["phone"],
        religion: json["religion"],
        position: json["position"],
        workshiftId: json["workshift_id"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "employee_id": employeeId,
        "date": date,
        "check_in": checkIn.toIso8601String(),
        "check_out": checkOut,
        "image_check_in": imageCheckIn,
        "image_check_out": imageCheckOut,
        "longitude": longitude,
        "latitude": latitude,
        "device_info": deviceInfo,
        "islate": islate,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "user_id": userId,
        "first_name": firstName,
        "last_name": lastName,
        "nik": nik,
        "nip": nip,
        "place_of_birth": placeOfBirth,
        "date_of_birth":
            "${dateOfBirth.year.toString().padLeft(4, '0')}-${dateOfBirth.month.toString().padLeft(2, '0')}-${dateOfBirth.day.toString().padLeft(2, '0')}",
        "gender": gender,
        "address": address,
        "phone": phone,
        "religion": religion,
        "position": position,
        "workshift_id": workshiftId,
        "image": image,
      };
}

class IsWorkAttandanceModel {
  IsWorkAttandanceModel({
    required this.isWork,
  });

  String isWork;

  factory IsWorkAttandanceModel.fromJson(Map<String, dynamic> json) =>
      IsWorkAttandanceModel(
        isWork: json["isWork"],
      );

  Map<String, dynamic> toJson() => {
        "isWork": isWork,
      };
}

class CountAttandanceModel {
  CountAttandanceModel({
    required this.tepatWaktu,
    required this.terlambat,
    required this.tidakHadir,
  });

  int tepatWaktu;
  int terlambat;
  int tidakHadir;

  factory CountAttandanceModel.fromJson(Map<String, dynamic> json) =>
      CountAttandanceModel(
        tepatWaktu: json["tepat_waktu"],
        terlambat: json["terlambat"],
        tidakHadir: json["tidak_hadir"],
      );

  Map<String, dynamic> toJson() => {
        "tepat_waktu": tepatWaktu,
        "terlambat": terlambat,
        "tidak_hadir": tidakHadir,
      };
}

class ChartPresentModel {
  ChartPresentModel({
    required this.tepatWaktu,
    required this.terlambat,
    required this.tidakHadir,
    required this.countOntime,
    required this.countLate,
    required this.countNotprecent,
  });

  int tepatWaktu;
  int terlambat;
  int tidakHadir;
  int countOntime;
  int countLate;
  int countNotprecent;

  factory ChartPresentModel.fromJson(Map<String, dynamic> json) =>
      ChartPresentModel(
        tepatWaktu: json["tepat_waktu"],
        terlambat: json["terlambat"],
        tidakHadir: json["tidak_hadir"],
        countOntime: json["count_ontime"],
        countLate: json["count_late"],
        countNotprecent: json["count_notprecent"],
      );

  Map<String, dynamic> toJson() => {
        "tepat_waktu": tepatWaktu,
        "terlambat": terlambat,
        "tidak_hadir": tidakHadir,
        "count_ontime": countOntime,
        "count_late": countLate,
        "count_notprecent": countNotprecent,
      };
}
