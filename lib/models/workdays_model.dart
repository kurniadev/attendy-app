class WorkdaysModel {
  WorkdaysModel({
    required this.workdaysId,
    required this.dayName,
    required this.shiftName,
    required this.startTime,
    required this.endTime,
    required this.maxArrival,
  });

  int workdaysId;
  String dayName;
  String shiftName;
  String startTime;
  String endTime;
  String maxArrival;

  factory WorkdaysModel.fromJson(Map<String, dynamic> json) => WorkdaysModel(
        workdaysId: json["workdays_id"],
        dayName: json["day_name"],
        shiftName: json["shift_name"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        maxArrival: json["max_arrival"],
      );

  Map<String, dynamic> toJson() => {
        "workdays_id": workdaysId,
        "day_name": dayName,
        "shift_name": shiftName,
        "start_time": startTime,
        "end_time": endTime,
        "max_arrival": maxArrival,
      };
}

class DaysModel {
  DaysModel({
    required this.dayId,
    required this.dayName,
  });

  int dayId;
  String dayName;

  factory DaysModel.fromJson(Map<String, dynamic> json) => DaysModel(
        dayId: json["day_id"],
        dayName: json["day_name"],
      );

  Map<String, dynamic> toJson() => {
        "day_id": dayId,
        "day_name": dayName,
      };
}
