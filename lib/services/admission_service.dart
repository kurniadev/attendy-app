import 'dart:convert';
import 'dart:ffi';

import 'package:attandy_app/models/admission_model.dart';
import 'package:attandy_app/services/auth_service.dart';

import '../constants.dart';
import '../models/api_response_model.dart';
import 'package:http/http.dart' as http;

String apiGetAdmission = '$baseURL/admission/getemployee';

Future<ApiResponse> getAdmission() async {
  ApiResponse apiResponse = ApiResponse();
  try {
    String token = await getToken();
    final response = await http.get(Uri.parse(apiGetAdmission), headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Authorization': 'Bearer $token'
    });

    switch (response.statusCode) {
      case 200:
        print(response.body);
        apiResponse.data = AdmissionModel.fromJson(jsonDecode(response.body));
        break;
      case 401:
        apiResponse.error = unauthorized;
        break;
      default:
        apiResponse.error = somethingWhentWrong;
        break;
    }
  } catch (e) {
    apiResponse.error = serverError;
  }
  return apiResponse;
}

String apiCreateAdmission = '$baseURL/admission/addemployee';
Future<ApiResponse> createAdmission({
  required String firstName,
  required String lastName,
  required String nik,
  // required String nip,
  required String placeOfBirth,
  required String dateOfBirth,
  required String gender,
  required String religion,
  required String address,
  required String position,
  required String phone,
  required String workshiftId,
  required String? image,
}) async {
  ApiResponse apiResponse = ApiResponse();
  try {
    String token = await getToken();
    final response = await http.post(Uri.parse(apiCreateAdmission), headers: {
      'Accept': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Authorization': 'Bearer $token'
    }, body: {
      'first_name': firstName,
      'last_name': lastName,
      'nik': nik,
      // 'nip': nip,
      'place_of_birth': placeOfBirth,
      'date_of_birth': dateOfBirth,
      'gender': gender,
      'religion': religion,
      'address': address,
      'position': position,
      'workshift_id': workshiftId,
      'phone': phone,
      'image': image,
    });
    print(response.statusCode);
    switch (response.statusCode) {
      case 200:
        print(response.body);
        apiResponse.data =
            AdmissionImage.fromJson(jsonDecode(response.body)['data']);
        break;
      case 401:
        apiResponse.error = unauthorized;
        break;
      case 400:
        print(response.body);
        break;
      default:
        String? somethingWentWrong;
        apiResponse.error = somethingWentWrong;
        break;
    }
  } catch (e) {
    apiResponse.error = serverError;
  }
  return apiResponse;
}

Future<ApiResponse> updateDataEmployee({
  required String firstName,
  required String lastName,
  required String address,
  required String phone,
}) async {
  ApiResponse apiResponse = ApiResponse();
  try {
    String token = await getToken();
    final response = await http
        .post(Uri.parse('$baseURL/admission/updatedataemployee'), headers: {
      'Accept': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Authorization': 'Bearer $token'
    }, body: {
      'first_name': firstName,
      'last_name': lastName,
      'address': address,
      'phone': phone,
    });
    switch (response.statusCode) {
      case 200:
        apiResponse.data = jsonDecode(response.body);
        break;
      case 422:
        final errors = jsonDecode(response.body)['data']['errors'];
        apiResponse.error = errors[errors.keys.elementAt(0)][0];
        break;
      case 403:
        apiResponse.error = jsonDecode(response.body)['message'];
        break;

      default:
        print(response.body);
        apiResponse.error = somethingWhentWrong;
        break;
    }
  } catch (e) {
    print(serverError);
    apiResponse.error = serverError;
  }
  return apiResponse;
}

Future<ApiResponse> changeDataEmployee({
  required String idEmployee,
  required String firstName,
  required String lastName,
  required String nik,
  required String nip,
  required String placeOfBirth,
  required String dateOfBirth,
  required String gender,
  required String religion,
  required String address,
  required String position,
  required String phone,
  required String workshiftId,
}) async {
  ApiResponse apiResponse = ApiResponse();
  try {
    String token = await getToken();
    final response = await http.post(
        Uri.parse('$baseURL/admission/updatedataEmployeeadmin/$idEmployee'),
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Authorization': 'Bearer $token'
        },
        body: {
          'first_name': firstName,
          'last_name': lastName,
          'nik': nik,
          'nip': nip,
          'place_of_birth': placeOfBirth,
          'date_of_birth': dateOfBirth,
          'gender': gender,
          'religion': religion,
          'address': address,
          'position': position,
          'phone': phone,
          'workshift_id': workshiftId,
        });
    print(response.statusCode);
    switch (response.statusCode) {
      case 200:
        apiResponse.data = jsonDecode(response.body);
        break;
      case 500:
        print(response.body);
        apiResponse.error = serverError;
        break;
      case 400:
        final errors = jsonDecode(response.body)['data']['errors'];
        apiResponse.error = errors[errors.keys.elementAt(0)][0];
        break;
      default:
        apiResponse.error = somethingWhentWrong;
        break;
    }
  } catch (e) {
    apiResponse.error = serverError;
  }
  return apiResponse;
}
