import 'dart:ui';

import 'package:flutter/material.dart';

const serverError = 'Server Error';
const unauthorized = 'Kedaluwarsa';
const somethingWhentWrong = 'Terjadi Kesalahan';

// const baseURL = 'https://attendy.siretaku.com/api';
// const baseURL = 'http://10.208.136.173:8080/api';
const baseURL = 'https://attendy.my.id/main/api';
const String appIdOnesignal = "4f50535b-7613-4b92-83bc-e15627fd74d1";

const Color blueColor = Color(0xff3263c7);
const Color blue2Color = Color(0xff7092d8);
const Color yellowColor = Color(0xffffa201);
const Color orangeColor = Color(0xfffc5d34);
const Color greenColor = Color(0xff5ba874);
const Color redColor = Color(0xffd3282d);
const Color greyColor = Color.fromARGB(255, 204, 204, 204);
const Color grey2Color = Color(0xfff3f3f3);
const Color black2Color = Color(0xff4B556B);
const Color backgroundColor = Color(0xfffaf9ff);
const Color backgroundColor2 = Color(0xffeaeff8);
const Color backgroundGreyColor = Color(0xffe3e8e8);
const Color shimmer1 = Color.fromARGB(255, 216, 216, 216);
const Color shimmer2 = Color.fromARGB(31, 180, 180, 180);
const Color shimmer3 = Color.fromARGB(255, 216, 216, 216);

const String textDummy =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua';
const String textLogin =
    'Jika Anda mengalami masalah saat login, silakan hubungi admin kami untuk mendapatkan bantuan lebih lanjut.';
