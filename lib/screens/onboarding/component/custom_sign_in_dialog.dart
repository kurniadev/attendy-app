import 'package:attandy_app/constants.dart';
import 'package:attandy_app/screens/dashboard/trial_dashboard.dart';
import 'package:flutter/material.dart';
import '../../widget/custom_button.dart';

Future<Object?> customSigninDialog(BuildContext context,
    {required ValueChanged onCLosed}) {
  return showGeneralDialog(
    barrierDismissible: true,
    barrierLabel: "Sign In",
    context: context,
    transitionDuration: const Duration(milliseconds: 400),
    transitionBuilder: (_, animation, __, child) {
      Tween<Offset> tween;
      tween = Tween(begin: const Offset(0, -1), end: Offset.zero);
      return SlideTransition(
        position: tween.animate(
          CurvedAnimation(parent: animation, curve: Curves.easeInOutBack),
        ),
        child: child,
      );
    },
    pageBuilder: (context, _, __) => Center(
      child: Container(
        height: 590,
        margin: const EdgeInsets.symmetric(horizontal: 16),
        padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 24),
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.94),
          borderRadius: const BorderRadius.all(Radius.circular(40)),
        ),
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.transparent,
            body: SingleChildScrollView(
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: 150,
                        child: Image.asset(
                          "assets/Background/logo.png",
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 16),
                        child: Text(
                          "Selamat datang Attendy App. Masukkan email dan kata sandi Anda untuk masuk ke akun Anda",
                          style: TextStyle(
                            fontFamily: "PoppinsReguler",
                            fontSize: 12,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      Form(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Email",
                              style: TextStyle(color: Colors.black54),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 8, bottom: 16),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: const Color.fromARGB(
                                        255, 240, 240, 240),
                                    borderRadius: BorderRadius.circular(12)),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: TextFormField(
                                    decoration: const InputDecoration(
                                      border: InputBorder.none,
                                      hintText: 'Masukkan Email',
                                      hintStyle: TextStyle(fontSize: 14),
                                      // prefixIcon: Padding(
                                      //   padding:
                                      //       const EdgeInsets.symmetric(horizontal: 8),
                                      //   child:
                                      //       SvgPicture.asset("assets/icons/email.svg"),
                                      // ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const Text(
                              "Password",
                              style: TextStyle(color: Colors.black54),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 8, bottom: 16),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: const Color.fromARGB(
                                        255, 240, 240, 240),
                                    borderRadius: BorderRadius.circular(12)),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: TextFormField(
                                    obscureText: true,
                                    decoration: const InputDecoration(
                                      border: InputBorder.none,
                                      hintText: 'Masukkan Kata Sandi',
                                      hintStyle: TextStyle(fontSize: 14),
                                      // prefixIcon: Padding(
                                      //   padding:
                                      //       const EdgeInsets.symmetric(horizontal: 8),
                                      //   child: SvgPicture.asset(
                                      //       "assets/icons/password.svg"),
                                      // ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      CustomButton(
                        press: () {
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (context) => const TrialDashboard()),
                              (route) => false);
                        },
                        title: 'Login',
                      )

                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      //   children: [
                      //     IconButton(
                      //       padding: EdgeInsets.zero,
                      //       onPressed: () {},
                      //       icon: SvgPicture.asset(
                      //         "assets/icons/email_box.svg",
                      //         height: 64,
                      //         width: 64,
                      //       ),
                      //     ),
                      //     IconButton(
                      //       padding: EdgeInsets.zero,
                      //       onPressed: () {},
                      //       icon: SvgPicture.asset(
                      //         "assets/icons/apple_box.svg",
                      //         height: 64,
                      //         width: 64,
                      //       ),
                      //     ),
                      //     IconButton(
                      //       padding: EdgeInsets.zero,
                      //       onPressed: () {},
                      //       icon: SvgPicture.asset(
                      //         "assets/icons/google_box.svg",
                      //         height: 64,
                      //         width: 64,
                      //       ),
                      //     ),
                      //   ],
                      // )
                    ],
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: -48,
                    child: InkWell(
                      onTap: () {},
                      child: const SizedBox(
                        height: 40,
                        width: 40,
                        child: CircleAvatar(
                          radius: 16,
                          backgroundColor: blueColor,
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )),
      ),
    ),
  ).then(onCLosed);
}
