import 'dart:ui';

import 'package:attandy_app/screens/login/login_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

import '../../constants.dart';
import 'component/animated_btn..dart';
import 'component/custom_sign_in_dialog.dart';

class OnboardingScreen extends StatefulWidget {
  const OnboardingScreen({super.key});

  @override
  State<OnboardingScreen> createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  bool isSignInDialogShown = false;
  late RiveAnimationController _btnAnimationColtroller;

  @override
  void initState() {
    _btnAnimationColtroller = OneShotAnimation(
      "active",
      autoplay: false,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          // Positioned(
          //   width: MediaQuery.of(context).size.width * 1.6,
          //   bottom: 100,
          //   left: 70,
          //   child: Image.asset(
          //     "assets/Background/Spline.png",
          //   ),
          // ),
          Positioned.fill(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 20, sigmaY: 10),
            ),
          ),
          const RiveAnimation.asset(
            "assets/RiveAssets/shapes.riv",
            fit: BoxFit.cover,
          ),
          Positioned.fill(
            child: BackdropFilter(
              filter: ImageFilter.blur(
                sigmaX: 25,
                sigmaY: 25,
              ),
              child: const SizedBox(),
            ),
          ),
          AnimatedPositioned(
            top: isSignInDialogShown ? -50 : 0,
            duration: const Duration(milliseconds: 240),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        child: SizedBox(
                          width: 260,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text(
                                "Attendy",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 60,
                                  fontFamily: "Poppins",
                                  color: blueColor,
                                  height: 1.2,
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(
                                "Presensi lebih efisien hanya dalam satu genggaman dimanapun dan kapanpun.",
                                style: TextStyle(
                                  fontSize: 14,
                                  color: black2Color,
                                  fontFamily: "PoppinsReguler",
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 150,
                        child: Opacity(
                          opacity: 0.8,
                          child: Image.asset(
                            "assets/Background/right.png",
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 230,
                  ),
                  AnimatedBtn(
                    btnAnimationColtroller: _btnAnimationColtroller,
                    press: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const LoginScreen()));
                    },
                  ),
                  const Spacer(flex: 1),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 24, horizontal: 25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text(
                          "PT. Teknolab Caraka Internasional  ",
                          style: TextStyle(
                            fontSize: 14,
                            color: black2Color,
                            fontWeight: FontWeight.bold,
                            fontFamily: "PoppinsReguler",
                          ),
                        ),
                        Text(
                          "Develop By Reza Kurnia Setiawan",
                          style: TextStyle(
                            fontSize: 12,
                            color: black2Color,
                            fontFamily: "PoppinsReguler",
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
