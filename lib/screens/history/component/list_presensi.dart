import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../widget/custom_shimmer.dart';

class ListPresensi extends StatefulWidget {
  const ListPresensi({
    super.key,
    required this.date,
    required this.checkIn,
    required this.deviceInfo,
    required this.location,
    required this.islate,
    required this.statusCekOut,
    required this.checkOut,
    required this.imageUrl,
    required this.pressCheckIn,
    required this.pressCheckOut,
  });

  final String date, checkIn, checkOut, deviceInfo, location, islate;
  final dynamic statusCekOut, imageUrl;
  final VoidCallback pressCheckIn, pressCheckOut;

  @override
  State<ListPresensi> createState() => _ListPresensiState();
}

class _ListPresensiState extends State<ListPresensi> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10,
        ),
        Text(
          widget.date,
          style: const TextStyle(
            fontSize: 12,
            color: black2Color,
            fontFamily: 'PoppinsReguler',
            fontWeight: FontWeight.bold,
            letterSpacing: 1,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        GestureDetector(
          onTap: widget.pressCheckIn,
          child: Container(
            height: 120,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              image: const DecorationImage(
                  image: AssetImage(
                    'assets/Background/bg-attand.png',
                  ),
                  fit: BoxFit.cover),
            ),
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            const Text(
                              'Absensi Masuk',
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontFamily: 'PoppinsReguler',
                                fontWeight: FontWeight.bold,
                                letterSpacing: 0.5,
                              ),
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            SizedBox(
                              height: 16,
                              child: Image.asset(
                                'assets/Icons/verify.png',
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                        Expanded(
                          child: Text(
                            widget.checkIn,
                            style: const TextStyle(
                              fontSize: 30,
                              color: Colors.white,
                              fontFamily: 'PoppinsReguler',
                              fontWeight: FontWeight.w700,
                              letterSpacing: 0.5,
                            ),
                          ),
                        ),
                        Text(
                          widget.location,
                          style: const TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                            fontFamily: 'PoppinsReguler',
                            fontWeight: FontWeight.bold,
                            letterSpacing: 0.5,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 120,
                  width: MediaQuery.of(context).size.width * 0.42,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 22,
                              child: Image.asset(
                                'assets/Icons/handphone.png',
                                color: Colors.white,
                              ),
                            ),
                            const SizedBox(
                              width: 6,
                            ),
                            Expanded(
                              child: Text(
                                widget.deviceInfo,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontFamily: 'PoppinsReguler',
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 0.5,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Container(
                          height: 30,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.25),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Center(
                            child: Text(
                              widget.islate,
                              style: const TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontFamily: 'PoppinsReguler',
                                fontWeight: FontWeight.bold,
                                letterSpacing: 0.5,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        if (widget.statusCekOut != null)
          GestureDetector(
            onTap: widget.pressCheckOut,
            child: Container(
              height: 120,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  image: const DecorationImage(
                      image: AssetImage(
                        'assets/Background/bg-attand2.png',
                      ),
                      fit: BoxFit.cover)),
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              const Text(
                                'Absensi Keluar',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontFamily: 'PoppinsReguler',
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 0.5,
                                ),
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              SizedBox(
                                height: 16,
                                child: Image.asset(
                                  'assets/Icons/verify.png',
                                  color: Colors.white,
                                ),
                              )
                            ],
                          ),
                          Expanded(
                            child: Text(
                              widget.checkOut,
                              style: const TextStyle(
                                fontSize: 30,
                                color: Colors.white,
                                fontFamily: 'PoppinsReguler',
                                fontWeight: FontWeight.w700,
                                letterSpacing: 0.5,
                              ),
                            ),
                          ),
                          const Text(
                            'Ketuk untuk melihat detail',
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.white,
                              fontFamily: 'PoppinsReguler',
                              fontWeight: FontWeight.bold,
                              letterSpacing: 0.5,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        else
          const SizedBox(),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
