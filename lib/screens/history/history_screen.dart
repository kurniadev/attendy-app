// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/screens/history/component/list_presensi.dart';
import 'package:attandy_app/services/attandance_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'dart:math' as math;

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../models/attandance_model.dart';
import '../../services/auth_service.dart';
import '../shimmer/shimmer_history_attandance.dart';
import '../splash_screen.dart';
import '../widget/custom_shimmer.dart';
import 'component/image_attandance.dart';

class HistoryScreen extends StatefulWidget {
  const HistoryScreen({super.key});

  @override
  State<HistoryScreen> createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  String _formatDate(DateTime dateTime) {
    return DateFormat('EEEE, d MMMM yyyy', "id_ID").format(dateTime);
  }

  bool _loading = true;
  List<dynamic> historyAllAttandancetList = [];

  Future<void> fungsiGetHistoryAttandanceAll() async {
    ApiResponse response = await getHistoryAttandanceAll();
    if (response.error == null) {
      setState(() {
        historyAllAttandancetList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  @override
  void initState() {
    fungsiGetHistoryAttandanceAll();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Presensi",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: _loading
          ? const SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(12.0),
                child: ShimmerHistoryAttandance(),
              ),
            )
          : historyAllAttandancetList.isEmpty
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 150,
                      width: MediaQuery.of(context).size.width,
                      child: Lottie.asset(
                        "assets/lottie/empty-box-blue.json",
                      ),
                    ),
                  ],
                )
              : ListView.builder(
                  itemCount: historyAllAttandancetList.length,
                  itemBuilder: (BuildContext context, int index) {
                    String checkOut = '';
                    HistoryAllAttandanceModel historyAllAttandanceModel =
                        historyAllAttandancetList[index];
                    String date = DateFormat('dd-MM-yyyy')
                        .format(historyAllAttandanceModel.checkIn);
                    String dateCekIn = DateFormat('KK:mm a')
                        .format(historyAllAttandanceModel.checkIn);
                    if (historyAllAttandanceModel.checkOut != null) {
                      String dateCekOut = DateFormat('KK:mm a').format(
                          DateTime.parse(historyAllAttandanceModel.checkOut));
                      checkOut = dateCekOut;
                    }
                    return Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: ListPresensi(
                        date: _formatDate(historyAllAttandanceModel.checkIn),
                        checkIn: dateCekIn,
                        deviceInfo: historyAllAttandanceModel.deviceInfo,
                        location:
                            '${historyAllAttandanceModel.latitude} ${historyAllAttandanceModel.latitude}',
                        islate: historyAllAttandanceModel.islate,
                        statusCekOut: historyAllAttandanceModel.checkOut,
                        checkOut: checkOut,
                        imageUrl: historyAllAttandanceModel.imageCheckIn,
                        pressCheckIn: () {
                          showModalBottomSheet(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            context: context,
                            builder: (builder) {
                              return Container(
                                  height: 300.0,
                                  color: Colors
                                      .transparent, //could change this to Color(0xFF737373),
                                  child: ImageAttandance(
                                    imageUrl:
                                        historyAllAttandanceModel.imageCheckIn,
                                    title: 'Foto Absensi Masuk',
                                  ));
                            },
                          );
                        },
                        pressCheckOut: () {
                          showModalBottomSheet(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            context: context,
                            builder: (builder) {
                              return Container(
                                  height: 300.0,
                                  color: Colors
                                      .transparent, //could change this to Color(0xFF737373),
                                  child: ImageAttandance(
                                    imageUrl:
                                        historyAllAttandanceModel.imageCheckOut,
                                    title: 'Foto Absensi Keluar',
                                  ));
                            },
                          );
                        },
                      ),
                    );
                  },
                ),
    );
  }
}
