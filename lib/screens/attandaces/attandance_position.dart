import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_month_picker/flutter_month_picker.dart';
import 'package:http/http.dart' as http;
// import 'package:charts_flutter/flutter.dart' as charts;
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';

import '../../constants.dart';
import '../../models/test_model.dart';
import '../../services/auth_service.dart';

class AttandancePosition extends StatefulWidget {
  const AttandancePosition({super.key});

  @override
  State<AttandancePosition> createState() => _AttandancePositionState();
}

class _AttandancePositionState extends State<AttandancePosition> {
  bool loading = true;
  TextEditingController date = TextEditingController();
  TextEditingController monthName = TextEditingController();
  final String datenow = DateFormat('MM').format(DateTime.now());
  final Map<String, String> monthNames = {
    '01': 'Januari',
    '02': 'Februari',
    '03': 'Maret',
    '04': 'April',
    '05': 'Mei',
    '06': 'Juni',
    '07': 'Juli',
    '08': 'Agustus',
    '09': 'September',
    '10': 'Oktober',
    '11': 'November',
    '12': 'Desember',
  };

  List<Datum> datas = [];
  List<Datum> datas2 = [];
  List<Datum> datas3 = [];
  void getData() async {
    String token = await getToken();
    var response = await http.get(
      Uri.parse('$baseURL/attendances/getdailyreportposition/${date.text}'),
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'Bearer $token'
      },
    );
    List data1 = json.decode(response.body)['data']['data1'];
    List data2 = json.decode(response.body)['data']['data2'];
    List data3 = json.decode(response.body)['data']['data3'];

    setState(() {
      //memasukan data json ke dalam model
      datas = stockModelFromJson(data1);
      datas2 = stockModelFromJson(data2);
      datas3 = stockModelFromJson(data3);

      loading = false;
    });
  }

  List<charts.Series<Datum, String>> chartPresensiEmployee() {
    return [
      charts.Series<Datum, String>(
          data: datas,
          id: 'id',
          seriesColor: charts.ColorUtil.fromDartColor(greenColor),
          domainFn: (Datum datum, _) => datum.domain,
          measureFn: (Datum datum, _) => datum.measure),
      charts.Series<Datum, String>(
          data: datas2,
          id: 'id',
          seriesColor: charts.ColorUtil.fromDartColor(orangeColor),
          domainFn: (Datum datum, _) => datum.domain,
          measureFn: (Datum datum, _) => datum.measure),
      charts.Series<Datum, String>(
          data: datas3,
          id: 'id',
          seriesColor: charts.ColorUtil.fromDartColor(redColor),
          domainFn: (Datum datum, _) => datum.domain,
          measureFn: (Datum datum, _) => datum.measure),
    ];
  }

  @override
  void initState() {
    super.initState();
    date.text = datenow;
    monthName.text = monthNames[datenow] ?? '';
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(
          "Laporan Berdasarkan Posisi",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      'Presentase Bulanan',
                      style: TextStyle(
                        fontSize: 16,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.bold,
                        color: black2Color,
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        final selected = await showMonthPicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2000),
                          lastDate: DateTime(2050),
                        );
                        if (selected != null) {
                          String formattedDate =
                              DateFormat('MM').format(selected);
                          setState(() {
                            date.text = formattedDate;
                            monthName.text = monthNames[formattedDate] ?? '';
                            print(monthName.text);
                            getData();
                            loading = true;
                          });
                        } else {
                          print("Date is not selected");
                        }
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(7),
                            color: Colors.white,
                            border: Border.all(color: greyColor)),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Row(
                            children: [
                              const SizedBox(
                                height: 20,
                                width: 20,
                                child: Icon(
                                  Icons.calendar_month,
                                  size: 18,
                                ),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Text(
                                monthName.text,
                                style: const TextStyle(
                                  fontSize: 14,
                                  fontFamily: "Roboto",
                                  fontWeight: FontWeight.normal,
                                  color: black2Color,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              loading
                  ? SizedBox(
                      height: 250,
                      child: Center(
                        child: SizedBox(
                          height: 100,
                          width: MediaQuery.of(context).size.width,
                          child: Lottie.asset(
                            "assets/lottie/loading.json",
                          ),
                        ),
                      ),
                    )
                  : SizedBox(
                      height: MediaQuery.of(context).size.height * 0.5,
                      width: MediaQuery.of(context).size.width,
                      child: charts.BarChart(
                        chartPresensiEmployee(),
                        animate: true,
                        defaultRenderer: charts.BarRendererConfig(
                          maxBarWidthPx: 100,
                        ),
                      ),
                    ),
              const SizedBox(
                height: 20,
              ),
              loading
                  ? const SizedBox()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Keterangan',
                          style: TextStyle(
                            fontSize: 16,
                            fontFamily: "Roboto",
                            fontWeight: FontWeight.bold,
                            color: black2Color,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.white,
                              border: Border.all(color: greyColor)),
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          color: greenColor),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    const Text(
                                      'Tepat Waktu',
                                      style: TextStyle(
                                        fontSize: 13,
                                        fontFamily: "Roboto",
                                        fontWeight: FontWeight.bold,
                                        color: black2Color,
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          color: orangeColor),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    const Text(
                                      'Terlambat',
                                      style: TextStyle(
                                        fontSize: 13,
                                        fontFamily: "Roboto",
                                        fontWeight: FontWeight.bold,
                                        color: black2Color,
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          color: redColor),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    const Text(
                                      'Tidak Hadir',
                                      style: TextStyle(
                                        fontSize: 13,
                                        fontFamily: "Roboto",
                                        fontWeight: FontWeight.bold,
                                        color: black2Color,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: blueColor,
                              border: Border.all(color: greyColor)),
                          child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Text(
                                    'GA : General Affair',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    'MS : Marketing Staff',
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    'NES : Network Engineer Staff',
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    'EES : Electronics Engineer Staff',
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              )),
                        ),
                      ],
                    )
            ],
          ),
        ),
      ),
    );
  }
}
