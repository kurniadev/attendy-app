import 'package:attandy_app/screens/widget/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../constants.dart';

class DummyLocation extends StatefulWidget {
  const DummyLocation({super.key});

  @override
  State<DummyLocation> createState() => _DummyLocationState();
}

class _DummyLocationState extends State<DummyLocation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 300,
            child: Lottie.asset(
              "assets/lottie/stop.json",
            ),
          ),
          Text(
            'Anda Terdeteksi \n Menggunakan Lokasi Tiruan',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
              color: black2Color,
              fontFamily: 'PoppinsReguler',
              fontWeight: FontWeight.bold,
              letterSpacing: 1,
            ),
          ),
          SizedBox(
            height: 100,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 35),
            child: CustomButton(
                press: () {
                  Navigator.pop(context);
                },
                title: 'Kembali'),
          )
        ],
      ),
    );
  }
}
