import 'package:attandy_app/constants.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class WaitingAttandy extends StatefulWidget {
  const WaitingAttandy({super.key});

  @override
  State<WaitingAttandy> createState() => _WaitingAttandyState();
}

class _WaitingAttandyState extends State<WaitingAttandy> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 200,
            child: Lottie.asset(
              "assets/lottie/maps.json",
            ),
          ),
          Text(
            'Tunggu Sebentar',
            style: const TextStyle(
              fontSize: 16,
              color: black2Color,
              fontFamily: 'PoppinsReguler',
              fontWeight: FontWeight.bold,
              letterSpacing: 1,
            ),
          ),
        ],
      ),
    );
  }
}
