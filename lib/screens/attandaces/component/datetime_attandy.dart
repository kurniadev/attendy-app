import 'package:flutter/material.dart';

import '../../../constants.dart';

class DateTimeAttandy extends StatefulWidget {
  const DateTimeAttandy({super.key, required this.date, required this.time});

  final String date, time;

  @override
  State<DateTimeAttandy> createState() => _DateTimeAttandyState();
}

class _DateTimeAttandyState extends State<DateTimeAttandy> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          Center(
            child: Container(
              height: 60,
              width: MediaQuery.of(context).size.width * 0.5,
              decoration: BoxDecoration(
                color: blueColor,
                borderRadius: BorderRadius.circular(30),
              ),
              child: Stack(
                children: [
                  Positioned(
                      bottom: -20,
                      right: -15,
                      child: Container(
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(60)),
                      )),
                  Positioned(
                    top: 0,
                    right: 0,
                    left: 0,
                    bottom: 0,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          widget.date,
                          style: const TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                            fontFamily: 'PoppinsReguler',
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1,
                          ),
                        ),
                        Text(
                          widget.time,
                          style: const TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
