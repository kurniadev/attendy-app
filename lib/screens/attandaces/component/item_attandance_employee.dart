import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../widget/custom_shimmer.dart';

class ItemAttandanceEmployee extends StatefulWidget {
  const ItemAttandanceEmployee(
      {super.key,
      required this.name,
      required this.startTime,
      required this.isLate,
      required this.color,
      required this.imageUrl});

  final String name, startTime, isLate;
  final Color color;
  final dynamic imageUrl;

  @override
  State<ItemAttandanceEmployee> createState() => _ItemAttandanceEmployeeState();
}

class _ItemAttandanceEmployeeState extends State<ItemAttandanceEmployee> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12),
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            border: Border.all(color: greyColor),
            color: Colors.white),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.name,
                      style: const TextStyle(
                          fontSize: 14,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.bold,
                          color: black2Color,
                          letterSpacing: 0.5),
                    ),
                    Text(
                      'Berhasil Melakukan Absensi Masuk Pukul ${widget.startTime} ${widget.isLate}',
                      style: const TextStyle(
                          fontSize: 14,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.normal,
                          color: black2Color,
                          letterSpacing: 0.5),
                    ),
                  ],
                ),
              ),
              SizedBox(
                  height: 60.0,
                  width: 60.0,
                  // decoration: BoxDecoration(
                  //   border: Border.all(color: greenColor, width: 2),
                  //   borderRadius: BorderRadius.circular(12),
                  // ),
                  child: CachedNetworkImage(
                    imageUrl: widget.imageUrl,
                    fit: BoxFit.cover,
                    imageBuilder: (context, imageProvider) => Container(
                      height: 60.0,
                      width: 60.0,
                      decoration: BoxDecoration(
                        border: Border.all(color: widget.color, width: 3),
                        borderRadius: BorderRadius.circular(12),
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                    placeholder: (context, url) =>
                        const CustomShimmer(height: 60, width: 60, radius: 12),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
