import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../widget/custom_button.dart';

class FooterAttandance extends StatefulWidget {
  const FooterAttandance(
      {super.key,
      required this.diff,
      required this.startTime,
      required this.endTime,
      required this.road,
      required this.press});

  final String diff, startTime, endTime, road;
  final VoidCallback press;

  @override
  State<FooterAttandance> createState() => _FooterAttandanceState();
}

class _FooterAttandanceState extends State<FooterAttandance> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 10,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SizedBox(
            width: MediaQuery.of(context).size.width * 0.9,
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      children: [
                        Container(
                          height: 25,
                          width: 25,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(40)),
                          child: const Icon(
                            Icons.login,
                            color: Colors.white,
                            size: 13,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Container(
                            width: 1,
                            height: 60,
                            color: Colors.black.withOpacity(0.5),
                          ),
                        ),
                        Container(
                          height: 25,
                          width: 25,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                                color: Colors.black.withOpacity(0.5)),
                            borderRadius: BorderRadius.circular(40),
                          ),
                          child: const Icon(
                            Icons.logout,
                            color: black2Color,
                            size: 13,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.startTime,
                          style: const TextStyle(
                            fontSize: 24,
                            color: blueColor,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1,
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        // Text(
                        //   '${widget.diff} Menit Lagi',
                        //   style: const TextStyle(
                        //     fontSize: 14,
                        //     color: blueColor,
                        //     fontFamily: 'PoppinsReguler',
                        //     fontWeight: FontWeight.normal,
                        //   ),
                        // ),
                        SizedBox(
                          height: 35,
                          width: MediaQuery.of(context).size.width * 0.75,
                          child: Text(
                            widget.road,
                            // 'Jl. Brigjend Katamso No.21-25, Ngeni, Kepuhkiriman, Kec. Waru, Kabupaten Sidoarjo, Jawa Timur 61256',
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 12,
                              color: black2Color,
                              fontFamily: 'PoppinsReguler',
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                        Text(
                          widget.endTime,
                          style: const TextStyle(
                            fontSize: 24,
                            color: black2Color,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                CustomButton(
                  press: widget.press,
                  title: 'Absensi Masuk',
                )
              ],
            ),
          ),
        ));
  }
}
