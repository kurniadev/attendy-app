import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BackAttandy extends StatefulWidget {
  const BackAttandy({super.key, required this.press});

  final VoidCallback press;

  @override
  State<BackAttandy> createState() => _BackAttandyState();
}

class _BackAttandyState extends State<BackAttandy> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.press,
      child: Container(
        height: 50,
        width: 50,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(50),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 5,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: const Icon(CupertinoIcons.back),
      ),
    );
  }
}
