import 'package:flutter/material.dart';
import 'package:printing/printing.dart';
import '../builder/daily.dart';

class DailyGeneratePdf extends StatefulWidget {
  const DailyGeneratePdf({
    super.key,
    required this.data,
  });
  final List<dynamic> data;
  @override
  State<DailyGeneratePdf> createState() => _DailyGeneratePdfState();
}

class _DailyGeneratePdfState extends State<DailyGeneratePdf> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Laporan",
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w500,
            letterSpacing: 1,
          ),
        ),
        elevation: 0,
        backgroundColor: const Color(0xff276aa5),
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      body: PdfPreview(
        canChangeOrientation: false,
        canChangePageFormat: false,
        canDebug: false,
        build: (format) => daily(
          format: format,
          data: widget.data,
        ),
      ),
      //  ListView.builder(itemBuilder: (BuildContext context, int index) {
      //   AttandanceEmployeeModel attandanceEmployeeModel =
      //       widget.attendanceEmployeeList[index];
      //   String dateCekIn =
      //       DateFormat('HH:mm:ss').format(attandanceEmployeeModel.checkIn);
      //   return attandanceEmployeeModel.islate != "Tidak Hadir"
      //       ? Padding(
      //           padding: const EdgeInsets.symmetric(horizontal: 12),
      //           child: ItemAttandanceEmployee(
      //             imageUrl: attandanceEmployeeModel.imageCheckIn,
      //             startTime: dateCekIn,
      //             name:
      //                 '${attandanceEmployeeModel.firstName} ${attandanceEmployeeModel.lastName}',
      //             color: attandanceEmployeeModel.islate == 'Terlambat'
      //                 ? blueColor
      //                 : greenColor,
      //             isLate: attandanceEmployeeModel.islate,
      //           ),
      //         )
      //       : const SizedBox();
      // }),
    );
  }
}
