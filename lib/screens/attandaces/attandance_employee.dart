// ignore_for_file: use_build_context_synchronously
import 'package:attandy_app/screens/attandaces/report_attandance/annualy_report.dart';
import 'package:attandy_app/screens/attandaces/report_attandance/daily_report.dart';
import 'package:attandy_app/screens/attandaces/report_attandance/monthly_report.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

class AttandanceEmployee extends StatefulWidget {
  const AttandanceEmployee({super.key});

  @override
  State<AttandanceEmployee> createState() => _AttandanceEmployeeState();
}

class _AttandanceEmployeeState extends State<AttandanceEmployee> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: const Text(
              "Laporan Keseluruhan",
              style: TextStyle(
                fontSize: 18,
                color: black2Color,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w700,
                letterSpacing: 1,
              ),
            ),
            bottom: const TabBar(
              indicatorColor: blueColor,
              labelColor: black2Color,
              labelStyle: TextStyle(
                fontSize: 14,
                color: black2Color,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w700,
                letterSpacing: 1,
              ),
              tabs: [
                Tab(
                  text: 'Harian',
                ),
                Tab(
                  text: 'Bulanan',
                ),
                Tab(text: 'Tahunan'),
              ],
            ),
            backgroundColor: Colors.white,
            iconTheme: const IconThemeData(
              color: black2Color,
            ),
            elevation: 0,
          ),
          body: const TabBarView(
            children: [
              DailyReport(),
              MonthlyReport(),
              AnnualyReport(),
            ],
          ),
        ),
      ),
    );
  }
}
