// ignore_for_file: use_build_context_synchronously

import 'dart:async';
import 'dart:io';

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/constants.dart';
import 'package:attandy_app/screens/attandaces/component/back_attandy.dart';
import 'package:attandy_app/screens/widget/custom_button.dart';
import 'package:attandy_app/services/attandance_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../widget/custom_loading.dart';
import '../widget/custom_shimmer.dart';

class AttandanceOutScreen extends StatefulWidget {
  const AttandanceOutScreen(
      {super.key,
      required this.imageUrl,
      required this.time,
      required this.name});

  final String imageUrl, time, name;

  @override
  State<AttandanceOutScreen> createState() => _AttandanceOutScreenState();
}

class _AttandanceOutScreenState extends State<AttandanceOutScreen> {
  File? _imageCamera;
  bool _loading = false;
  String? _time;
  final _picker = ImagePicker();

  Future getCamera() async {
    final pickedFile = await _picker.getImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        _imageCamera = File(pickedFile.path);
      });
    }
  }

  void fungsiCheckOut() async {
    showAlertDialog(context);
    String? image = _imageCamera == null ? null : getStringImage(_imageCamera);
    ApiResponse response = await attandanceCekOut(
      imageCheckOut: image,
    );
    if (response.error == null) {
      Navigator.pop(context);
      AnimatedSnackBar.material('Berhasil Berhasil Melakukan Absensi Pulang',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        removePred();
        Navigator.pop(context, 'refresh');
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  removePred() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.remove("chekin");
    preferences.commit();
  }

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedTime = _formatTime(now);
    setState(() {
      _time = formattedTime;
    });
  }

  String _formatTime(DateTime dateTime) {
    return DateFormat('HH:mm:ss').format(dateTime);
  }

  @override
  void initState() {
    _time = _formatTime(DateTime.now());

    Timer.periodic(const Duration(seconds: 1), (Timer t) => _getTime());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor2,
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(
              height: 140,
              width: MediaQuery.of(context).size.width,
              child: Stack(children: [
                Positioned(
                  right: 0,
                  child: Opacity(
                    opacity: 0.2,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12.0),
                      child: Image.asset(
                        'assets/Background/Border@3x.png',
                        height: 70,
                        width: 70,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(50),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.2),
                            spreadRadius: 2,
                            blurRadius: 5,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: const Icon(CupertinoIcons.back),
                    ),
                  ),
                )
              ]),
            ),
            const SizedBox(
              height: 50,
            ),
            Expanded(
              child: Container(
                height: 120,
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Stack(clipBehavior: Clip.none, children: [
                  Positioned(
                    left: 15,
                    right: 15,
                    top: -35,
                    child: Container(
                        height: 80,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 2,
                              blurRadius: 5,
                              offset: const Offset(
                                  0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Stack(
                          children: [
                            Positioned(
                              right: 0,
                              child: Opacity(
                                opacity: 0.8,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(12.0),
                                  child: Image.asset(
                                    'assets/Background/Border@3x.png',
                                    height: 55,
                                    width: 55,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Selamat Istirahat ${widget.name}',
                                    style: const TextStyle(
                                      fontSize: 16,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.w700,
                                      color: blueColor,
                                      letterSpacing: 0.5,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: const [
                                      Expanded(
                                        child: Text(
                                          'Silahkan ambil foto terlebih dahulu , kemudian lakukan absensi pulang',
                                          style: TextStyle(
                                            fontSize: 13,
                                            fontFamily: "Roboto",
                                            fontWeight: FontWeight.normal,
                                            color: black2Color,
                                            letterSpacing: 0.5,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                        width: 50,
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ],
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 70,
                        ),
                        Row(
                          children: [
                            Container(
                                height: 55,
                                width: 55,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: CachedNetworkImage(
                                  imageUrl: widget.imageUrl,
                                  fit: BoxFit.cover,
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    width: 55.0,
                                    height: 55.0,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                  placeholder: (context, url) =>
                                      const CustomShimmer(
                                          height: 55, width: 55, radius: 12),
                                  errorWidget: (context, url, error) =>
                                      const Icon(Icons.error),
                                )),
                            const SizedBox(
                              width: 15,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  'Absens Masuk',
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.normal,
                                      color: black2Color,
                                      letterSpacing: 0.5),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  widget.time,
                                  style: const TextStyle(
                                      fontSize: 14,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.bold,
                                      color: black2Color,
                                      letterSpacing: 0.5),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: SizedBox(
                            height: 40,
                            width: 55,
                            child: Center(
                              child: Container(
                                height: 40,
                                width: 3,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    color: blueColor),
                              ),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            _imageCamera == null
                                ? Container(
                                    height: 55,
                                    width: 55,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      image: const DecorationImage(
                                          image: AssetImage(
                                              'assets/Background/image-not-available.jpg'),
                                          fit: BoxFit.cover),
                                    ),
                                  )
                                : GestureDetector(
                                    onTap: () {
                                      getCamera();
                                    },
                                    child: Container(
                                      height: 55,
                                      width: 55,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        image: DecorationImage(
                                            image: FileImage(
                                                _imageCamera ?? File('')),
                                            fit: BoxFit.cover),
                                      ),
                                    ),
                                  ),
                            const SizedBox(
                              width: 15,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  'Absens Pulang',
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.normal,
                                      color: black2Color,
                                      letterSpacing: 0.5),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  _time.toString(),
                                  style: const TextStyle(
                                      fontSize: 14,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.bold,
                                      color: black2Color,
                                      letterSpacing: 0.5),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Container(
                color: Colors.white,
                child: _imageCamera == null
                    ? Padding(
                        padding: const EdgeInsets.only(top: 8, bottom: 24),
                        child: GestureDetector(
                          onTap: () {
                            getCamera();
                          },
                          child: Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                color: redColor,
                                borderRadius: BorderRadius.circular(12)),
                            child: const Center(
                              child: Text(
                                'Ambil Foto',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      )
                    : CustomButton(
                        press: () {
                          fungsiCheckOut();
                        },
                        title: 'Absen Pulang Sekarang'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
