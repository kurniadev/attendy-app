// ignore_for_file: use_build_context_synchronously

import 'dart:async';
import 'dart:io';
import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/screens/attandaces/component/back_attandy.dart';
import 'package:attandy_app/screens/attandaces/component/datetime_attandy.dart';
import 'package:attandy_app/screens/attandaces/component/dummy_location.dart';
import 'package:attandy_app/screens/attandaces/component/footer_attandance.dart';
import 'package:attandy_app/screens/attandaces/component/waiting_attandy.dart';
import 'package:attandy_app/services/attandance_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geocoding/geocoding.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:latlong2/latlong.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trust_location/trust_location.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../models/company_model.dart';
import '../../services/auth_service.dart';
import '../../services/company_service.dart';
import '../splash_screen.dart';
import '../widget/custom_button.dart';
import '../widget/custom_loading.dart';

class AttandanceScreen extends StatefulWidget {
  const AttandanceScreen({
    super.key,
    required this.startTime,
    required this.endTime,
    required this.diff,
    required this.deviceInfo,
  });

  final String startTime, endTime, diff;
  final dynamic deviceInfo;

  @override
  State<AttandanceScreen> createState() => _AttandanceScreenState();
}

class _AttandanceScreenState extends State<AttandanceScreen> {
  String? _date, _time;
  double? latitudes, longitudes, latitude, longitude;
  bool? isMock, isMock2;
  bool showPage = false;
  String negara = '';
  String jalan = '';
  bool _loading = false;
  CompanyModel? companyModel;

  File? _imageFile;
  final _picker = ImagePicker();

  Future getImage() async {
    // ignore: deprecated_member_use
    final pickedFile = await _picker.getImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
      });
    }
  }

  void functionGetCompany() async {
    ApiResponse response = await getCompany();
    if (response.error == null) {
      setState(() {
        companyModel = response.data as CompanyModel;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void fungsiCheckIn() async {
    showAlertDialog(context);
    String? image = _imageFile == null ? null : getStringImage(_imageFile);
    ApiResponse response = await attandanceCekin(
      latitudeTo: latitude.toString(),
      longitudeTo: longitude.toString(),
      deviceInfo: widget.deviceInfo,
      imageCheckIn: image,
    );
    if (response.error == null) {
      Navigator.pop(context);
      AnimatedSnackBar.material('Berhasil Berhasil Melakukan Absensi Masuk',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        setPrefCekIn();
        Navigator.pop(context, 'refresh');
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  setPrefCekIn() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('chekin', 'masuk');
  }

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDate = _formatDate(now);
    final String formattedTime = _formatTime(now);
    setState(() {
      _date = formattedDate;
      _time = formattedTime;
    });
  }

  String _formatDate(DateTime dateTime) {
    return DateFormat('EEEE, d MMMM yyyy', "id_ID").format(dateTime);
  }

  String _formatTime(DateTime dateTime) {
    return DateFormat('HH:mm:ss').format(dateTime);
  }

  void requestPermission() async {
    final permission = await Permission.location.request();
    if (permission == PermissionStatus.granted) {
      TrustLocation.start(10);
      getLocation();
    } else if (permission == PermissionStatus.denied) {
      await Permission.location.request();
    }
  }

  void getLocation() async {
    try {
      TrustLocation.onChange.listen((event) {
        setState(() {
          // if (event.latitude == null && event.longitude == null) {
          //   latitude = event.latitude;
          //   longitude = event.longitude;
          //   isMock2 = event.isMockLocation;
          // }
          latitude = double.parse(event.latitude!);
          longitude = double.parse(event.longitude!);
          isMock = event.isMockLocation;

          showPage = true;
          geoCode();
        });
      });
    } catch (e) {
      print('ERROR');
    }
  }

  void geoCode() async {
    List<Placemark> placemark =
        await placemarkFromCoordinates(latitude!, longitude!);

    setState(() {
      print(
          '${placemark[0].street}, ${placemark[0].subLocality}, ${placemark[0].locality}, ${placemark[0].postalCode}, ${placemark[0].country}');
      negara = placemark[0].country.toString();
      jalan =
          '${placemark[0].street}, ${placemark[0].subLocality}, ${placemark[0].locality}, ${placemark[0].postalCode}, ${placemark[0].country}';
    });
  }

  @override
  void initState() {
    functionGetCompany();
    requestPermission();
    getLocation();
    _date = _formatDate(DateTime.now());
    _time = _formatTime(DateTime.now());

    Timer.periodic(const Duration(seconds: 1), (Timer t) => _getTime());
    super.initState();
  }

  @override
  void dispose() {
    TrustLocation.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: showPage
          ? companyModel == null
              ? const SafeArea(
                  child: WaitingAttandy(),
                )
              : longitude == null
                  ? const SafeArea(
                      child: WaitingAttandy(),
                    )
                  : isMock == true
                      ? const SafeArea(
                          child: DummyLocation(),
                        )
                      : Stack(
                          children: <Widget>[
                            FlutterMap(
                              options: MapOptions(
                                center: LatLng(latitude!, longitude!),
                                zoom: 17,
                                maxZoom: 17,
                              ),
                              // nonRotatedChildren: [
                              //   AttributionWidget.defaultWidget(
                              //     source: 'Attandy',
                              //     onSourceTapped: null,
                              //   ),
                              // ],
                              children: [
                                TileLayer(
                                  urlTemplate:
                                      'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                                  userAgentPackageName:
                                      'dev.fleaflet.flutter_map.example',
                                ),
                                PolylineLayer(
                                  polylineCulling: false,
                                  polylines: [
                                    Polyline(
                                      points: [
                                        LatLng(
                                            double.parse(
                                                companyModel!.latitude),
                                            double.parse(
                                                companyModel!.longitude)),
                                        LatLng(latitude!, longitude!),
                                      ],
                                      color: blueColor.withOpacity(0.7),
                                      borderStrokeWidth: 2,
                                      borderColor: blueColor,
                                    ),
                                  ],
                                ),
                                CircleLayer(circles: [
                                  CircleMarker(
                                      point: LatLng(
                                          double.parse(companyModel!.latitude),
                                          double.parse(
                                              companyModel!.longitude)),
                                      color: blueColor.withOpacity(0.7),
                                      borderStrokeWidth: 2,
                                      borderColor: blueColor,
                                      useRadiusInMeter: true,
                                      radius: double.parse(companyModel!
                                          .distance) // 2000 meters | 2 km
                                      ),
                                ]),
                                MarkerLayer(
                                  markers: [
                                    Marker(
                                      point: LatLng(latitude!, longitude!),
                                      width: 80,
                                      height: 80,
                                      rotate: true,
                                      builder: (context) =>
                                          Image.asset('assets/Icons/pin.png'),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            SafeArea(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, right: 10, top: 15),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        BackAttandy(
                                          press: (() {
                                            Navigator.pop(context);
                                          }),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(12),
                                            color: Colors.white,
                                          ),
                                          child: Center(
                                              child: Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: Text(
                                              'Radius Perusahaan ${companyModel?.distance.toString()} M',
                                              style: const TextStyle(
                                                  color: blueColor,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                          )),
                                        )
                                      ],
                                    ),
                                    DateTimeAttandy(
                                        date: _date ?? '', time: _time ?? ''),
                                    Container(
                                      height: 225,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: const BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(12),
                                            topRight: Radius.circular(12)),
                                      ),
                                      child: Stack(
                                        clipBehavior: Clip.none,
                                        children: [
                                          FooterAttandance(
                                              diff: widget.diff,
                                              startTime: widget.startTime,
                                              endTime: widget.endTime,
                                              road: jalan,
                                              press: () {
                                                if (_imageFile == null) {
                                                  AnimatedSnackBar.material(
                                                          'Foto tidak boleh kosong',
                                                          type:
                                                              AnimatedSnackBarType
                                                                  .warning,
                                                          mobileSnackBarPosition:
                                                              MobileSnackBarPosition
                                                                  .top,
                                                          desktopSnackBarPosition:
                                                              DesktopSnackBarPosition
                                                                  .topCenter)
                                                      .show(context);
                                                } else {
                                                  print(latitude);
                                                  print(longitude);
                                                  print(widget.deviceInfo);
                                                  fungsiCheckIn();
                                                }
                                              }),
                                          Positioned(
                                            right: 20,
                                            top: 35,
                                            child: GestureDetector(
                                              onTap: () {
                                                getImage();
                                              },
                                              child: Container(
                                                height: 30,
                                                width: 100,
                                                decoration: BoxDecoration(
                                                  color: _imageFile == null
                                                      ? blueColor
                                                      : redColor,
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    const Icon(
                                                      Icons.camera_enhance,
                                                      color: Colors.white,
                                                      size: 14,
                                                    ),
                                                    const SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      _imageFile == null
                                                          ? 'Ambil Foto'
                                                          : 'Ganti Foto',
                                                      style: const TextStyle(
                                                        fontSize: 10,
                                                        color: Colors.white,
                                                        fontFamily:
                                                            'PoppinsReguler',
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        letterSpacing: 1,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            right: 20,
                                            top: -110,
                                            child: Container(
                                              height: 130,
                                              width: 100,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(12),
                                                image: DecorationImage(
                                                    image: FileImage(
                                                        _imageFile ?? File('')),
                                                    fit: BoxFit.cover),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.grey
                                                        .withOpacity(0.3),
                                                    spreadRadius: 3,
                                                    blurRadius: 5,
                                                    offset: const Offset(2,
                                                        4), // changes position of shadow
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        )
          : const SafeArea(
              child: WaitingAttandy(),
            ),
    );
  }
}
