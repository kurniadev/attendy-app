import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:typed_data';
import '../../../models/attandance_model.dart';

Future<Uint8List> daily({
  required PdfPageFormat format,
  required List<dynamic> data,
}) async {
  final pdf = Document();
  final images1 = MemoryImage(
      (await rootBundle.load('assets/Background/logo-teknolab.png'))
          .buffer
          .asUint8List());
  final images2 = MemoryImage(
      (await rootBundle.load('assets/Background/logo-teknolab.png'))
          .buffer
          .asUint8List());

  String _formatTime(DateTime dateTime) {
    return DateFormat('HH:mm:ss').format(dateTime);
  }
  // final ttf = await fontFromAssetBundle('assets/Fonts/font-normal.ttf');
  // final ttfb = await fontFromAssetBundle('assets/Fonts/font-bold.ttf');

  // print(output);

  pdf.addPage(
    Page(
      build: (context) {
        return Column(
          children: [
            Row(
              children: [
                SizedBox(
                  height: 75,
                  width: 75,
                  child: Image(images1),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "LAPORAN PRESENSI KARYAWAN",
                        style: TextStyle(
                          fontSize: 13,
                          color: PdfColors.black,
                          // font: ttf,
                          fontWeight: FontWeight.normal,
                          letterSpacing: 1,
                        ),
                      ),
                      Text(
                        "PT. TEKNOLAB CARAKA INTERNASIONAL",
                        style: TextStyle(
                          fontSize: 17,
                          color: PdfColors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        "Jl. Cibodas No.03, Tanggung, Kec. Kepanjenkidul, Kota Blitar",
                        style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                      Text(
                        "Email : office.teknolab@gmail.com",
                        style: TextStyle(
                          fontSize: 13,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                children: [
                  Container(
                    height: 1,
                    color: PdfColors.black,
                  ),
                  SizedBox(height: 1),
                  Container(
                    height: 1,
                    color: PdfColors.black,
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Table(
              border: TableBorder.all(color: PdfColors.black),
              children: [
                TableRow(
                  children: [
                    SizedBox(
                      width: 25,
                      child: Center(child: PaddedTextBold('Tanggal')),
                    ),
                    SizedBox(
                      width: 40,
                      child: Center(child: PaddedTextBold('Nama')),
                    ),
                    SizedBox(
                      width: 40,
                      child: Center(child: PaddedTextBold('Absensi Masuk')),
                    ),
                    SizedBox(
                      width: 40,
                      child: Center(child: PaddedTextBold('Absensi Keluar')),
                    ),
                    SizedBox(
                      width: 35,
                      child: Center(child: PaddedTextBold('Status')),
                    ),
                  ],
                ),
              ],
            ),
            ListView.builder(
              itemBuilder: (context, int index) {
                AttandanceEmployeeModel attandanceEmployeeModel = data[index];
                String dateCekIn = DateFormat('HH:mm:ss')
                    .format(attandanceEmployeeModel.checkIn);
                return Table(
                  border: TableBorder.all(color: PdfColors.black),
                  children: [
                    TableRow(
                      children: [
                        SizedBox(
                          width: 25,
                          child: PaddedText(attandanceEmployeeModel.date),
                        ),
                        SizedBox(
                          width: 40,
                          child: PaddedText(
                              '${attandanceEmployeeModel.firstName} ${attandanceEmployeeModel.lastName}'),
                        ),
                        SizedBox(
                          width: 40,
                          child: PaddedText(
                              _formatTime(attandanceEmployeeModel.checkIn)),
                        ),
                        SizedBox(
                          width: 40,
                          child: PaddedText(_formatTime(DateTime.parse(
                              attandanceEmployeeModel.checkOut))),
                        ),
                        SizedBox(
                          width: 35,
                          child: PaddedText(attandanceEmployeeModel.islate),
                        ),
                      ],
                    ),
                  ],
                );
              },
              itemCount: data.length,
            ),
          ],
          // ),
        );
      },
    ),
  );

  return pdf.save();
}

Widget PaddedText(
  final String text, {
  final TextAlign align = TextAlign.left,
}) =>
    Padding(
      padding: EdgeInsets.all(3),
      child: Text(text,
          textAlign: align,
          style: TextStyle(fontSize: 10, fontWeight: FontWeight.normal)),
    );
Widget PaddedTextBold(
  final String text, {
  final TextAlign align = TextAlign.left,
}) =>
    Padding(
      padding: EdgeInsets.all(3),
      child: Text(text,
          textAlign: align,
          style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold)),
    );
