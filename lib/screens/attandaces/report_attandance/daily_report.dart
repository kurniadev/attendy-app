// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/screens/attandaces/generate_pdf/daily_generate_pdf.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:pie_chart/pie_chart.dart';

import '../../../constants.dart';
import '../../../models/api_response_model.dart';
import '../../../models/attandance_model.dart';
import '../../../services/attandance_service.dart';
import '../../../services/auth_service.dart';
import '../../splash_screen.dart';
import '../component/item_attandance_employee.dart';

class DailyReport extends StatefulWidget {
  const DailyReport({super.key});

  @override
  State<DailyReport> createState() => _DailyReportState();
}

class _DailyReportState extends State<DailyReport> {
  bool _loading = true;
  bool _loadingpie = true;
  List<dynamic> attandanceEmployeeList = [];
  TextEditingController date = TextEditingController();
  final String datenow = DateFormat('yyyy-MM-dd').format(DateTime.now());

  Future<void> fungsigetHistoryAttandanceEmployee() async {
    _loadingpie = true;
    ApiResponse response = await getHistoryAttandanceEmployee(date: date.text);
    if (response.error == null) {
      setState(() {
        attandanceEmployeeList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  Map<String, double> dataMap = {
    "Tepat Waktu": 0,
    "Terlambat": 0,
    "Tidak Hadir": 0,
  };

  int totalEmployee = 0;
  ChartPresentModel? chartPresentModel;
  void fungsiGetPiechart() async {
    _loadingpie = true;
    ApiResponse response = await getPieCharPresentAdmin(tanggal: date.text);
    if (response.error == null) {
      setState(() {
        chartPresentModel = response.data as ChartPresentModel;
        // tepatwaktu = countAttandanceModel!.tepatWaktu.toDouble();
        _loadingpie = false;
        totalEmployee = chartPresentModel!.countLate +
            chartPresentModel!.countNotprecent +
            chartPresentModel!.countOntime;
        dataMap = {
          "Tepat Waktu": chartPresentModel!.tepatWaktu.toDouble(),
          "Terlambat": chartPresentModel!.terlambat.toDouble(),
          "Tidak Hadir": chartPresentModel!.tidakHadir.toDouble(),
        };
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  final colorList = <Color>[
    greenColor,
    orangeColor,
    redColor,
  ];

  @override
  void initState() {
    date.text = datenow;
    fungsiGetPiechart();
    fungsigetHistoryAttandanceEmployee();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const ScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Presentase Harian',
                  style: TextStyle(
                    fontSize: 16,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.bold,
                    color: black2Color,
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    DateTime? pickedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(2000),
                        lastDate: DateTime(2101));
                    if (pickedDate != null) {
                      String formattedDate =
                          DateFormat('yyyy-MM-dd').format(pickedDate);
                      setState(() {
                        date.text = formattedDate;
                        fungsigetHistoryAttandanceEmployee();
                        fungsiGetPiechart();
                        _loading = true;
                      });
                    } else {
                      print("Date is not selected");
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        color: Colors.white,
                        border: Border.all(color: greyColor)),
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Row(
                        children: [
                          const SizedBox(
                            height: 20,
                            width: 20,
                            child: Icon(
                              Icons.calendar_month,
                              size: 18,
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Text(
                            date.text,
                            style: const TextStyle(
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.normal,
                              color: black2Color,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          _loadingpie
              ? SizedBox(
                  height: 250,
                  child: Center(
                    child: SizedBox(
                      height: 100,
                      width: MediaQuery.of(context).size.width,
                      child: Lottie.asset(
                        "assets/lottie/loading.json",
                      ),
                    ),
                  ),
                )
              : chartPresentModel!.countLate == 0 &&
                      chartPresentModel!.countNotprecent == 0 &&
                      chartPresentModel!.countOntime == 0
                  ? SizedBox(
                      height: 250,
                      child: Center(
                        child: SizedBox(
                          height: 100,
                          width: MediaQuery.of(context).size.width,
                          child: Lottie.asset(
                            "assets/lottie/empty-box-blue.json",
                          ),
                        ),
                      ),
                    )
                  : Padding(
                      padding: const EdgeInsets.all(15),
                      child: PieChart(
                        dataMap: dataMap,
                        animationDuration: const Duration(milliseconds: 800),
                        chartLegendSpacing: 32,
                        chartRadius: MediaQuery.of(context).size.width,
                        colorList: colorList,
                        // initialAngleInDegree: 0,
                        chartType: ChartType.disc,
                        ringStrokeWidth: 32,
                        chartValuesOptions: const ChartValuesOptions(
                          showChartValuesInPercentage: true,
                        ),
                        legendOptions: const LegendOptions(
                          showLegendsInRow: false,
                          legendPosition: LegendPosition.right,
                          // showLegends: true,
                          legendTextStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
          _loadingpie
              ? const SizedBox()
              : chartPresentModel!.countLate == 0 &&
                      chartPresentModel!.countNotprecent == 0 &&
                      chartPresentModel!.countOntime == 0
                  ? const SizedBox()
                  : Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DailyGeneratePdf(
                                            data: attandanceEmployeeList,
                                          )));
                            },
                            child: Container(
                              height: 30,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                border: Border.all(color: blueColor),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Icon(
                                    Icons.download,
                                    size: 16,
                                    color: blueColor,
                                  ),
                                  Text(
                                    'Download Laporan',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: "Roboto",
                                        fontWeight: FontWeight.w700,
                                        color: blueColor,
                                        letterSpacing: 1),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
          const SizedBox(
            height: 10,
          ),
          _loadingpie
              ? const SizedBox()
              : chartPresentModel!.countLate == 0 &&
                      chartPresentModel!.countNotprecent == 0 &&
                      chartPresentModel!.countOntime == 0
                  ? const SizedBox()
                  : Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Keterangan',
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.bold,
                              color: black2Color,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              // border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(10),
                              color: blueColor,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Dari $totalEmployee karyawan yang berhasil melakukan presensi masuk tepat waktu hari ini ada ${chartPresentModel!.countOntime} orang (${chartPresentModel!.tepatWaktu}%), Presensi masuk dengan catatan terlambat ada ${chartPresentModel!.countLate} orang (${chartPresentModel!.terlambat}%) dan yang tidak melakukan presensi pada hari ini ada ${chartPresentModel!.countNotprecent} orang (${chartPresentModel!.tidakHadir}%)',
                                    style: const TextStyle(
                                        color: Colors.white,
                                        height: 1.3,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  const SizedBox(
                                    height: 7,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
          const SizedBox(
            height: 20,
          ),
          _loadingpie
              ? const SizedBox()
              : chartPresentModel!.countLate == 0 &&
                      chartPresentModel!.countNotprecent == 0 &&
                      chartPresentModel!.countOntime == 0
                  ? SizedBox()
                  : const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 12),
                      child: Text(
                        'Detail',
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.bold,
                          color: black2Color,
                        ),
                      ),
                    ),
          _loadingpie
              ? const SizedBox()
              : chartPresentModel!.countLate == 0 &&
                      chartPresentModel!.countNotprecent == 0 &&
                      chartPresentModel!.countOntime == 0
                  ? const SizedBox()
                  : _loading
                      ? SizedBox(
                          height: 200,
                          child: Center(
                            child: SizedBox(
                              height: 100,
                              width: MediaQuery.of(context).size.width,
                              child: Lottie.asset(
                                "assets/lottie/loading.json",
                              ),
                            ),
                          ),
                        )
                      : attandanceEmployeeList.isEmpty
                          ? SizedBox(
                              height: 200,
                              child: Center(
                                child: SizedBox(
                                  height: 100,
                                  width: MediaQuery.of(context).size.width,
                                  child: Lottie.asset(
                                    "assets/lottie/empty-box-blue.json",
                                  ),
                                ),
                              ),
                            )
                          : ListView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: attandanceEmployeeList.length,
                              itemBuilder: (BuildContext context, int index) {
                                AttandanceEmployeeModel
                                    attandanceEmployeeModel =
                                    attandanceEmployeeList[index];
                                String dateCekIn = DateFormat('HH:mm:ss')
                                    .format(attandanceEmployeeModel.checkIn);
                                return attandanceEmployeeModel.islate !=
                                        "Tidak Hadir"
                                    ? Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 12),
                                        child: ItemAttandanceEmployee(
                                          imageUrl: attandanceEmployeeModel
                                              .imageCheckIn,
                                          startTime: dateCekIn,
                                          name:
                                              '${attandanceEmployeeModel.firstName} ${attandanceEmployeeModel.lastName}',
                                          color:
                                              attandanceEmployeeModel.islate ==
                                                      'Terlambat'
                                                  ? orangeColor
                                                  : greenColor,
                                          isLate:
                                              attandanceEmployeeModel.islate,
                                        ),
                                      )
                                    : const SizedBox();
                              },
                            ),
        ],
      ),
    );
  }
}
