import 'dart:io';

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/attandance_model.dart';
import 'package:attandy_app/screens/account/change_password.dart';
import 'package:attandy_app/screens/account/change_profile.dart';
import 'package:attandy_app/screens/account/component/absensi_where_month.dart';
import 'package:attandy_app/screens/account/component/change_personal_data.dart';
import 'package:attandy_app/screens/account/component/image_profile.dart';
import 'package:attandy_app/screens/account/component/item_present_profile.dart';
import 'package:attandy_app/screens/account/component/item_setting_profile.dart';
import 'package:attandy_app/screens/account/component/workshift_profile.dart';
import 'package:attandy_app/screens/account/view_profile.dart';
import 'package:attandy_app/screens/shimmer/shimmer_count_presensi.dart';
import 'package:attandy_app/screens/splash_screen.dart';
import 'package:attandy_app/screens/widget/custom_shimmer.dart';
import 'package:attandy_app/services/attandance_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constants.dart';
import '../../models/admission_model.dart';
import '../../models/api_response_model.dart';
import '../../models/workshift_model.dart';
import '../../services/admission_service.dart';
import '../../services/auth_service.dart';
import '../../services/workshift_service.dart';
import '../shimmer/shimmer_workshift_profile.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({super.key});

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  String? _date;
  WorkshiftModel? workshiftModel;
  String firstName = '';
  String lastName = '';
  String position = '';
  AdmissionModel? admissionModel;

  File? _imageCamera;
  final _picker = ImagePicker();

  String _formatDate(DateTime dateTime) {
    return DateFormat('MMMM', "id_ID").format(dateTime);
  }

  String _formatMount(DateTime dateTime) {
    return DateFormat('MM', "id_ID").format(dateTime);
  }

  loadPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      firstName = (prefs.getString('firstName') ?? '');
      lastName = (prefs.getString('lastName') ?? '');
      position = (prefs.getString('position') ?? '');
    });
  }

  removePred() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.remove("chekin");
    preferences.remove("firstName");
    preferences.remove("lastName");
    preferences.remove("position");
    preferences.remove("imageUrl");
    preferences.remove("status");
    preferences.remove("chekin");
    preferences.remove("role");
    preferences.commit();
  }

  Future getGalery() async {
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _imageCamera = File(pickedFile.path);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeProfile(
                      imageFile: _imageCamera!,
                    )));
      });
    }
  }

  Future getCamera() async {
    final pickedFile = await _picker.getImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() async {
        _imageCamera = File(pickedFile.path);
        String refresh = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeProfile(
                      imageFile: _imageCamera!,
                    )));
        if (refresh == 'refresh') {
          functionGetWorkshift();
          functionGetAdmission();
          loadPref();
        }
      });
    }
  }

  void functionGetAdmission() async {
    ApiResponse response = await getAdmission();
    if (response.error == null) {
      setState(() {
        admissionModel = response.data as AdmissionModel;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void functionGetWorkshift() async {
    ApiResponse response = await getWorkshiftAuth();
    if (response.error == null) {
      setState(() {
        workshiftModel = response.data as WorkshiftModel;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  CountAttandanceModel? countAttandanceModel;
  void fungsiGetCountAttandance() async {
    ApiResponse response = await getCountAttandance(
        mounth: _formatMount(DateTime.now()).toString());
    if (response.error == null) {
      setState(() {
        countAttandanceModel = response.data as CountAttandanceModel;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  @override
  void initState() {
    _date = _formatDate(DateTime.now());
    functionGetWorkshift();
    fungsiGetCountAttandance();
    functionGetAdmission();
    loadPref();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Profil",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              admissionModel == null
                  ? Container(
                      height: 200,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15),
                        child: Column(
                          children: [
                            const CustomShimmer(
                                height: 120, width: 120, radius: 60),
                            const SizedBox(
                              height: 10,
                            ),
                            CustomShimmer(
                                height: 15,
                                width: MediaQuery.of(context).size.width * 0.5,
                                radius: 12),
                            const SizedBox(
                              height: 5,
                            ),
                            CustomShimmer(
                                height: 15,
                                width: MediaQuery.of(context).size.width * 0.4,
                                radius: 12),
                          ],
                        ),
                      ),
                    )
                  : ImageProfile(
                      name:
                          '${admissionModel!.data.firstName} ${admissionModel!.data.lastName}',
                      role: 'NIP : ${admissionModel!.data.nip}',
                      imageFile: _imageCamera,
                      imageUrl: admissionModel!.data.image,
                      view: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ViewProfil(
                              imageUrl: admissionModel!.data.image,
                            ),
                          ),
                        );
                      },
                      press: () {
                        showModalBottomSheet(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          context: context,
                          builder: (builder) {
                            return Container(
                              height: 170.0,
                              color: Colors
                                  .transparent, //could change this to Color(0xFF737373),
                              child: Container(
                                  decoration: const BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10.0),
                                          topRight: Radius.circular(10.0))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            const Text(
                                              'Foto Profil',
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: black2Color,
                                                fontFamily: 'PoppinsReguler',
                                                fontWeight: FontWeight.w700,
                                                letterSpacing: 1,
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                Navigator.pop(context);
                                              },
                                              child: const SizedBox(
                                                height: 30,
                                                width: 30,
                                                child: Icon(Icons.close),
                                              ),
                                            )
                                          ],
                                        ),
                                        const SizedBox(
                                          height: 15,
                                        ),
                                        Row(
                                          children: [
                                            const SizedBox(
                                              width: 15,
                                            ),
                                            ItemFotoProfil(
                                              icons: Icons.camera_alt,
                                              press: () {
                                                setState(() {
                                                  getCamera();
                                                });
                                              },
                                              title: 'Kamera',
                                            ),
                                            const SizedBox(
                                              width: 15,
                                            ),
                                            ItemFotoProfil(
                                              icons: Icons.image,
                                              press: () {
                                                setState(() {
                                                  getGalery();
                                                });
                                              },
                                              title: 'Galeri',
                                            ),
                                            const SizedBox(
                                              width: 15,
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  )),
                            );
                          },
                        );
                      }),
              const SizedBox(
                height: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  admissionModel == null
                      ? SizedBox(
                          height: 225,
                          width: MediaQuery.of(context).size.width,
                          child: Center(
                            child: SizedBox(
                              height: 100,
                              width: MediaQuery.of(context).size.width,
                              child: Lottie.asset(
                                "assets/lottie/loading.json",
                              ),
                            ),
                          ),
                        )
                      : admissionModel!.data.position ==
                              'Chief Executive Officer'
                          ? const SizedBox()
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                workshiftModel == null
                                    ? const ShimmerWorkshiftProfile()
                                    : WorkshiftProfile(
                                        masuk: workshiftModel!.workshiftId == 1
                                            ? 'Belum Ditentukan'
                                            : workshiftModel!.startTime,
                                        keluar: workshiftModel!.workshiftId == 1
                                            ? 'Belum Ditentukan'
                                            : workshiftModel!.endTime,
                                      ),
                                const SizedBox(
                                  height: 15,
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                AbsensiWhereMonth(
                                                  nameMonth: _formatDate(
                                                          DateTime.now())
                                                      .toString(),
                                                  numberMonth: _formatMount(
                                                          DateTime.now())
                                                      .toString(),
                                                )));
                                  },
                                  child: Container(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Absensi Bulan $_date",
                                          style: const TextStyle(
                                            fontSize: 18,
                                            color: black2Color,
                                            fontFamily: 'Roboto',
                                            fontWeight: FontWeight.w700,
                                            letterSpacing: 1,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 25,
                                          width: 25,
                                          child:
                                              Icon(CupertinoIcons.arrow_right),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    ItemPresentProfile(
                                      colors: greenColor,
                                      number: countAttandanceModel == null
                                          ? '0'
                                          : countAttandanceModel!.tepatWaktu
                                              .toString(),
                                      title: 'Tepat Waktu',
                                    ),
                                    ItemPresentProfile(
                                      colors: yellowColor,
                                      number: countAttandanceModel == null
                                          ? '0'
                                          : countAttandanceModel!.terlambat
                                              .toString(),
                                      title: 'Terlambat',
                                    ),
                                    ItemPresentProfile(
                                      colors: orangeColor,
                                      number: countAttandanceModel == null
                                          ? '0'
                                          : countAttandanceModel!.tidakHadir
                                              .toString(),
                                      title: 'Tidak Hadir',
                                    ),
                                  ],
                                ),
                              ],
                            ),
                  const SizedBox(
                    height: 15,
                  ),
                  const Text(
                    "Pengaturan",
                    style: TextStyle(
                      fontSize: 18,
                      color: black2Color,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                      letterSpacing: 1,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ItemSettingProfile(
                    icons: CupertinoIcons.person,
                    press: () async {
                      String refresh = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChangePersonalData(
                                    admissionModel: admissionModel!,
                                  )));
                      if (refresh == 'refresh') {
                        functionGetWorkshift();
                        functionGetAdmission();
                        loadPref();
                      }
                    },
                    titile: 'Ubah Data Diri',
                  ),
                  ItemSettingProfile(
                    icons: CupertinoIcons.lock,
                    press: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ChangePassword()));
                    },
                    titile: 'Atur Ulang Kata Sandi',
                  ),
                  ItemSettingProfile(
                    icons: CupertinoIcons.arrow_right_square,
                    press: () {
                      Alert(
                        context: context,
                        type: AlertType.warning,
                        desc: "Apakah anda yakin ingin keluar?",
                        buttons: [
                          DialogButton(
                            color: blueColor,
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            width: 120,
                            child: const Text(
                              "Batal",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                          DialogButton(
                            color: redColor,
                            onPressed: () {
                              setState(() {
                                removePred();
                              });
                              logout().then((value) => Navigator.of(context)
                                  .pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const SplashScreen()),
                                      (route) => false));
                            },
                            width: 120,
                            child: const Text(
                              "Keluar",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          )
                        ],
                      ).show();
                    },
                    titile: 'Keluar',
                  ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ItemFotoProfil extends StatelessWidget {
  const ItemFotoProfil({
    Key? key,
    required this.title,
    required this.press,
    required this.icons,
  }) : super(key: key);

  final String title;
  final IconData icons;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        children: [
          Container(
            height: 70,
            width: 70,
            decoration: BoxDecoration(
                color: blueColor.withOpacity(0.2),
                borderRadius: BorderRadius.circular(35)),
            child: Center(
              child: SizedBox(
                height: 70,
                width: 70,
                child: Center(
                  child: Icon(
                    icons,
                    color: blueColor,
                    size: 40,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            title,
            style: const TextStyle(
              fontSize: 11,
              color: black2Color,
              fontFamily: 'PoppinsReguler',
              fontWeight: FontWeight.w700,
              letterSpacing: 1,
            ),
          ),
        ],
      ),
    );
  }
}
