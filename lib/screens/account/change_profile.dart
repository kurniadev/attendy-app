// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:attandy_app/services/auth_service.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../splash_screen.dart';
import '../widget/custom_loading.dart';

class ChangeProfile extends StatefulWidget {
  const ChangeProfile({super.key, required this.imageFile});

  final File imageFile;

  @override
  State<ChangeProfile> createState() => _ChangeProfileState();
}

class _ChangeProfileState extends State<ChangeProfile> {
  bool loading = true;

  void fungsiUpdate() async {
    showAlertDialog(context);
    String? image = getStringImage(widget.imageFile);
    ApiResponse response = await updateFoto(image: image);
    if (response.error == null) {
      setState(() {
        loading = false;
        Navigator.pop(context);
        Navigator.pop(context);
        Navigator.pop(context, 'refresh');
      });
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Perbarui Foto Profil')));
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 36, 36, 36),
      appBar: AppBar(
        title: const Text(
          "Edit Foto Profil",
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 36, 36, 36),
        iconTheme: const IconThemeData(
          color: Colors.white,
        ),
        actions: [
          GestureDetector(
            onTap: () {
              fungsiUpdate();
            },
            child: const SizedBox(
              height: 20,
              width: 50,
              child: Icon(
                Icons.done,
                size: 30,
              ),
            ),
          )
        ],
        elevation: 0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 500,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: FileImage(widget.imageFile), fit: BoxFit.cover),
            ),
          )
        ],
      ),
    );
  }
}
