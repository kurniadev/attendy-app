// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../widget/custom_loading.dart';

class ChangePassword extends StatefulWidget {
  const ChangePassword({super.key});

  @override
  State<ChangePassword> createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  bool loading = true;
  TextEditingController oldPass = TextEditingController();
  TextEditingController newPass = TextEditingController();
  TextEditingController newPassConfirmation = TextEditingController();
  void fungsiChangePassword() async {
    showAlertDialog(context);
    ApiResponse response = await changePassword(
        oldPass: oldPass.text,
        newPass: newPass.text,
        newPassConfirmation: newPassConfirmation.text);
    if (response.error == null) {
      setState(() {
        loading = false;
      });
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
      AnimatedSnackBar.material('Berhasil Ubah Kata Sandi Silahkan Login Ulang',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      Navigator.pop(context);
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.warning,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: SizedBox(
                    height: 300,
                    child: Lottie.asset(
                      "assets/lottie/changepass.json",
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  'Atur Ulang Kata Sandi',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Poppins',
                    color: Color(0xff4B556B),
                    letterSpacing: 1,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      SizedBox(
                        height: 30,
                        width: 30,
                        child: Image.asset(
                          'assets/Icons/password.png',
                          color: black2Color.withOpacity(0.8),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: SizedBox(
                          // height: 35,
                          child: TextFormField(
                            controller: oldPass,
                            style: const TextStyle(
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold),
                            decoration: InputDecoration(
                              // border: InputBorder.none,
                              hintText: 'Masukkan kata sandi lama',
                              hintStyle: TextStyle(
                                  fontSize: 14.0,
                                  fontFamily: 'PoppinsReguler',
                                  color: black2Color.withOpacity(0.7),
                                  letterSpacing: 1),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      SizedBox(
                        height: 30,
                        width: 30,
                        child: Image.asset(
                          'assets/Icons/password.png',
                          color: black2Color.withOpacity(0.8),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: SizedBox(
                          // height: 35,
                          child: TextFormField(
                            controller: newPass,
                            style: const TextStyle(
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold),
                            decoration: InputDecoration(
                              // border: InputBorder.none,
                              hintText: 'Masukkan kata sandi baru',
                              hintStyle: TextStyle(
                                  fontSize: 14.0,
                                  fontFamily: 'PoppinsReguler',
                                  color: black2Color.withOpacity(0.7),
                                  letterSpacing: 1),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      SizedBox(
                        height: 30,
                        width: 30,
                        child: Image.asset(
                          'assets/Icons/password.png',
                          color: black2Color.withOpacity(0.8),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: SizedBox(
                          // height: 35,
                          child: TextFormField(
                            controller: newPassConfirmation,
                            style: const TextStyle(
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold),
                            decoration: InputDecoration(
                              // border: InputBorder.none,
                              hintText: 'Masukkan konfirmasi kata sandi baru',
                              hintStyle: TextStyle(
                                  fontSize: 14.0,
                                  fontFamily: 'PoppinsReguler',
                                  color: black2Color.withOpacity(0.7),
                                  letterSpacing: 1),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8, bottom: 24),
                  child: GestureDetector(
                      onTap: () {
                        if (oldPass.text == '') {
                          AnimatedSnackBar.material(
                                  'Kata Sandi Lama Harus Diisi',
                                  type: AnimatedSnackBarType.warning,
                                  mobileSnackBarPosition:
                                      MobileSnackBarPosition.top,
                                  desktopSnackBarPosition:
                                      DesktopSnackBarPosition.topCenter)
                              .show(context);
                        } else if (newPass.text == '') {
                          AnimatedSnackBar.material(
                                  'Kata Sandi Baru Harus Diisi',
                                  type: AnimatedSnackBarType.warning,
                                  mobileSnackBarPosition:
                                      MobileSnackBarPosition.top,
                                  desktopSnackBarPosition:
                                      DesktopSnackBarPosition.topCenter)
                              .show(context);
                        } else if (newPassConfirmation.text == '') {
                          AnimatedSnackBar.material(
                                  'Kata Sandi Baru Harus Diisi',
                                  type: AnimatedSnackBarType.warning,
                                  mobileSnackBarPosition:
                                      MobileSnackBarPosition.top,
                                  desktopSnackBarPosition:
                                      DesktopSnackBarPosition.topCenter)
                              .show(context);
                        } else {
                          fungsiChangePassword();
                        }
                      },
                      child: Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: const Color.fromARGB(255, 28, 73, 163),
                              borderRadius: BorderRadius.circular(12)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Text(
                                'Simpan',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ],
                          ))),
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
