import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';

class ItemSettingProfile extends StatefulWidget {
  const ItemSettingProfile(
      {super.key,
      required this.titile,
      required this.press,
      required this.icons});

  final String titile;
  final VoidCallback press;
  final IconData icons;

  @override
  State<ItemSettingProfile> createState() => _ItemSettingProfileState();
}

class _ItemSettingProfileState extends State<ItemSettingProfile> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: GestureDetector(
        onTap: widget.press,
        child: Container(
          height: 50,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Row(
            children: [
              SizedBox(
                height: 40,
                width: 40,
                child: Icon(
                  widget.icons,
                  color: black2Color,
                ),
              ),
              Text(
                widget.titile,
                style: const TextStyle(
                  fontSize: 12,
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w700,
                  color: black2Color,
                  letterSpacing: 0.5,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
