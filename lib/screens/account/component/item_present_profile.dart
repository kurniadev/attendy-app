import 'package:flutter/material.dart';

import '../../../constants.dart';

class ItemPresentProfile extends StatefulWidget {
  const ItemPresentProfile(
      {super.key,
      required this.title,
      required this.number,
      required this.colors});

  final String title, number;
  final Color colors;

  @override
  State<ItemPresentProfile> createState() => _ItemPresentProfileState();
}

class _ItemPresentProfileState extends State<ItemPresentProfile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: 110,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(110),
                color: widget.colors.withOpacity(0.2)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.number,
                  style: TextStyle(
                    fontSize: 14,
                    fontFamily: "Poppins",
                    fontWeight: FontWeight.w700,
                    color: widget.colors,
                    letterSpacing: 0.5,
                  ),
                ),
                Text(
                  'Hari',
                  style: TextStyle(
                    fontSize: 14,
                    fontFamily: "Poppins",
                    fontWeight: FontWeight.w700,
                    color: widget.colors,
                    letterSpacing: 0.5,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            widget.title,
            style: const TextStyle(
              fontSize: 12,
              fontFamily: "Poppins",
              fontWeight: FontWeight.w700,
              color: black2Color,
              letterSpacing: 0.5,
            ),
          ),
        ],
      ),
    );
  }
}
