// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';

import '../../../constants.dart';
import '../../../models/api_response_model.dart';
import '../../../models/attandance_model.dart';
import '../../../services/attandance_service.dart';
import '../../../services/auth_service.dart';
import '../../dashboard/component/dashboard_last_present.dart';
import '../../splash_screen.dart';

class AbsensiWhereMonth extends StatefulWidget {
  const AbsensiWhereMonth(
      {super.key, required this.numberMonth, required this.nameMonth});

  final String numberMonth, nameMonth;

  @override
  State<AbsensiWhereMonth> createState() => _AbsensiWhereMonthState();
}

class _AbsensiWhereMonthState extends State<AbsensiWhereMonth> {
  String _formatDate(DateTime dateTime) {
    return DateFormat('MMMM', "id_ID").format(dateTime);
  }

  bool _loading = true;
  List<dynamic> historyAllAttandancetList = [];

  Future<void> fungsiGetHistoryAttandanceAll() async {
    ApiResponse response =
        await getHistoryAttandanceMonth(mounth: widget.numberMonth);
    if (response.error == null) {
      setState(() {
        historyAllAttandancetList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  @override
  void initState() {
    fungsiGetHistoryAttandanceAll();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: Text(
          "Absensi Bulan ${widget.nameMonth} ",
          style: const TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: _loading
          ? Container(
              height: 100,
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Center(
                child: SizedBox(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  child: Lottie.asset(
                    "assets/lottie/loading.json",
                  ),
                ),
              ),
            )
          : historyAllAttandancetList.isEmpty
              ? Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Center(
                    child: SizedBox(
                      height: 100,
                      width: MediaQuery.of(context).size.width,
                      child: Lottie.asset(
                        "assets/lottie/empty-box-blue.json",
                      ),
                    ),
                  ),
                )
              : ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: historyAllAttandancetList.length,
                  itemBuilder: (BuildContext context, int index) {
                    HistoryAllAttandanceModel historyAllAttandanceModel =
                        historyAllAttandancetList[index];

                    String checkOut = '';
                    String dateCekIn = DateFormat('HH:mm:ss')
                        .format(historyAllAttandanceModel.checkIn);
                    if (historyAllAttandanceModel.checkOut != null) {
                      String dateCekOut = DateFormat('HH:mm:ss').format(
                          DateTime.parse(historyAllAttandanceModel.checkOut));
                      checkOut = dateCekOut;
                    }
                    return historyAllAttandanceModel.islate == '-'
                        ? Container(
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: DahsboardNoPresent(
                                date: _formatDate(
                                    historyAllAttandanceModel.checkIn),
                              ),
                            ))
                        : Container(
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: DahsboardLastPresent(
                                date: _formatDate(
                                    historyAllAttandanceModel.checkIn),
                                checkIn: dateCekIn,
                                checkOut: checkOut,
                                imageCheckIn:
                                    historyAllAttandanceModel.imageCheckIn,
                                imageOut:
                                    historyAllAttandanceModel.imageCheckOut,
                                isLate: historyAllAttandanceModel.islate,
                                text: historyAllAttandanceModel.islate ==
                                        'Terlambat'
                                    ? redColor
                                    : greenColor,
                              ),
                            ));
                  },
                ),
    );
  }
}
