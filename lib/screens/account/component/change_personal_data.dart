// ignore_for_file: use_build_context_synchronously

import 'package:attandy_app/screens/widget/custom_button.dart';
import 'package:attandy_app/services/admission_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../constants.dart';
import '../../../models/admission_model.dart';
import '../../../models/api_response_model.dart';
import '../../../services/auth_service.dart';
import '../../splash_screen.dart';
import '../../widget/custom_input_field.dart';
import '../../widget/custom_loading.dart';
import '../../widget/custom_shimmer.dart';
import '../../widget/custom_title_input_field.dart';

class ChangePersonalData extends StatefulWidget {
  const ChangePersonalData({super.key, required this.admissionModel});

  final AdmissionModel admissionModel;

  @override
  State<ChangePersonalData> createState() => _ChangePersonalDataState();
}

class _ChangePersonalDataState extends State<ChangePersonalData> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController phone = TextEditingController();

  void getData() {
    firstName.text = widget.admissionModel.data.firstName;
    lastName.text = widget.admissionModel.data.lastName;
    address.text = widget.admissionModel.data.address;
    phone.text = widget.admissionModel.data.phone;
  }

  bool _loading = true;
  void fungsiUpdate() async {
    showAlertDialog(context);
    ApiResponse response = await updateDataEmployee(
        firstName: firstName.text,
        lastName: lastName.text,
        address: address.text,
        phone: phone.text);
    if (response.error == null) {
      setState(() {
        _loading = false;
        Navigator.pop(context);
        Navigator.pop(context, 'refresh');
      });
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Perbarui Data')));
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Ubah Data Diri",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _key,
            child: Column(
              children: [
                // SizedBox(
                //   height: 270,
                //   child: Lottie.asset(
                //     "assets/lottie/regg.json",
                //   ),
                // ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        widget.admissionModel.data.image != null
                            ? Center(
                                child: CachedNetworkImage(
                                  imageUrl: widget.admissionModel.data.image,
                                  fit: BoxFit.cover,
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    width: 120,
                                    height: 120,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(60),
                                      image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                  placeholder: (context, url) =>
                                      const CustomShimmer(
                                          height: 120, width: 120, radius: 60),
                                  errorWidget: (context, url, error) =>
                                      const Icon(Icons.error),
                                ),
                              )
                            : Center(
                                child: Container(
                                  height: 120,
                                  width: 120,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(60),
                                      image: const DecorationImage(
                                          image: AssetImage(
                                              "assets/image/image-not-available.jpg"),
                                          fit: BoxFit.cover)),
                                ),
                              ),
                        const SizedBox(
                          height: 20,
                        ),
                        const CustomTitleInputField(
                          title: 'Nama Lengkap',
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: CustomInputField(
                                controller: firstName,
                                maxLine: 1,
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: CustomInputField(
                                controller: lastName,
                                maxLine: 1,
                              ),
                            ),
                          ],
                        ),
                        const CustomTitleInputField(
                          title: 'Alamat Tinggal',
                        ),
                        CustomInputField(
                          controller: address,
                          maxLine: 4,
                        ),
                        const CustomTitleInputField(
                          title: 'Nomor Telepon',
                        ),
                        CustomInputField(
                          controller: phone,
                          maxLine: 1,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CustomButton(
                            press: () {
                              fungsiUpdate();
                            },
                            title: 'Perbarui')
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
