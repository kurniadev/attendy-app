import 'dart:io';

import 'package:attandy_app/screens/widget/custom_shimmer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../../constants.dart';

class ImageProfile extends StatefulWidget {
  const ImageProfile({
    super.key,
    required this.name,
    required this.role,
    required this.press,
    required this.view,
    required this.imageFile,
    required this.imageUrl,
  });

  final String name, role;
  final VoidCallback press, view;
  final dynamic imageFile, imageUrl;

  @override
  State<ImageProfile> createState() => _ImageProfileState();
}

class _ImageProfileState extends State<ImageProfile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.view,
      child: Container(
        height: 200,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: SizedBox(
                height: 120,
                width: 120,
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    SizedBox(
                      width: 120,
                      height: 120,
                      child: widget.imageFile == null
                          ? widget.imageUrl != null
                              ? CachedNetworkImage(
                                  imageUrl: widget.imageUrl,
                                  fit: BoxFit.cover,
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    width: 120.0,
                                    height: 120.0,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(120),
                                      image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                  placeholder: (context, url) =>
                                      const CustomShimmer(
                                          height: 120, width: 120, radius: 120),
                                  errorWidget: (context, url, error) =>
                                      const Icon(Icons.error),
                                )
                              : Container(
                                  height: 120,
                                  width: 120,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(120),
                                      image: const DecorationImage(
                                          image: AssetImage(
                                              "assets/image/image-not-available.jpg"),
                                          fit: BoxFit.cover)),
                                )
                          : Container(
                              height: 120,
                              width: 120,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(120),
                                image: DecorationImage(
                                    image:
                                        FileImage(widget.imageFile ?? File('')),
                                    fit: BoxFit.cover),
                              ),
                            ),
                    ),
                    Positioned(
                      right: -5,
                      bottom: -2,
                      child: GestureDetector(
                        onTap: widget.press,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            color: blueColor,
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: Icon(
                            Icons.camera_alt,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Text(
              widget.name,
              style: const TextStyle(
                color: black2Color,
                fontFamily: "Poppins",
                fontWeight: FontWeight.w700,
                fontSize: 16,
              ),
            ),
            Text(
              widget.role,
              style: const TextStyle(
                color: redColor,
                fontFamily: "PoppinsReguler",
                fontWeight: FontWeight.w600,
                fontSize: 14,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
