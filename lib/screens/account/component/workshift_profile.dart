import 'package:flutter/material.dart';

import '../../../constants.dart';

class WorkshiftProfile extends StatefulWidget {
  const WorkshiftProfile(
      {super.key, required this.masuk, required this.keluar});

  final String masuk, keluar;

  @override
  State<WorkshiftProfile> createState() => _WorkshiftProfileState();
}

class _WorkshiftProfileState extends State<WorkshiftProfile> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 80,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Absensi Masuk",
                  style: TextStyle(
                    color: black2Color,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w700,
                    fontSize: 14,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  widget.masuk,
                  style: const TextStyle(
                    color: blueColor,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w700,
                    fontSize: 14,
                  ),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          width: 15,
        ),
        Expanded(
          child: Container(
            height: 80,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Absensi Keluar",
                  style: TextStyle(
                    color: black2Color,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w700,
                    fontSize: 14,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  widget.keluar,
                  style: const TextStyle(
                    color: redColor,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w700,
                    fontSize: 14,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
