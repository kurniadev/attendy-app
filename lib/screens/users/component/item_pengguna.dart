import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../widget/custom_shimmer.dart';

class ItemPengguna extends StatefulWidget {
  ItemPengguna({
    super.key,
    required this.image,
    required this.name,
    required this.email,
    required this.position,
    required this.imageUrl,
    required this.ontaps,
  });

  final String image, name, email, position;
  final dynamic imageUrl;
  final VoidCallback ontaps;

  @override
  State<ItemPengguna> createState() => _ItemPenggunaState();
}

class _ItemPenggunaState extends State<ItemPengguna> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Container(
        height: 70,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 5,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          children: [
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: widget.imageUrl != null
                    ? CachedNetworkImage(
                        imageUrl: widget.imageUrl,
                        fit: BoxFit.cover,
                        imageBuilder: (context, imageProvider) => Container(
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            image: DecorationImage(
                                image: imageProvider, fit: BoxFit.cover),
                          ),
                        ),
                        placeholder: (context, url) => const CustomShimmer(
                            height: 50, width: 50, radius: 25),
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.error),
                      )
                    : Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            image: const DecorationImage(
                                image: AssetImage(
                                    "assets/image/image-not-available.jpg"),
                                fit: BoxFit.cover)),
                      )),
            Expanded(
              child: SizedBox(
                height: 60,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      widget.name,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontSize: 16,
                        color: black2Color,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1,
                      ),
                    ),
                    Text(
                      widget.email,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 12,
                          fontFamily: "Roboto",
                          color: black2Color.withOpacity(0.8),
                          fontWeight: FontWeight.w600,
                          letterSpacing: 0.5),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      widget.position,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 10,
                        color: black2Color.withOpacity(0.7),
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: widget.ontaps,
              child: SizedBox(
                width: 40,
                height: 30,
                child: Image.asset('assets/Icons/three-dots.png'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
