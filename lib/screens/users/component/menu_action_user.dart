import 'package:flutter/material.dart';

import '../../../constants.dart';

class MenuActionUser extends StatefulWidget {
  const MenuActionUser(
      {super.key, required this.changedata, required this.changepassword});

  final VoidCallback changedata, changepassword;

  @override
  State<MenuActionUser> createState() => _MenuActionUserState();
}

class _MenuActionUserState extends State<MenuActionUser> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0))),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Container(
                height: 5,
                width: 50,
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.35),
                    borderRadius: BorderRadius.circular(12)),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            GestureDetector(
              onTap: widget.changepassword,
              child: SizedBox(
                height: 40,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: const [
                    Icon(
                      Icons.lock,
                      size: 20,
                      color: black2Color,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Reset Kata Sandi Karyawan",
                      style: TextStyle(
                        fontSize: 16,
                        color: black2Color,
                        fontFamily: 'PoppinsReguler',
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: widget.changedata,
              child: SizedBox(
                height: 40,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: const [
                    Icon(
                      Icons.person_sharp,
                      size: 20,
                      color: black2Color,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Ubah Data Karyawan",
                      style: TextStyle(
                        fontSize: 16,
                        color: black2Color,
                        fontFamily: 'PoppinsReguler',
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
