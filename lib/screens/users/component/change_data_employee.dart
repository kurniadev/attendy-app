// ignore_for_file: use_build_context_synchronously

import 'dart:convert';

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/admission_model.dart';
import 'package:attandy_app/services/admission_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../constants.dart';
import '../../../models/api_response_model.dart';
import '../../../models/employee_model.dart';
import '../../../services/auth_service.dart';
import '../../widget/custom_button.dart';
import '../../widget/custom_input_field.dart';
import 'package:http/http.dart' as http;
import '../../widget/custom_loading.dart';
import '../../widget/custom_shimmer.dart';
import '../../widget/custom_title_input_field.dart';

class ChangeDataEmployee extends StatefulWidget {
  const ChangeDataEmployee({super.key, required this.employeeModel});

  final EmployeeModel employeeModel;

  @override
  State<ChangeDataEmployee> createState() => _ChangeDataEmployeeState();
}

class _ChangeDataEmployeeState extends State<ChangeDataEmployee> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController nip = TextEditingController();
  TextEditingController nik = TextEditingController();
  TextEditingController placeOfBirth = TextEditingController();
  TextEditingController dateOfBirth = TextEditingController();
  String? gender;
  String? religion;
  String? position;

  bool _loading = false;

  final List<String> items = [
    'Laki-Laki',
    'Perempuan',
  ];
  final List<String> itemsAgama = [
    'Islam',
    'Kristen',
    'Katolik',
    'Hindu',
    'Budha',
    'Konghucu',
  ];
  final List<String> itemsPosisi = [
    'Chief Executive Officer (CEO)',
    'Chief Marketing Officer (CMO)',
    'Chief Oprational Officer (COO)',
    'General Affair',
    'Marketing Staff',
    'Network Engineer Staff',
    'Electronics Engineer Staff',
  ];
  var valueWorkshift;
  List workshiftList = [];

  Future getWorkShift() async {
    print('jalan');
    String token = await getToken();
    final response = await http.get(
      Uri.parse('$baseURL/masterdata/workshift/'),
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'Bearer $token'
      },
    );
    print(response.body);
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body)['data'];
      setState(() {
        workshiftList = jsonData;
        valueWorkshift = widget.employeeModel.workshiftId;
      });
    }
  }

  void fungsiChangeDataEmployee({required int idEmployee}) async {
    showAlertDialog(context);
    print(
      dateOfBirth.text,
    );
    ApiResponse response = await changeDataEmployee(
        idEmployee: idEmployee.toString(),
        firstName: firstName.text,
        lastName: lastName.text,
        nik: nik.text,
        nip: nip.text,
        placeOfBirth: placeOfBirth.text,
        dateOfBirth: dateOfBirth.text,
        gender: gender.toString(),
        religion: religion.toString(),
        address: address.text,
        position: position.toString(),
        phone: phone.text,
        workshiftId: valueWorkshift.toString());
    if (response.error == null) {
      Navigator.pop(context);
      // changePassword.clear();
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Data Karyawan Berhasil Diperbarui')));
      setState(() {
        Navigator.pop(context, 'refresh');
        Navigator.pop(context);
      });
    } else {
      AnimatedSnackBar.material('Ada Kesalahan',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  String _formatDate(DateTime dateTime) {
    return DateFormat('yyyy-MM-dd').format(dateTime);
  }

  void getData() {
    firstName.text = widget.employeeModel.firstName;
    lastName.text = widget.employeeModel.lastName;
    address.text = widget.employeeModel.address;
    phone.text = widget.employeeModel.phone;
    gender = widget.employeeModel.gender;
    religion = widget.employeeModel.religion;
    position = widget.employeeModel.position;
    nip.text = widget.employeeModel.nip;
    nik.text = widget.employeeModel.nik;
    placeOfBirth.text = widget.employeeModel.placeOfBirth;
    dateOfBirth.text = _formatDate(widget.employeeModel.dateOfBirth);
  }

  @override
  void initState() {
    getData();
    if (widget.employeeModel.workshiftId == 1) {
      // getWorkShift();
    } else {
      getWorkShift();
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Ubah Data Diri",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _key,
            child: Column(
              children: [
                // SizedBox(
                //   height: 270,
                //   child: Lottie.asset(
                //     "assets/lottie/regg.json",
                //   ),
                // ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        widget.employeeModel.image != null
                            ? Center(
                                child: CachedNetworkImage(
                                  imageUrl: widget.employeeModel.image,
                                  fit: BoxFit.cover,
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    width: 120,
                                    height: 120,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(60),
                                      image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                  placeholder: (context, url) =>
                                      const CustomShimmer(
                                          height: 120, width: 120, radius: 60),
                                  errorWidget: (context, url, error) =>
                                      const Icon(Icons.error),
                                ),
                              )
                            : Center(
                                child: Container(
                                  height: 120,
                                  width: 120,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(60),
                                      image: const DecorationImage(
                                          image: AssetImage(
                                              "assets/image/image-not-available.jpg"),
                                          fit: BoxFit.cover)),
                                ),
                              ),
                        const SizedBox(
                          height: 20,
                        ),
                        const CustomTitleInputField(
                          title: 'Nama Lengkap',
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: CustomInputField(
                                controller: firstName,
                                maxLine: 1,
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: CustomInputField(
                                controller: lastName,
                                maxLine: 1,
                              ),
                            ),
                          ],
                        ),
                        const CustomTitleInputField(
                          title: 'Alamat Tinggal',
                        ),
                        CustomInputField(
                          controller: address,
                          maxLine: 4,
                        ),
                        const CustomTitleInputField(
                          title: 'Nomor Telepon',
                        ),
                        CustomInputField(
                          controller: phone,
                          maxLine: 1,
                        ),
                        const CustomTitleInputField(
                          title: 'NIK',
                        ),
                        CustomInputField2(
                          controller: nik,
                          maxLine: 1,
                          maxLength: 16,
                        ),
                        const CustomTitleInputField(
                          title: 'NIP',
                        ),
                        CustomInputField2(
                          controller: nip,
                          maxLine: 1,
                          maxLength: 16,
                        ),
                        const CustomTitleInputField(
                          title: 'Tempat Lahir',
                        ),
                        CustomInputField(
                          controller: placeOfBirth,
                          maxLine: 1,
                        ),
                        const CustomTitleInputField(
                          title: 'Tanggal Lahir',
                        ),
                        CustomInputFieldTgl(
                          controller: dateOfBirth,
                          maxLine: 1,
                          ontaps: () async {
                            DateTime? pickedDate = await showDatePicker(
                                context: context,
                                initialDate: widget.employeeModel.dateOfBirth,
                                firstDate: DateTime(2000),
                                lastDate: DateTime(2101));

                            if (pickedDate != null) {
                              String formattedDate =
                                  DateFormat('yyyy-MM-dd').format(pickedDate);

                              setState(() {
                                dateOfBirth.text = formattedDate;
                              });
                            } else {
                              print("Date is not selected");
                            }
                          },
                        ),
                        widget.employeeModel.workshiftId == 1
                            ? const SizedBox()
                            : Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const CustomTitleInputField(
                                    title: 'Shift Kerja',
                                  ),
                                  Container(
                                    height: 48,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(12),
                                        border: Border.all(color: greyColor)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(15.0),
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton(
                                          dropdownColor: Color(0xffF0F1F2),
                                          borderRadius:
                                              BorderRadius.circular(15),
                                          hint: workshiftList.isEmpty
                                              ? const Text('Sedang Memuat')
                                              : const Text('Pilih Shift Kerja'),
                                          items: workshiftList.map((item) {
                                            return DropdownMenuItem(
                                              value: item['workshift_id'],
                                              child: Text(item['shift_name']
                                                  .toString()),
                                            );
                                          }).toList(),
                                          onChanged: (newVal) {
                                            setState(() {
                                              valueWorkshift = newVal;
                                              print(valueWorkshift);
                                            });
                                          },
                                          value: valueWorkshift,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                        const CustomTitleInputField(
                          title: 'Jenis Kelamin',
                        ),
                        Container(
                          height: 48,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: greyColor)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                // icon: const ImageIcon(
                                //   AssetImage('assets/icons/arrow-down.png'),
                                // ),
                                dropdownColor: const Color(0xffF0F1F2),
                                borderRadius: BorderRadius.circular(15),
                                isExpanded: true,
                                items: items
                                    .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Text(
                                            item,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600,
                                              color: Colors.black87,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ))
                                    .toList(),
                                hint: const Text(
                                  'Pilih Jenis Kelamin',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black87,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                menuMaxHeight: 300,
                                value: gender,
                                onChanged: (value) {
                                  setState(() {
                                    gender = value as String;
                                    print(gender);
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                        const CustomTitleInputField(
                          title: 'Agama',
                        ),
                        Container(
                          height: 48,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: greyColor)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                // icon: const ImageIcon(
                                //   AssetImage('assets/icons/arrow-down.png'),
                                // ),
                                dropdownColor: const Color(0xffF0F1F2),
                                borderRadius: BorderRadius.circular(15),
                                isExpanded: true,
                                items: itemsAgama
                                    .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Text(
                                            item,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600,
                                              color: Colors.black87,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ))
                                    .toList(),
                                hint: const Text(
                                  'Pilih Agama',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black87,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                menuMaxHeight: 300,
                                value: religion,
                                onChanged: (value) {
                                  setState(() {
                                    religion = value as String;
                                    print(religion);
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                        const CustomTitleInputField(
                          title: 'Posisi',
                        ),
                        Container(
                          height: 48,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: greyColor)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                // icon: const ImageIcon(
                                //   AssetImage('assets/icons/arrow-down.png'),
                                // ),
                                dropdownColor: const Color(0xffF0F1F2),
                                borderRadius: BorderRadius.circular(15),
                                isExpanded: true,
                                items: itemsPosisi
                                    .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Text(
                                            item,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600,
                                              color: Colors.black87,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ))
                                    .toList(),
                                hint: const Text(
                                  'Pilih Posisi',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black87,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                menuMaxHeight: 300,
                                value: position,
                                onChanged: (value) {
                                  setState(() {
                                    position = value as String;
                                    print(position);
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CustomButton(
                            press: () {
                              fungsiChangeDataEmployee(
                                  idEmployee: widget.employeeModel.employeeId);
                            },
                            title: 'Perbarui')
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
