import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../constants.dart';
import '../../widget/custom_button.dart';

class Register extends StatelessWidget {
  const Register({super.key, required this.controller, required this.ontaps});

  final TextEditingController controller;
  final VoidCallback ontaps;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(
            height: 5,
          ),
          const Text(
            'Karyawan Baru',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
              fontFamily: 'Poppins',
              color: Color(0xff4B556B),
              letterSpacing: 1,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Center(
            child: SizedBox(
              height: 150,
              child: Lottie.asset(
                "assets/lottie/register.json",
              ),
            ),
          ),
          Row(
            children: [
              SizedBox(
                height: 30,
                width: 30,
                child: Image.asset(
                  'assets/Icons/email.png',
                  color: black2Color.withOpacity(0.8),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: SizedBox(
                  // height: 35,
                  child: TextFormField(
                    controller: controller,
                    style: const TextStyle(
                        fontFamily: 'Roboto', fontWeight: FontWeight.bold),
                    decoration: InputDecoration(
                      // border: InputBorder.none,
                      hintText: 'Masukkan nama',
                      hintStyle: TextStyle(
                          fontSize: 14.0,
                          fontFamily: 'PoppinsReguler',
                          color: black2Color.withOpacity(0.7),
                          letterSpacing: 1),
                    ),
                  ),
                ),
              ),
              const Text(
                '@attendy.my.id',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'PoppinsReguler',
                  color: Color(0xff4B556B),
                  letterSpacing: 1,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          CustomButton(press: ontaps, title: 'Daftar')
        ],
      ),
    );
  }
}
