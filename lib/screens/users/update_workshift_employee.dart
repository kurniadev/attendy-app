// ignore_for_file: use_build_context_synchronously

import 'dart:convert';

import 'package:attandy_app/screens/widget/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../models/employee_model.dart';
import '../../services/auth_service.dart';
import '../../services/workshift_service.dart';
import '../splash_screen.dart';
import '../widget/custom_loading.dart';

class UpdateWorkshiftEmployee extends StatefulWidget {
  const UpdateWorkshiftEmployee(
      {super.key, required this.employeeNotWorkModel});

  final EmployeeNotWorkModel employeeNotWorkModel;

  @override
  State<UpdateWorkshiftEmployee> createState() =>
      _UpdateWorkshiftEmployeeState();
}

class _UpdateWorkshiftEmployeeState extends State<UpdateWorkshiftEmployee> {
  var valueWorkshift;
  List workshiftList = [];
  bool loading = true;



  void fungsiAddWokrkshiftEmployee() async {
    showAlertDialog(context);
    ApiResponse response = await updateWorkshiftEmployee(
        workshiftId: valueWorkshift,
        employeeId: widget.employeeNotWorkModel.employeeId.toString());
    if (response.error == null) {
      setState(() {
        loading = false;
      });
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Menambahkan Shift Kerja')));
      Navigator.pop(context, 'refresh');
      Navigator.pop(context);
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  Future getWorkShift() async {
    String token = await getToken();
    final response = await http.get(
      Uri.parse('$baseURL/masterdata/workshift/'),
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'Bearer $token'
      },
    );
    print(response.body);
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body)['data'];
      setState(() {
        workshiftList = jsonData;
      });
    }
  }

  @override
  void initState() {
    getWorkShift();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Tentukan Shift Kerja",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            Container(
              height: 120,
              width: 120,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(60),
                image: DecorationImage(
                  image: NetworkImage(widget.employeeNotWorkModel.image),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              '${widget.employeeNotWorkModel.firstName} ${widget.employeeNotWorkModel.lastName}',
              style: const TextStyle(
                color: black2Color,
                fontFamily: "Poppins",
                fontWeight: FontWeight.w700,
                fontSize: 16,
              ),
            ),
            Text(
              widget.employeeNotWorkModel.position,
              style: const TextStyle(
                color: redColor,
                fontFamily: "PoppinsReguler",
                fontWeight: FontWeight.w600,
                fontSize: 14,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Container(
              height: 48,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: const Color(0xffF0F1F2),
                  borderRadius: BorderRadius.circular(15)),
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    dropdownColor: Color(0xffF0F1F2),
                    borderRadius: BorderRadius.circular(15),
                    hint: workshiftList.isEmpty
                        ? const Text('Sedang Memuat')
                        : const Text('Pilih Shift Kerja'),
                    items: workshiftList.map((item) {
                      return DropdownMenuItem(
                        value: item['workshift_id'].toString(),
                        child: Text(item['shift_name'].toString()),
                      );
                    }).toList(),
                    onChanged: (newVal) {
                      setState(() {
                        valueWorkshift = newVal;
                        print(valueWorkshift);
                      });
                    },
                    value: valueWorkshift,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            CustomButton(
                press: () {
                  fungsiAddWokrkshiftEmployee();
                },
                title: 'Tambahkan')
          ],
        ),
      ),
    );
  }
}
