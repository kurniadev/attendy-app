import 'dart:convert';

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/employee_model.dart';
import 'package:attandy_app/screens/splash_screen.dart';
import 'package:attandy_app/screens/tabs_screen.dart';
import 'package:attandy_app/screens/users/component/change_data_employee.dart';
import 'package:attandy_app/screens/users/component/item_pengguna.dart';
import 'package:attandy_app/screens/users/component/menu_action_user.dart';
import 'package:attandy_app/screens/users/component/register.dart';
import 'package:attandy_app/screens/users/update_workshift_employee.dart';
import 'package:attandy_app/services/employee_service.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../shimmer/shimmer_item_user.dart';
import '../widget/custom_loading.dart';
import 'component/change_password_user.dart';
import 'component/item_not_workshift.dart';
import 'component/item_pengguna_baru.dart';

class UsersScreen extends StatefulWidget {
  const UsersScreen({super.key, required this.role});

  final String role;

  @override
  State<UsersScreen> createState() => _UsersScreenState();
}

class _UsersScreenState extends State<UsersScreen> {
  TextEditingController email = TextEditingController();
  TextEditingController changePassword = TextEditingController();
  bool _loadingEmployee = true;
  bool _loadingNewEmployee = true;
  bool _loadingNotWorkEmployee = true;
  bool loadCount = true;
  bool _showFab = true;
  bool _loading = false;
  List<dynamic> userList = [];
  List<dynamic> userNewList = [];
  List<dynamic> userNotWorkList = [];
  int count = 0;

  Future<void> fungsiGetEmployee() async {
    ApiResponse response = await getEmployee();
    if (response.error == null) {
      setState(() {
        userList = response.data as List<dynamic>;
        _loadingEmployee =
            _loadingEmployee ? !_loadingEmployee : _loadingEmployee;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  Future<void> fungsiGetNewEmployee() async {
    ApiResponse response = await getNewEmployee();
    if (response.error == null) {
      setState(() {
        userNewList = response.data as List<dynamic>;
        _loadingNewEmployee =
            _loadingNewEmployee ? !_loadingNewEmployee : _loadingNewEmployee;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  Future<void> fungsiGetNotWorkEmployee() async {
    ApiResponse response = await getNotWorkEmployee();
    if (response.error == null) {
      setState(() {
        userNotWorkList = response.data as List<dynamic>;
        _loadingNotWorkEmployee = _loadingNotWorkEmployee
            ? !_loadingNotWorkEmployee
            : _loadingNotWorkEmployee;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void funsiRegisterEmployee() async {
    showAlertDialog(context);
    ApiResponse response = await registerEmployee(
      email: email.text,
    );
    if (response.error == null) {
      Navigator.pop(context);
      email.clear();
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Registrasi Berhasil')));
      // ignore: use_build_context_synchronously
      setState(() {
        fungsiGetEmployee();
        fungsiGetNewEmployee();
      });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('Ada Kesalahan',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      // ignore: use_build_context_synchronously
      Navigator.pop(context);
    }
  }

  void fungsiresetpassword({required int idUser}) async {
    showAlertDialog(context);
    ApiResponse response = await resetPasswordEmployee(
      password: changePassword.text,
      idUser: idUser.toString(),
    );
    if (response.error == null) {
      Navigator.pop(context);
      changePassword.clear();
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Reset Password Berhasil')));
      // ignore: use_build_context_synchronously
      setState(() {
        fungsiGetEmployee();
        fungsiGetNewEmployee();
      });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('Ada Kesalahan',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      // ignore: use_build_context_synchronously
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    fungsiGetNotWorkEmployee();
    fungsiGetEmployee();
    fungsiGetNewEmployee();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const duration = Duration(milliseconds: 300);
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Pengguna",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      floatingActionButton: AnimatedSlide(
        duration: duration,
        offset: _showFab ? Offset.zero : const Offset(0, 2),
        child: AnimatedOpacity(
          duration: duration,
          opacity: _showFab ? 1 : 0,
          child: widget.role == 'employee'
              ? SizedBox()
              : FloatingActionButton(
                  backgroundColor: blueColor,
                  child: const Icon(Icons.add),
                  onPressed: () {
                    showModalBottomSheet(
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.vertical(
                              top: Radius.circular(25.0))),
                      backgroundColor: Colors.white,
                      context: context,
                      isScrollControlled: true,
                      builder: (context) => Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: Register(
                          controller: email,
                          ontaps: () {
                            if (email.text == '') {
                              AnimatedSnackBar.material(
                                      'Email tidak boleh kosong',
                                      type: AnimatedSnackBarType.warning,
                                      mobileSnackBarPosition:
                                          MobileSnackBarPosition.top,
                                      desktopSnackBarPosition:
                                          DesktopSnackBarPosition.topCenter)
                                  .show(context);
                            } else {
                              Navigator.pop(context);
                              funsiRegisterEmployee();
                            }
                          },
                        ),
                      ),
                    );
                  },
                ),
        ),
      ),
      body: widget.role == 'employee'
          ? SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  _loadingEmployee
                      ? const Expanded(
                          child: SingleChildScrollView(
                            child: ShimmerItemUser(),
                          ),
                        )
                      : Expanded(
                          child: ListView.builder(
                            itemCount: userList.length,
                            itemBuilder: (BuildContext context, int index) {
                              EmployeeModel employeeModel = userList[index];
                              return Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10.0),
                                child: ItemPengguna(
                                  image: 'assets/Background/rezakurnia.jpg',
                                  email: employeeModel.email,
                                  name:
                                      '${employeeModel.firstName} ${employeeModel.lastName}',
                                  position: employeeModel.position,
                                  imageUrl: employeeModel.image,
                                  ontaps: () {},
                                ),
                              );
                            },
                          ),
                        ),
                ],
              ),
            )
          : NotificationListener<UserScrollNotification>(
              onNotification: (notification) {
                final ScrollDirection direction = notification.direction;
                setState(() {
                  if (direction == ScrollDirection.reverse) {
                    _showFab = false;
                  } else if (direction == ScrollDirection.forward) {
                    _showFab = true;
                  }
                });
                return true;
              },
              child: SafeArea(
                  child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              showModalBottomSheet(
                                // shape: const RoundedRectangleBorder(
                                //     borderRadius: BorderRadius.vertical(
                                //         top: Radius.circular(25.0))),
                                backgroundColor: Colors.white,
                                context: context,
                                isScrollControlled: false,
                                builder: (context) => Padding(
                                  padding: EdgeInsets.only(
                                      bottom: MediaQuery.of(context)
                                          .viewInsets
                                          .bottom),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const Padding(
                                        padding: EdgeInsets.all(12.0),
                                        child: Text(
                                          'Pengguna Baru',
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: black2Color,
                                            fontFamily: "Roboto",
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1,
                                          ),
                                        ),
                                      ),
                                      userNewList.isEmpty
                                          ? Expanded(
                                              child: Center(
                                                child: SizedBox(
                                                  height: 200,
                                                  child: Lottie.asset(
                                                    "assets/lottie/empty-box-blue.json",
                                                  ),
                                                ),
                                              ),
                                            )
                                          : Expanded(
                                              child: ListView.builder(
                                                itemCount: userNewList.length,
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  UserStatusModel
                                                      userStatusModel =
                                                      userNewList[index];
                                                  return ItemPenggunaBaru(
                                                    email:
                                                        userStatusModel.email,
                                                    password: userStatusModel
                                                        .passwordOld,
                                                    ontaps: () {
                                                      Clipboard.setData(
                                                          ClipboardData(
                                                              text:
                                                                  'Email : ${userStatusModel.email}, Password :${userStatusModel.passwordOld}'));
                                                      Navigator.pop(context);
                                                      ScaffoldMessenger.of(
                                                              context)
                                                          .showSnackBar(
                                                              const SnackBar(
                                                                  content: Text(
                                                                      'Berhasil Menyalin User Penguna Baru')));
                                                    },
                                                  );
                                                },
                                              ),
                                            ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              height: 45,
                              width: 135,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  color: blueColor),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    height: 25,
                                    width: 25,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Colors.white),
                                    child: Center(
                                      child: Text(
                                        userNewList.length.toString(),
                                        style: const TextStyle(
                                          fontSize: 16,
                                          color: blueColor,
                                          fontFamily: "Roboto",
                                          fontWeight: FontWeight.bold,
                                          letterSpacing: 1,
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  const Text(
                                    'Pengguna Baru',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontSize: 10,
                                      color: Colors.white,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 1,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                showModalBottomSheet(
                                  // shape: const RoundedRectangleBorder(
                                  //     borderRadius: BorderRadius.vertical(
                                  //         top: Radius.circular(25.0))),
                                  backgroundColor: Colors.white,
                                  context: context,
                                  isScrollControlled: false,
                                  builder: (context) => Padding(
                                    padding: EdgeInsets.only(
                                        bottom: MediaQuery.of(context)
                                            .viewInsets
                                            .bottom),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const Padding(
                                          padding: EdgeInsets.all(12.0),
                                          child: Text(
                                            'Shift Kerja Belum Ditentukan',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              color: black2Color,
                                              fontFamily: "Roboto",
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 1,
                                            ),
                                          ),
                                        ),
                                        userNotWorkList.isEmpty
                                            ? Expanded(
                                                child: Center(
                                                  child: SizedBox(
                                                    height: 200,
                                                    child: Lottie.asset(
                                                      "assets/lottie/empty-box-blue.json",
                                                    ),
                                                  ),
                                                ),
                                              )
                                            : Expanded(
                                                child: ListView.builder(
                                                  itemCount:
                                                      userNotWorkList.length,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    EmployeeNotWorkModel
                                                        employeeNotWorkModel =
                                                        userNotWorkList[index];
                                                    return ItemNotWorkshift(
                                                        name:
                                                            '${employeeNotWorkModel.firstName} ${employeeNotWorkModel.lastName}',
                                                        position:
                                                            employeeNotWorkModel
                                                                .position,
                                                        ontaps: () async {
                                                          String refresh =
                                                              await Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                      builder: (context) => UpdateWorkshiftEmployee(
                                                                            employeeNotWorkModel:
                                                                                employeeNotWorkModel,
                                                                          )));
                                                          if (refresh ==
                                                              'refresh') {
                                                            fungsiGetNotWorkEmployee();
                                                            fungsiGetEmployee();
                                                            fungsiGetNewEmployee();
                                                          }
                                                        });
                                                  },
                                                ),
                                              )
                                      ],
                                    ),
                                  ),
                                );
                              },
                              child: Container(
                                height: 45,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    color: blueColor),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      height: 25,
                                      width: 25,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          color: Colors.white),
                                      child: Center(
                                        child: Text(
                                          userNotWorkList.length.toString(),
                                          style: const TextStyle(
                                            fontSize: 16,
                                            color: blueColor,
                                            fontFamily: "Roboto",
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    const Text(
                                      'Shift Kerja Belum Ditentukan',
                                      style: TextStyle(
                                        fontSize: 10,
                                        color: Colors.white,
                                        fontFamily: "Roboto",
                                        fontWeight: FontWeight.bold,
                                        letterSpacing: 1,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      "Pengguna",
                      style: TextStyle(
                          color: black2Color, fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    _loadingEmployee
                        ? const Expanded(
                            child: SingleChildScrollView(
                              child: ShimmerItemUser(),
                            ),
                          )
                        : Expanded(
                            child: ListView.builder(
                              itemCount: userList.length,
                              itemBuilder: (BuildContext context, int index) {
                                EmployeeModel employeeModel = userList[index];
                                return ItemPengguna(
                                  image: 'assets/Background/rezakurnia.jpg',
                                  email: employeeModel.email,
                                  name:
                                      '${employeeModel.firstName} ${employeeModel.lastName}',
                                  position: employeeModel.position,
                                  imageUrl: employeeModel.image,
                                  ontaps: () {
                                    showModalBottomSheet(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      context: context,
                                      builder: (builder) {
                                        return Container(
                                          height: 150.0,
                                          color: Colors
                                              .transparent, //could change this to Color(0xFF737373),
                                          child: MenuActionUser(
                                            changedata: () async {
                                              String refresh =
                                                  await Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              ChangeDataEmployee(
                                                                employeeModel:
                                                                    employeeModel,
                                                              )));
                                              if (refresh == 'refresh') {
                                                fungsiGetEmployee();
                                              }
                                            },
                                            changepassword: () {
                                              Navigator.pop(context);
                                              showModalBottomSheet(
                                                shape:
                                                    const RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.vertical(
                                                                top: Radius
                                                                    .circular(
                                                                        25.0))),
                                                backgroundColor: Colors.white,
                                                context: context,
                                                isScrollControlled: true,
                                                builder: (context) => Padding(
                                                  padding: EdgeInsets.only(
                                                      bottom:
                                                          MediaQuery.of(context)
                                                              .viewInsets
                                                              .bottom),
                                                  child: ChangePasswordUser(
                                                    controller: changePassword,
                                                    names:
                                                        employeeModel.firstName,
                                                    ontaps: () {
                                                      if (changePassword.text ==
                                                          '') {
                                                        AnimatedSnackBar.material(
                                                                'Kata Sandi tidak boleh kosong',
                                                                type:
                                                                    AnimatedSnackBarType
                                                                        .warning,
                                                                mobileSnackBarPosition:
                                                                    MobileSnackBarPosition
                                                                        .top,
                                                                desktopSnackBarPosition:
                                                                    DesktopSnackBarPosition
                                                                        .topCenter)
                                                            .show(context);
                                                      } else {
                                                        Navigator.pop(context);
                                                        fungsiresetpassword(
                                                            idUser:
                                                                employeeModel
                                                                    .userId);
                                                      }
                                                    },
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                        );
                                      },
                                    );
                                  },
                                );
                              },
                            ),
                          ),
                  ],
                ),
              )),
            ),
    );
  }
}
