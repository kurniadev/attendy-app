import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../constants.dart';
import '../../widget/custom_button.dart';

class ActionTypeLeave extends StatefulWidget {
  const ActionTypeLeave(
      {super.key,
      required this.controller,
      required this.ontaps,
      required this.title, required this.buttonTitle});
  final TextEditingController controller;
  final VoidCallback ontaps;
  final String title, buttonTitle;

  @override
  State<ActionTypeLeave> createState() => _ActionTypeLeaveState();
}

class _ActionTypeLeaveState extends State<ActionTypeLeave> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(
            height: 5,
          ),
          Text(
            widget.title,
            style: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
              fontFamily: 'Poppins',
              color: Color(0xff4B556B),
              letterSpacing: 1,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Center(
            child: SizedBox(
              height: 160,
              child: Lottie.asset("assets/lottie/document.json",
                  fit: BoxFit.cover),
            ),
          ),
          Row(
            children: [
              SizedBox(
                height: 30,
                width: 30,
                child: Image.asset(
                  'assets/Icons/type.png',
                  color: black2Color.withOpacity(0.8),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: SizedBox(
                  // height: 35,
                  child: TextFormField(
                    controller: widget.controller,
                    style: const TextStyle(
                        fontFamily: 'Roboto', fontWeight: FontWeight.bold),
                    decoration: InputDecoration(
                      // border: InputBorder.none,
                      hintText: 'Masukkan jenis izin cuti',
                      hintStyle: TextStyle(
                          fontSize: 14.0,
                          fontFamily: 'PoppinsReguler',
                          color: black2Color.withOpacity(0.7),
                          letterSpacing: 1),
                    ),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          CustomButton(press: widget.ontaps, title: widget.buttonTitle)
        ],
      ),
    );
  }
}
