// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/leave_permit_model.dart';
import 'package:attandy_app/screens/type_leave/component/action_type_leave.dart';
import 'package:attandy_app/services/leave_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../shimmer/shimmer_list_workshift.dart';
import '../splash_screen.dart';
import '../widget/custom_loading.dart';
import '../widget/custom_shimmer.dart';

class TypeLeaveScreen extends StatefulWidget {
  const TypeLeaveScreen({super.key});

  @override
  State<TypeLeaveScreen> createState() => _TypeLeaveScreenState();
}

class _TypeLeaveScreenState extends State<TypeLeaveScreen> {
  bool _showFab = true;
  TextEditingController textController = TextEditingController();
  bool _loading = true;
  List<dynamic> leaveTypeList = [];

  Future<void> fungsiGetLeaveType() async {
    ApiResponse response = await getLeaveType();
    if (response.error == null) {
      setState(() {
        leaveTypeList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void fungsiCreateLeaveType() async {
    showAlertDialog(context);
    ApiResponse response = await createLeaveType(
      leaveTypeName: textController.text,
    );
    if (response.error == null) {
      Navigator.pop(context);
      setState(() {
        fungsiGetLeaveType();
      });
      Navigator.pop(context);
      AnimatedSnackBar.material('Berhasil Menambahkan Jenis Izin Cuti',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('Ada Kesalahan',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  bool loading = true;

  void fungsiUpdateLeaveType(int typeId) async {
    showAlertDialog(context);
    ApiResponse response = await updateLeaveType(
        leaveTypeName: textController.text, typeId: typeId.toString());
    if (response.error == null) {
      setState(() {
        loading = false;
        fungsiGetLeaveType();
      });
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Perbarui Jenis Izin Cuti')));
      Navigator.pop(context);
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  void fungsiDeleteLeaveType(int typeId) async {
    showAlertDialog(context);
    ApiResponse response = await deleteLeaveType(typeId: typeId);
    if (response.error == null) {
      Navigator.pop(context);

      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Menghapus Jenis Izin Cuti')));
      setState(() {
        fungsiGetLeaveType();
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    fungsiGetLeaveType();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const duration = Duration(milliseconds: 300);
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Jenis Izin Cuti",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      floatingActionButton: AnimatedSlide(
        duration: duration,
        offset: _showFab ? Offset.zero : const Offset(0, 2),
        child: AnimatedOpacity(
          duration: duration,
          opacity: _showFab ? 1 : 0,
          child: FloatingActionButton(
            backgroundColor: blueColor,
            child: const Icon(Icons.add),
            onPressed: () async {
              setState(() {
                textController.clear();
              });
              showModalBottomSheet(
                shape: const RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(25.0))),
                backgroundColor: Colors.white,
                context: context,
                isScrollControlled: true,
                builder: (context) => Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: ActionTypeLeave(
                      controller: textController,
                      ontaps: () {
                        fungsiCreateLeaveType();
                      },
                      title: 'Tambah Jenis Izin Cuti',
                      buttonTitle: 'Tambah',
                    )),
              );
            },
          ),
        ),
      ),
      body: NotificationListener<UserScrollNotification>(
          onNotification: (notification) {
            final ScrollDirection direction = notification.direction;
            setState(() {
              if (direction == ScrollDirection.reverse) {
                _showFab = false;
              } else if (direction == ScrollDirection.forward) {
                _showFab = true;
              }
            });
            return true;
          },
          child: _loading
              ? Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(12),
                      child: CustomShimmer(
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          radius: 12),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12),
                      child: CustomShimmer(
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          radius: 12),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12),
                      child: CustomShimmer(
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          radius: 12),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12),
                      child: CustomShimmer(
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          radius: 12),
                    ),
                  ],
                )
              : ListView.builder(
                  itemCount: leaveTypeList.length,
                  itemBuilder: (BuildContext context, int index) {
                    LeaveTypeModel leaveTypeModel = leaveTypeList[index];
                    return Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 2,
                              blurRadius: 5,
                              offset: const Offset(
                                  0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                  leaveTypeModel.leaveTypeName,
                                  style: const TextStyle(
                                    fontSize: 18,
                                    color: black2Color,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.w700,
                                    letterSpacing: 1,
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    textController.text =
                                        leaveTypeModel.leaveTypeName;
                                  });
                                  showModalBottomSheet(
                                    shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.vertical(
                                            top: Radius.circular(25.0))),
                                    backgroundColor: Colors.white,
                                    context: context,
                                    isScrollControlled: true,
                                    builder: (context) => Padding(
                                        padding: EdgeInsets.only(
                                            bottom: MediaQuery.of(context)
                                                .viewInsets
                                                .bottom),
                                        child: ActionTypeLeave(
                                          controller: textController,
                                          ontaps: () {
                                            fungsiUpdateLeaveType(
                                                leaveTypeModel.leaveTypeId);
                                          },
                                          title: 'Edit Jenis Izin Cuti',
                                          buttonTitle: 'Perbarui',
                                        )),
                                  );
                                },
                                child: const SizedBox(
                                  height: 50,
                                  width: 40,
                                  child: Icon(Icons.edit),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Alert(
                                    context: context,
                                    type: AlertType.warning,
                                    desc:
                                        "Apakah anda ingin menghapus Hari ${leaveTypeModel.leaveTypeName} ",
                                    buttons: [
                                      DialogButton(
                                        color: redColor,
                                        onPressed: () {
                                          fungsiDeleteLeaveType(
                                              leaveTypeModel.leaveTypeId);
                                        },
                                        width: 120,
                                        child: const Text(
                                          "Hapus",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20),
                                        ),
                                      )
                                    ],
                                  ).show();
                                },
                                child: const SizedBox(
                                  height: 50,
                                  width: 40,
                                  child: Icon(Icons.delete_outline),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                )),
    );
  }
}
