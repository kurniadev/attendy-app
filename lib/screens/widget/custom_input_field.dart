import 'package:attandy_app/constants.dart';
import 'package:flutter/material.dart';

class CustomInputField extends StatefulWidget {
  const CustomInputField(
      {super.key, required this.controller, required this.maxLine});
  final TextEditingController controller;
  final int maxLine;
  @override
  State<CustomInputField> createState() => _CustomInputFieldState();
}

class _CustomInputFieldState extends State<CustomInputField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 50,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(color: greyColor)),
      child: Padding(
        padding: const EdgeInsets.only(left: 10),
        child: TextFormField(
          maxLines: widget.maxLine,
          controller: widget.controller,
          decoration: const InputDecoration(
            border: InputBorder.none,
            // hintText: 'Masukkan Nama Depan',
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Wajib Di isi';
            }
            return null;
          },
        ),
      ),
    );
  }
}

class CustomInputField2 extends StatefulWidget {
  const CustomInputField2(
      {super.key,
      required this.controller,
      required this.maxLine,
      required this.maxLength});
  final TextEditingController controller;
  final int maxLine, maxLength;
  @override
  State<CustomInputField2> createState() => _CustomInputField2State();
}

class _CustomInputField2State extends State<CustomInputField2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 50,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(color: greyColor)),
      child: Padding(
        padding: const EdgeInsets.only(left: 10),
        child: TextFormField(
          maxLines: widget.maxLine,
          maxLength: widget.maxLength,
          controller: widget.controller,
          decoration: const InputDecoration(
            border: InputBorder.none,
            // hintText: 'Masukkan Nama Depan',
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Wajib Di isi';
            }
            return null;
          },
        ),
      ),
    );
  }
}

class CustomInputFieldTgl extends StatefulWidget {
  const CustomInputFieldTgl(
      {super.key,
      required this.controller,
      required this.maxLine,
      required this.ontaps});
  final TextEditingController controller;
  final int maxLine;
  final VoidCallback ontaps;
  @override
  State<CustomInputFieldTgl> createState() => _CustomInputFieldTglState();
}

class _CustomInputFieldTglState extends State<CustomInputFieldTgl> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 50,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(color: greyColor)),
      child: Padding(
        padding: const EdgeInsets.only(left: 10),
        child: TextFormField(
          maxLines: widget.maxLine,
          readOnly: true,
          controller: widget.controller,
          decoration: const InputDecoration(
            border: InputBorder.none,
            // hintText: 'Masukkan Nama Depan',
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Wajib Di isi';
            }
            return null;
          },
          onTap: widget.ontaps,
        ),
      ),
    );
  }
}
