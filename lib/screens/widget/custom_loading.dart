import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

showAlertDialog(BuildContext context) {
  showGeneralDialog(
    // barrierDismissible: true,
    barrierLabel: '',
    barrierColor: Colors.white.withOpacity(0.1),
    transitionDuration: const Duration(milliseconds: 300),
    pageBuilder: (ctx, anim1, anim2) => Center(
      child: SizedBox(
        height: 150,
        width: MediaQuery.of(context).size.width,
        child: Lottie.asset(
          "assets/lottie/loading.json",
        ),
      ),
    ),
    transitionBuilder: (ctx, anim1, anim2, child) => BackdropFilter(
      filter:
          ImageFilter.blur(sigmaX: 3 * anim1.value, sigmaY: 3 * anim1.value),
      child: FadeTransition(
        opacity: anim1,
        child: child,
      ),
    ),
    context: context,
  );
}
