import 'package:flutter/material.dart';

class CustomTitleInputField extends StatefulWidget {
  const CustomTitleInputField({super.key, required this.title});

  final String title;

  @override
  State<CustomTitleInputField> createState() => _CustomTitleInputFieldState();
}

class _CustomTitleInputFieldState extends State<CustomTitleInputField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Text(
        widget.title,
        style: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w600,
          fontFamily: 'Poppins',
          color: Color(0xff4B556B),
        ),
      ),
    );
  }
}
