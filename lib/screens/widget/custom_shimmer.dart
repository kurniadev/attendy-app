import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../constants.dart';

class CustomShimmer extends StatefulWidget {
  const CustomShimmer(
      {super.key,
      required this.height,
      required this.width,
      required this.radius});

  final double height, width, radius;

  @override
  State<CustomShimmer> createState() => _CustomShimmerState();
}

class _CustomShimmerState extends State<CustomShimmer> {
  @override
  Widget build(BuildContext context) {
    return Shimmer(
      gradient: const LinearGradient(
        stops: [0.2, 0.5, 0.6],
        colors: [shimmer1, shimmer2, shimmer3],
      ),
      child: Container(
        height: widget.height,
        width: widget.width,
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(widget.radius),
        ),
      ),
    );
  }
}
