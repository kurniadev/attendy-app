import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../widget/custom_button.dart';

class TextEditingCompany extends StatefulWidget {
  const TextEditingCompany(
      {super.key,
      required this.title,
      required this.controller,
      required this.press});

  final String title;
  final TextEditingController controller;
  final VoidCallback press;

  @override
  State<TextEditingCompany> createState() => _TextEditingCompanyState();
}

class _TextEditingCompanyState extends State<TextEditingCompany> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(
            height: 5,
          ),
          Text(
            widget.title,
            style: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
              fontFamily: 'Poppins',
              color: Color(0xff4B556B),
              letterSpacing: 1,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          TextFormField(
            controller: widget.controller,
            style: const TextStyle(
                fontFamily: 'Roboto', fontWeight: FontWeight.bold),
            decoration: InputDecoration(
              // border: InputBorder.none,
              hintStyle: TextStyle(
                  fontSize: 14.0,
                  fontFamily: 'PoppinsReguler',
                  color: black2Color.withOpacity(0.7),
                  letterSpacing: 1),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          CustomButton(press: widget.press, title: 'Edit')
        ],
      ),
    );
  }
}
