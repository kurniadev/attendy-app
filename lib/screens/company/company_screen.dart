// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/company_model.dart';
import 'package:attandy_app/screens/widget/custom_button.dart';
import 'package:attandy_app/services/company_service.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../widget/custom_loading.dart';
import '../widget/custom_shimmer.dart';
import 'component/text_editing_company.dart';

class CompanyScreen extends StatefulWidget {
  const CompanyScreen({super.key});

  @override
  State<CompanyScreen> createState() => _CompanyScreenState();
}

class _CompanyScreenState extends State<CompanyScreen> {
  TextEditingController longController = TextEditingController();
  TextEditingController latController = TextEditingController();
  TextEditingController distanceController = TextEditingController();
  String long = '';
  String lat = '';
  String distance = '';

  CompanyModel? companyModel;
  bool loading = true;

  void fungsiUpdateCompany() async {
    showAlertDialog(context);
    ApiResponse response = await updateCompany(
        companyName: companyModel!.companyName.toString(),
        companyAddress: companyModel!.companyAddress.toString(),
        latitude: lat,
        longitude: long,
        distance: distance);
    if (response.error == null) {
      setState(() {
        loading = false;
        functionGetCompany();
      });
      Navigator.pop(context);
      AnimatedSnackBar.material('Data Perusahaan Berhasil Diperbarui',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  void functionGetCompany() async {
    ApiResponse response = await getCompany();
    if (response.error == null) {
      setState(() {
        companyModel = response.data as CompanyModel;
        long = companyModel!.longitude;
        lat = companyModel!.latitude;
        distance = companyModel!.distance;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  Future<void> _loadData() async {
    setState(() {
      functionGetCompany();
    });
  }

  @override
  void initState() {
    functionGetCompany();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Perusahaan",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(6.0),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 100,
                          width: 100,
                          child: Image.asset(
                            "assets/Icons/notification.png",
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              companyModel == null
                                  ? CustomShimmer(
                                      height: 14,
                                      width: MediaQuery.of(context).size.width,
                                      radius: 12,
                                    )
                                  : Text(
                                      companyModel!.companyName,
                                      style: const TextStyle(
                                          color: black2Color,
                                          fontWeight: FontWeight.w600),
                                    ),
                              const SizedBox(
                                height: 5,
                              ),
                              companyModel == null
                                  ? CustomShimmer(
                                      height: 50,
                                      width: MediaQuery.of(context).size.width,
                                      radius: 8,
                                    )
                                  : Text(
                                      companyModel!.companyAddress,
                                      style: const TextStyle(
                                          color: black2Color,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12),
                                    ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  const Text(
                    "Longitude",
                    style: TextStyle(
                        color: black2Color, fontWeight: FontWeight.w600),
                  ),
                  companyModel == null
                      ? Padding(
                          padding: const EdgeInsets.only(top: 8, bottom: 16),
                          child: CustomShimmer(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            radius: 12,
                          ),
                        )
                      : Padding(
                          padding: const EdgeInsets.only(top: 8, bottom: 16),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                longController.text = long;
                              });
                              showModalBottomSheet(
                                shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.vertical(
                                        top: Radius.circular(25.0))),
                                backgroundColor: Colors.white,
                                context: context,
                                isScrollControlled: true,
                                builder: (context) => Padding(
                                    padding: EdgeInsets.only(
                                        bottom: MediaQuery.of(context)
                                            .viewInsets
                                            .bottom),
                                    child: TextEditingCompany(
                                      controller: longController,
                                      press: () {
                                        setState(() {
                                          long = longController.text;
                                        });
                                        Navigator.pop(context);
                                      },
                                      title: 'Longitude',
                                    )),
                              );
                            },
                            child: Container(
                                height: 50,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    border: Border.all(color: Colors.grey)),
                                child: Center(
                                  child: Text(
                                    long,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                )),
                          ),
                        ),
                  const Text(
                    "Latitude",
                    style: TextStyle(
                        color: black2Color, fontWeight: FontWeight.w600),
                  ),
                  companyModel == null
                      ? Padding(
                          padding: const EdgeInsets.only(top: 8, bottom: 16),
                          child: CustomShimmer(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            radius: 12,
                          ),
                        )
                      : Padding(
                          padding: const EdgeInsets.only(top: 8, bottom: 16),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                latController.text = lat;
                              });
                              showModalBottomSheet(
                                shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.vertical(
                                        top: Radius.circular(25.0))),
                                backgroundColor: Colors.white,
                                context: context,
                                isScrollControlled: true,
                                builder: (context) => Padding(
                                    padding: EdgeInsets.only(
                                        bottom: MediaQuery.of(context)
                                            .viewInsets
                                            .bottom),
                                    child: TextEditingCompany(
                                      controller: latController,
                                      press: () {
                                        setState(() {
                                          lat = latController.text;
                                        });
                                        Navigator.pop(context);
                                      },
                                      title: 'Latitude',
                                    )),
                              );
                            },
                            child: Container(
                                height: 50,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    border: Border.all(color: Colors.grey)),
                                child: Center(
                                  child: Text(
                                    lat,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                )),
                          ),
                        ),
                  const Text(
                    "Jarak Maksimal Absensi",
                    style: TextStyle(
                        color: black2Color, fontWeight: FontWeight.w600),
                  ),
                  companyModel == null
                      ? Padding(
                          padding: const EdgeInsets.only(top: 8, bottom: 16),
                          child: CustomShimmer(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            radius: 12,
                          ),
                        )
                      : Padding(
                          padding: const EdgeInsets.only(top: 8, bottom: 16),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                distanceController.text = distance;
                              });
                              showModalBottomSheet(
                                shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.vertical(
                                        top: Radius.circular(25.0))),
                                backgroundColor: Colors.white,
                                context: context,
                                isScrollControlled: true,
                                builder: (context) => Padding(
                                    padding: EdgeInsets.only(
                                        bottom: MediaQuery.of(context)
                                            .viewInsets
                                            .bottom),
                                    child: TextEditingCompany(
                                      controller: distanceController,
                                      press: () {
                                        setState(() {
                                          distance = distanceController.text;
                                        });
                                        Navigator.pop(context);
                                      },
                                      title: 'Jarak Maksimal Absensi',
                                    )),
                              );
                            },
                            child: Container(
                                height: 50,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    border: Border.all(color: Colors.grey)),
                                child: Center(
                                  child: Text(
                                    distance,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                )),
                          ),
                        ),
                  CustomButton(
                      press: () {
                        fungsiUpdateCompany();
                      },
                      title: 'Perbarui')
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
