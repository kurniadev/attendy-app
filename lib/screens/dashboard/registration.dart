// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/admission_model.dart';
import 'package:attandy_app/screens/dashboard/success_admission.dart';
import 'package:attandy_app/screens/widget/custom_button.dart';
import 'package:attandy_app/services/admission_service.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../widget/custom_input_field.dart';
import '../widget/custom_loading.dart';
import '../widget/custom_title_input_field.dart';

class Registration extends StatefulWidget {
  const Registration({super.key});

  @override
  State<Registration> createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  bool _loading = false;
  AdmissionImage? admissionImage;
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController nik = TextEditingController();
  TextEditingController nip = TextEditingController();
  TextEditingController placeOfBirth = TextEditingController();
  TextEditingController dateOfBirth = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController phone = TextEditingController();

  String? gender;
  String? religion;
  String? position;

  final List<String> items = [
    'Laki-Laki',
    'Perempuan',
  ];

  final List<String> itemsAgama = [
    'Islam',
    'Kristen',
    'Katolik',
    'Hindu',
    'Budha',
    'Konghucu',
  ];
  final List<String> itemsPosisi = [
    // 'Chief Executive Officer (CEO)',
    // 'Chief Marketing Officer (CMO)',
    // 'Chief Oprational Officer (COO)',
    'General Affair',
    'Marketing Staff',
    'Network Engineer Staff',
    'Electronics Engineer Staff',
  ];

  File? _imageFile;
  final _picker = ImagePicker();

  Future getImage() async {
    // ignore: deprecated_member_use
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
      });
    }
  }

  void fungsiCreateAdmission() async {
    showAlertDialog(context);
    print(dateOfBirth.text);
    String? image = _imageFile == null ? null : getStringImage(_imageFile);
    ApiResponse response = await createAdmission(
      firstName: firstName.text,
      lastName: lastName.text,
      nik: nik.text,
      // nip: nip.text,
      placeOfBirth: placeOfBirth.text,
      dateOfBirth: dateOfBirth.text,
      gender: gender.toString(),
      religion: religion.toString(),
      address: address.text,
      position: position.toString(),
      image: image,
      workshiftId: '1',
      phone: phone.text,
    );
    if (response.error == null) {
      Navigator.pop(context);
      print('sukses');
      setState(() {
        admissionImage = response.data as AdmissionImage;
        setPrefStatus();
      });
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => const SuccessAdmission()),
          (route) => false);
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('Ada Kesalahan',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  setPrefStatus() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('firstName', firstName.text);
    await preferences.setString('lastName', lastName.text);
    await preferences.setString('position', position.toString());
    await preferences.setString('imageUrl', admissionImage!.image);
    await preferences.setString('status', 'true');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Registrasi",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _key,
            child: Column(
              children: [
                SizedBox(
                  height: 270,
                  child: Lottie.asset(
                    "assets/lottie/regg.json",
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const CustomTitleInputField(
                          title: 'Nama Lengkap',
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: CustomInputField(
                                controller: firstName,
                                maxLine: 1,
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: CustomInputField(
                                controller: lastName,
                                maxLine: 1,
                              ),
                            ),
                          ],
                        ),
                        const CustomTitleInputField(
                          title: 'NIK',
                        ),
                        CustomInputField2(
                          controller: nik,
                          maxLine: 1,
                          maxLength: 16,
                        ),
                        // const CustomTitleInputField(
                        //   title: 'NIP',
                        // ),
                        // CustomInputField2(
                        //   controller: nip,
                        //   maxLine: 1,
                        //   maxLength: 16,
                        // ),
                        const CustomTitleInputField(
                          title: 'Tempat Lahir',
                        ),
                        CustomInputField(
                          controller: placeOfBirth,
                          maxLine: 1,
                        ),
                        const CustomTitleInputField(
                          title: 'Tanggal Lahir',
                        ),
                        CustomInputFieldTgl(
                          controller: dateOfBirth,
                          maxLine: 1,
                          ontaps: () async {
                            DateTime? pickedDate = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(1990),
                                lastDate: DateTime(2101));

                            if (pickedDate != null) {
                              String formattedDate =
                                  DateFormat('yyyy-MM-dd').format(pickedDate);

                              setState(() {
                                dateOfBirth.text = formattedDate;
                              });
                            } else {
                              print("Date is not selected");
                            }
                          },
                        ),
                        const CustomTitleInputField(
                          title: 'Jenis Kelamin',
                        ),
                        Container(
                          height: 48,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: greyColor)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                // icon: const ImageIcon(
                                //   AssetImage('assets/icons/arrow-down.png'),
                                // ),
                                dropdownColor: const Color(0xffF0F1F2),
                                borderRadius: BorderRadius.circular(15),
                                isExpanded: true,
                                items: items
                                    .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Text(
                                            item,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600,
                                              color: Colors.black87,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ))
                                    .toList(),
                                hint: const Text(
                                  'Pilih Jenis Kelamin',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black87,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                menuMaxHeight: 300,
                                value: gender,
                                onChanged: (value) {
                                  setState(() {
                                    gender = value as String;
                                    print(gender);
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                        const CustomTitleInputField(
                          title: 'Agama',
                        ),
                        Container(
                          height: 48,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: greyColor)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                // icon: const ImageIcon(
                                //   AssetImage('assets/icons/arrow-down.png'),
                                // ),
                                dropdownColor: const Color(0xffF0F1F2),
                                borderRadius: BorderRadius.circular(15),
                                isExpanded: true,
                                items: itemsAgama
                                    .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Text(
                                            item,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600,
                                              color: Colors.black87,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ))
                                    .toList(),
                                hint: const Text(
                                  'Pilih Agama',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black87,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                menuMaxHeight: 300,
                                value: religion,
                                onChanged: (value) {
                                  setState(() {
                                    religion = value as String;
                                    print(religion);
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                        const CustomTitleInputField(
                          title: 'Alamat Tinggal',
                        ),
                        CustomInputField(
                          controller: address,
                          maxLine: 4,
                        ),
                        const CustomTitleInputField(
                          title: 'Posisi',
                        ),
                        Container(
                          height: 48,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: greyColor)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                // icon: const ImageIcon(
                                //   AssetImage('assets/icons/arrow-down.png'),
                                // ),
                                dropdownColor: const Color(0xffF0F1F2),
                                borderRadius: BorderRadius.circular(15),
                                isExpanded: true,
                                items: itemsPosisi
                                    .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Text(
                                            item,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600,
                                              color: Colors.black87,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ))
                                    .toList(),
                                hint: const Text(
                                  'Pilih Posisi',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black87,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                menuMaxHeight: 300,
                                value: position,
                                onChanged: (value) {
                                  setState(() {
                                    position = value as String;
                                    print(position);
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                        const CustomTitleInputField(
                          title: 'Nomor Telepon',
                        ),
                        CustomInputField(
                          controller: phone,
                          maxLine: 1,
                        ),
                        const CustomTitleInputField(
                          title: 'Foto',
                        ),
                        GestureDetector(
                          onTap: () {
                            getImage();
                          },
                          child: _imageFile == null
                              ? Container(
                                  height: 250,
                                  width: 250,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(12),
                                      border: Border.all(color: greyColor)),
                                  child: _imageFile == null
                                      ? const Center(
                                          child: Icon(
                                            Icons.image,
                                            size: 40,
                                          ),
                                        )
                                      : const SizedBox())
                              : Container(
                                  height: 250,
                                  width: 250,
                                  decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    borderRadius: BorderRadius.circular(12),
                                    image: DecorationImage(
                                        image:
                                            FileImage(_imageFile ?? File('')),
                                        fit: BoxFit.cover),
                                  ),
                                  child: _imageFile == null
                                      ? const Center(
                                          child: Icon(
                                            Icons.image,
                                            size: 40,
                                          ),
                                        )
                                      : const SizedBox()),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                CustomButton(
                    press: () {
                      if (firstName.text == '' ||
                          lastName.text == '' ||
                          nik.text == '' ||
                          placeOfBirth.text == '' ||
                          dateOfBirth.text == '' ||
                          address.text == '' ||
                          phone.text == '' ||
                          gender == null ||
                          religion == null ||
                          position == null ||
                          _imageFile == null) {
                        AnimatedSnackBar.material('Form tidak boleh kosong',
                                type: AnimatedSnackBarType.warning,
                                mobileSnackBarPosition:
                                    MobileSnackBarPosition.top,
                                desktopSnackBarPosition:
                                    DesktopSnackBarPosition.topCenter)
                            .show(context);
                      } else {
                        fungsiCreateAdmission();
                      }
                    },
                    title: 'Kirim')
              ],
            ),
          ),
        ),
      ),
    );
  }
}
