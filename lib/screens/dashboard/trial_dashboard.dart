import 'package:attandy_app/screens/dashboard/registration.dart';
import 'package:attandy_app/screens/tabs_screen.dart';
import 'package:attandy_app/screens/widget/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../../constants.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';

class TrialDashboard extends StatefulWidget {
  const TrialDashboard({super.key});

  @override
  State<TrialDashboard> createState() => _TrialDashboardState();
}

class _TrialDashboardState extends State<TrialDashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 300,
              child: Lottie.asset(
                "assets/lottie/51971-hello.json",
              ),
            ),
            const Text(
              'Selamat Datang',
              style: TextStyle(
                fontSize: 24,
                color: black2Color,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.bold,
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              'Silahkan isikan data diri anda terlebih dahulu sebelum menggunakan aplikasi Attandy',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 12,
                color: black2Color,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.normal,
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: [
                  Expanded(
                      child: CustomButton(
                          press: (() {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const Registration()));
                          }),
                          title: 'Lanjutkan')),
                  const SizedBox(
                    width: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 24),
                    child: GestureDetector(
                      onTap: () {
                        Alert(
                          context: context,
                          type: AlertType.warning,
                          desc: "Apakah anda yakin ingin keluar?",
                          buttons: [
                            DialogButton(
                              color: blueColor,
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              width: 120,
                              child: const Text(
                                "Batal",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                            ),
                            DialogButton(
                              color: redColor,
                              onPressed: () {
                                logout().then((value) => Navigator.of(context)
                                    .pushAndRemoveUntil(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const SplashScreen()),
                                        (route) => false));
                              },
                              width: 120,
                              child: const Text(
                                "Keluar",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                            )
                          ],
                        ).show();
                      },
                      child: Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          color: redColor.withOpacity(0.9),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: const Icon(
                          Icons.logout,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
