import 'package:flutter/material.dart';

import '../../../constants.dart';

class DashboardTutorial extends StatelessWidget {
  const DashboardTutorial({super.key, required this.ontaps});

  final VoidCallback ontaps;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Image.asset(
                  'assets/Icons/tutorial.png',
                  color: blueColor,
                  height: 18,
                  width: 18,
                ),
                const SizedBox(
                  width: 7,
                ),
                const Text(
                  'Tutorial',
                  style: TextStyle(
                      fontSize: 14,
                      fontFamily: "Roboto",
                      fontWeight: FontWeight.bold,
                      color: black2Color,
                      letterSpacing: 0.5),
                ),
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            GestureDetector(
              onTap: ontaps,
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(color: blueColor.withOpacity(0.5)),
                  color: blueColor.withOpacity(0.1),
                ),
                child: Stack(
                  children: [
                    Positioned(
                      right: 0,
                      top: 0,
                      child: Container(
                        height: 55,
                        width: 80,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.only(
                              bottomLeft: Radius.circular(12),
                              topRight: Radius.circular(12)),
                          color: blueColor.withOpacity(0.2),
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      child: Container(
                        height: 35,
                        width: 110,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.only(
                              bottomLeft: Radius.circular(12),
                              topRight: Radius.circular(12)),
                          color: blueColor.withOpacity(0.2),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 45,
                                width: 45,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    color: Colors.white),
                                child: Center(
                                  child: Image.asset(
                                    'assets/Icons/tutorial.png',
                                    color: blueColor,
                                    height: 20,
                                    width: 20,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Text(
                                    'Ada kesulitan dalam penggunaan?',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: "Roboto",
                                        fontWeight: FontWeight.bold,
                                        color: black2Color,
                                        letterSpacing: 0.2),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    'Kami sudah menyiapkan tutorial untuk kamu',
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.normal,
                                      color: black2Color,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                          const Divider(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text(
                                'Klik untuk lihat selengkapnya',
                                style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: "Roboto",
                                  fontWeight: FontWeight.w700,
                                  color: blueColor,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Image.asset(
                                  'assets/Icons/next.png',
                                  color: blueColor,
                                  height: 20,
                                  width: 20,
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }
}
