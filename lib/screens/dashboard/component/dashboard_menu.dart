import 'package:attandy_app/screens/application/dinas_admin.dart';
import 'package:attandy_app/screens/application/dinas_screen.dart';
import 'package:attandy_app/screens/application/time_off_admin.dart';
import 'package:attandy_app/screens/application/time_off_screen.dart';
import 'package:attandy_app/screens/attandaces/attandance_employee.dart';
import 'package:attandy_app/screens/attandaces/menu_report_attandance.dart';
import 'package:attandy_app/screens/news/news_screen.dart';
import 'package:attandy_app/screens/tutorial/tutorial_screen.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../application/application_screen.dart';
import '../../company/company_screen.dart';
import '../../type_leave/type_leave.dart';
import '../../users/users_screen.dart';
import '../../workshift/workshift_screen.dart';

class DashboardMenu extends StatefulWidget {
  const DashboardMenu({
    super.key,
    required this.role,
  });

  final String role;

  @override
  State<DashboardMenu> createState() => _DashboardMenuState();
}

class _DashboardMenuState extends State<DashboardMenu> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ComponenDashboardMenu(
                title: 'Pengguna',
                imageAsset: 'assets/Icons/user.png',
                ontaps: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => UsersScreen(
                                role: widget.role,
                              )));
                },
              ),
              ComponenDashboardMenu(
                title: 'Perusahaan',
                imageAsset: 'assets/Icons/company.png',
                ontaps: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const CompanyScreen()));
                },
              ),
              ComponenDashboardMenu(
                title: 'Time Off',
                imageAsset: 'assets/Icons/permohonan.png',
                ontaps: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const TimeOffAdmin()));
                },
              ),
              ComponenDashboardMenu(
                title: 'Tugas Dinas',
                imageAsset: 'assets/Icons/permohonan.png',
                ontaps: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const DinasAdmin()));
                },
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ComponenDashboardMenu(
                title: 'Shift Kerja',
                imageAsset: 'assets/Icons/work.png',
                ontaps: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const WorkshiftScreen()));
                },
              ),
              ComponenDashboardMenu(
                title: 'Laporan\nPresensi',
                imageAsset: 'assets/Icons/presensi.png',
                ontaps: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          // builder: (context) => const AttandanceEmployee()));
                          builder: (context) => const MenuReportAttandance()));
                },
              ),
              ComponenDashboardMenu(
                title: 'Tutorial',
                imageAsset: 'assets/Icons/tutorial2.png',
                ontaps: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const TutorialScreen(
                                isAdmin: 'true',
                              )));
                },
              ),
              ComponenDashboardMenu(
                title: 'Berita',
                imageAsset: 'assets/Icons/newspaper-folded.png',
                ontaps: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const NewsScreen()));
                },
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}

class DashboardMenuEmployee extends StatefulWidget {
  const DashboardMenuEmployee({
    super.key,
    required this.role,
  });

  final String role;

  @override
  State<DashboardMenuEmployee> createState() => _DashboardMenuEmployeeState();
}

class _DashboardMenuEmployeeState extends State<DashboardMenuEmployee> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ComponenDashboardMenu(
                  title: 'Pengguna',
                  imageAsset: 'assets/Icons/user.png',
                  ontaps: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => UsersScreen(
                                  role: widget.role,
                                )));
                  },
                ),
                ComponenDashboardMenu(
                  title: 'Time Off',
                  imageAsset: 'assets/Icons/permohonan.png',
                  ontaps: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const TimeOffScreen()));
                  },
                ),
                ComponenDashboardMenu(
                  title: 'Tugas Dinas',
                  imageAsset: 'assets/Icons/permohonan.png',
                  ontaps: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const DinasScreen()));
                  },
                ),
                // ComponenDashboardMenu(
                //   title: 'Presensi',
                //   imageAsset: 'assets/Icons/presensi.png',
                //   ontaps: () {
                //     // Navigator.push(context,
                //     //     MaterialPageRoute(builder: (context) => const UsersScreen()));
                //   },
                // ),
              ],
            ),
            const SizedBox(
              height: 25,
            )
          ],
        ),
      ),
    );
  }
}

class ComponenDashboardMenu extends StatelessWidget {
  const ComponenDashboardMenu(
      {super.key,
      required this.title,
      required this.imageAsset,
      required this.ontaps});

  final String title, imageAsset;
  final VoidCallback ontaps;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ontaps,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: blueColor.withOpacity(0.1)),
            child: Center(
              child: Image.asset(
                imageAsset,
                color: blueColor,
                height: 35,
                width: 35,
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 12,
              fontFamily: "Roboto",
              fontWeight: FontWeight.w700,
              color: black2Color,
            ),
          ),
        ],
      ),
    );
  }
}
