import 'package:flutter/material.dart';
import '../../../constants.dart';

class AppBarCustom extends StatelessWidget {
  const AppBarCustom({
    super.key,
    required this.colors,
    required this.sizeOpacity,
    required this.name,
    required this.role,
    required this.height,
    required this.ontaps,
    required this.count,
  });

  final Color colors;
  final double sizeOpacity, height;
  final String name, role,count;
  final VoidCallback ontaps;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      color: colors.withOpacity(sizeOpacity),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: const TextStyle(
                    fontSize: 18,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w700,
                    color: black2Color,
                    letterSpacing: 0.5,
                  ),
                ),
                const SizedBox(
                  height: 3,
                ),
                Text(
                  role,
                  style: const TextStyle(
                    fontSize: 16,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w700,
                    color: blueColor,
                    letterSpacing: 0.5,
                  ),
                ),
              ],
            ),
            Row(
              children: [
                GestureDetector(
                  onTap: ontaps,
                  child: Container(
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(45),
                      color: Colors.white,
                    ),
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        const Positioned(
                          child: Center(
                            child: Icon(
                              Icons.notifications,
                              size: 25,
                              color: blueColor,
                            ),
                          ),
                        ),
                    Positioned(
                                right: -3,
                                top: -3,
                                child: Container(
                                  height: 18,
                                  width: 18,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      color: orangeColor),
                                  child: Center(
                                    child: Text(
                                      count,
                                      style: const TextStyle(
                                        fontSize: 10,
                                        fontFamily: "Roboto",
                                        fontWeight: FontWeight.w700,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
