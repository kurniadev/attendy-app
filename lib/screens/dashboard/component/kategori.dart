import 'package:attandy_app/screens/application/application_screen.dart';
import 'package:attandy_app/screens/company/company_screen.dart';
import 'package:attandy_app/screens/users/users_screen.dart';
import 'package:attandy_app/screens/workshift/workshift_screen.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';

class Kategori extends StatelessWidget {
  const Kategori({super.key, required this.role});

  final String role;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          ItemKategori(
            image: 'assets/Icons/user.png',
            title: 'Pengguna',
            colors: redColor,
            ontaps: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => UsersScreen(role: role,)));
            },
          ),
          ItemKategori(
            image: 'assets/Icons/company.png',
            title: 'Perusahaan',
            colors: Colors.green,
            ontaps: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const CompanyScreen()));
            },
          ),
          ItemKategori(
            image: 'assets/Icons/permohonan.png',
            title: 'Permohonan',
            colors: yellowColor,
            ontaps: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const ApplicationScreen()));
            },
          ),
          ItemKategori(
            image: 'assets/Icons/presensi.png',
            title: 'Presensi',
            colors: blueColor,
            ontaps: () {},
          ),
          ItemKategori(
            image: 'assets/Icons/work.png',
            title: 'Shift Kerja',
            colors: redColor,
            ontaps: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const WorkshiftScreen()));
            },
          ),
          ItemKategori(
            image: 'assets/Icons/type.png',
            title: 'Jenis Izin Cuti ',
            colors: Colors.green,
            ontaps: () {
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => const CompanyScreen()));
            },
          ),
        ],
      ),
    );
  }
}

class ItemKategori extends StatelessWidget {
  ItemKategori({
    required this.title,
    required this.image,
    required this.colors,
    required this.ontaps,
    Key? key,
  }) : super(key: key);

  String title, image;
  Color colors;
  VoidCallback ontaps;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ontaps,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: Container(
          width: 110.0,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10), color: Colors.white),
          child: Center(
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 70.0,
                      height: 70.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(110),
                          color: colors),
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Image.asset(
                          image,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      title,
                      style: const TextStyle(
                        fontSize: 12,
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w700,
                        color: black2Color,
                        letterSpacing: 0.5,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
