import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../constants.dart';
import '../../widget/custom_shimmer.dart';

class DahsboardLastPresent extends StatefulWidget {
  const DahsboardLastPresent(
      {super.key,
      required this.date,
      required this.checkIn,
      required this.checkOut,
      required this.imageCheckIn,
      required this.imageOut,
      required this.isLate,
      required this.text});

  final String date, checkIn, checkOut, imageCheckIn, isLate;
  final dynamic imageOut;
  final Color text;

  @override
  State<DahsboardLastPresent> createState() => _DahsboardLastPresentState();
}

class _DahsboardLastPresentState extends State<DahsboardLastPresent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          border: Border.all(
            color: black2Color.withOpacity(0.2),
          ),
          color: Colors.white),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.date,
                  style: const TextStyle(
                      fontSize: 14,
                      fontFamily: "Roboto",
                      fontWeight: FontWeight.bold,
                      color: black2Color,
                      letterSpacing: 0.5),
                ),
                widget.imageOut == null
                    ? Container(
                        height: 13,
                        width: 13,
                        decoration: BoxDecoration(
                            color: redColor,
                            borderRadius: BorderRadius.circular(10)),
                      )
                    : Container(
                        height: 16,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: widget.text.withOpacity(0.2)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Center(
                            child: Text(
                              widget.isLate,
                              style: TextStyle(
                                fontSize: 12,
                                fontFamily: "Roboto",
                                fontWeight: FontWeight.bold,
                                color: widget.text,
                              ),
                            ),
                          ),
                        ),
                      )
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Row(
                  children: [
                    Container(
                      height: 55,
                      width: 55,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: CachedNetworkImage(
                        imageUrl: widget.imageCheckIn,
                        fit: BoxFit.cover,
                        imageBuilder: (context, imageProvider) => Container(
                          width: 55.0,
                          height: 55.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            image: DecorationImage(
                                image: imageProvider, fit: BoxFit.cover),
                          ),
                        ),
                        placeholder: (context, url) => const CustomShimmer(
                            height: 55, width: 55, radius: 12),
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.error),
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Masuk',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.normal,
                              color: black2Color,
                              letterSpacing: 0.5),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          widget.checkIn,
                          style: const TextStyle(
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.bold,
                              color: black2Color,
                              letterSpacing: 0.5),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  width: 30,
                ),
                Row(
                  children: [
                    widget.imageOut == null
                        ? Container(
                            height: 55,
                            width: 55,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                image: const DecorationImage(
                                    image: AssetImage(
                                        'assets/Background/image-not-available.jpg'),
                                    fit: BoxFit.cover)),
                          )
                        : Container(
                            height: 55,
                            width: 55,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: CachedNetworkImage(
                              imageUrl: widget.imageOut,
                              fit: BoxFit.cover,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                width: 55.0,
                                height: 55.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                              placeholder: (context, url) =>
                                  const CustomShimmer(
                                      height: 55, width: 55, radius: 12),
                              errorWidget: (context, url, error) =>
                                  const Icon(Icons.error),
                            ),
                          ),
                    const SizedBox(
                      width: 5,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Keluar',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.normal,
                              color: black2Color,
                              letterSpacing: 0.5),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        widget.imageOut == null
                            ? const Text(
                                '--:--:--',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontFamily: "Roboto",
                                    fontWeight: FontWeight.bold,
                                    color: black2Color,
                                    letterSpacing: 0.5),
                              )
                            : Text(
                                widget.checkOut,
                                style: const TextStyle(
                                    fontSize: 14,
                                    fontFamily: "Roboto",
                                    fontWeight: FontWeight.bold,
                                    color: black2Color,
                                    letterSpacing: 0.5),
                              ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}

class DahsboardNoPresent extends StatefulWidget {
  const DahsboardNoPresent({
    super.key,
    required this.date,
  });

  final String date;

  @override
  State<DahsboardNoPresent> createState() => _DahsboardNoPresentState();
}

class _DahsboardNoPresentState extends State<DahsboardNoPresent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          border: Border.all(
            color: black2Color.withOpacity(0.2),
          ),
          color: redColor.withOpacity(0.1)),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.date,
                  style: const TextStyle(
                      fontSize: 14,
                      fontFamily: "Roboto",
                      fontWeight: FontWeight.bold,
                      color: black2Color,
                      letterSpacing: 0.5),
                ),
                Container(
                  height: 16,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: redColor.withOpacity(0.2)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Center(
                      child: Text(
                        'Tidak Masuk',
                        style: TextStyle(
                          fontSize: 12,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.bold,
                          color: redColor,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Row(
                  children: [
                    Container(
                      height: 55,
                      width: 55,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12),
                          image: const DecorationImage(
                              image: AssetImage(
                                  'assets/Background/image-not-available.jpg'),
                              fit: BoxFit.cover)),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text(
                          'Masuk',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.normal,
                              color: black2Color,
                              letterSpacing: 0.5),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          '--:--:--',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.bold,
                              color: black2Color,
                              letterSpacing: 0.5),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  width: 30,
                ),
                Row(
                  children: [
                    Container(
                      height: 55,
                      width: 55,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12),
                          image: const DecorationImage(
                              image: AssetImage(
                                  'assets/Background/image-not-available.jpg'),
                              fit: BoxFit.cover)),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text(
                          'Keluar',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.normal,
                              color: black2Color,
                              letterSpacing: 0.5),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          '--:--:--',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.bold,
                              color: black2Color,
                              letterSpacing: 0.5),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
