import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../constants.dart';

class BarPresensi extends StatefulWidget {
  const BarPresensi(
      {super.key,
      required this.ontaps,
      required this.shiftName,
      required this.time});

  final VoidCallback ontaps;
  final String shiftName, time;

  @override
  State<BarPresensi> createState() => _BarPresensiState();
}

class _BarPresensiState extends State<BarPresensi> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 15,
      right: 15,
      top: -45,
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 5,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Stack(
          children: [
            Positioned(
              right: 0,
              child: Opacity(
                opacity: 0.8,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(12.0),
                  child: Image.asset(
                    'assets/Background/Border@3x.png',
                    height: 70,
                    width: 70,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 5,
                      ),
                      const Text(
                        'Kamu belum absen hari ini',
                        style: TextStyle(
                          fontSize: 15,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.w700,
                          color: black2Color,
                          letterSpacing: 0.5,
                        ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Text(
                            '${widget.shiftName} : ',
                            style: const TextStyle(
                              fontSize: 13,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.bold,
                              color: black2Color,
                              letterSpacing: 0.5,
                            ),
                          ),
                          Text(
                            widget.time,
                            style: const TextStyle(
                              fontSize: 13,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.normal,
                              color: black2Color,
                              letterSpacing: 0.5,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: const [
                          Expanded(
                            child: Text(
                              'Segera lakukan absen dengan menekan tombol dibawah',
                              style: TextStyle(
                                fontSize: 13,
                                fontFamily: "Roboto",
                                fontWeight: FontWeight.normal,
                                color: black2Color,
                                letterSpacing: 0.5,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                            width: 50,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 8),
                GestureDetector(
                  onTap: widget.ontaps,
                  child: Container(
                    height: 45,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(15),
                        bottomRight: Radius.circular(15),
                      ),
                      color: orangeColor.withOpacity(0.1),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            'Absen Sekarang',
                            style: TextStyle(
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.w700,
                              color: orangeColor,
                            ),
                          ),
                          Container(
                            height: 35,
                            width: 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: orangeColor),
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Image.asset(
                                'assets/Icons/next.png',
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class BarPresensiLoading extends StatefulWidget {
  const BarPresensiLoading({super.key});

  @override
  State<BarPresensiLoading> createState() => _BarPresensiLoadingState();
}

class _BarPresensiLoadingState extends State<BarPresensiLoading> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 15,
      right: 15,
      top: -45,
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 5,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Center(
          child: SizedBox(
            height: 150,
            width: MediaQuery.of(context).size.width,
            child: Lottie.asset(
              "assets/lottie/loading.json",
            ),
          ),
        ),
      ),
    );
  }
}

class BarPresensiNoWorkShift extends StatefulWidget {
  const BarPresensiNoWorkShift({super.key});

  @override
  State<BarPresensiNoWorkShift> createState() => _BarPresensiNoWorkShiftState();
}

class _BarPresensiNoWorkShiftState extends State<BarPresensiNoWorkShift> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 15,
      right: 15,
      top: -45,
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 5,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Stack(
          children: [
            Positioned(
              right: 0,
              child: Opacity(
                opacity: 0.8,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(12.0),
                  child: Image.asset(
                    'assets/Background/Border@3x.png',
                    height: 70,
                    width: 70,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Shift kerja belum ditentukan',
                        style: TextStyle(
                          fontSize: 15,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.w700,
                          color: black2Color,
                          letterSpacing: 0.5,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Tunggu terlebih dahulu',
                        style: TextStyle(
                          fontSize: 13,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.normal,
                          color: black2Color,
                          letterSpacing: 0.5,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 45,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15),
                    ),
                    color: orangeColor.withOpacity(0.1),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class BarPresensiNoWork extends StatefulWidget {
  const BarPresensiNoWork({super.key});

  @override
  State<BarPresensiNoWork> createState() => _BarPresensiNoWorkState();
}

class _BarPresensiNoWorkState extends State<BarPresensiNoWork> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 15,
      right: 15,
      top: -45,
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 5,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Stack(
          children: [
            Positioned(
              right: 0,
              child: Opacity(
                opacity: 0.8,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(12.0),
                  child: Image.asset(
                    'assets/Background/Border@3x.png',
                    height: 70,
                    width: 70,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Hari ini hari libur',
                        style: TextStyle(
                          fontSize: 15,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.w700,
                          color: black2Color,
                          letterSpacing: 0.5,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Selamat beristirahat jangan lupa makan yaa',
                        style: TextStyle(
                          fontSize: 13,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.normal,
                          color: black2Color,
                          letterSpacing: 0.5,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 45,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15),
                    ),
                    color: orangeColor.withOpacity(0.1),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class BarPresensiOngoing extends StatefulWidget {
  const BarPresensiOngoing(
      {super.key,
      required this.ontaps,
      required this.firstName,
      required this.time,
      required this.islat,
      required this.pulang});

  final VoidCallback ontaps;
  final String firstName, time, islat, pulang;

  @override
  State<BarPresensiOngoing> createState() => _BarPresensiOngoingState();
}

class _BarPresensiOngoingState extends State<BarPresensiOngoing> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 15,
      right: 15,
      top: -45,
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 5,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Stack(
          children: [
            Positioned(
              right: 0,
              child: Opacity(
                opacity: 0.8,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(12.0),
                  child: Image.asset(
                    'assets/Background/Border@3x.png',
                    height: 70,
                    width: 70,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Semangat Hari ini ${widget.firstName}',
                        style: const TextStyle(
                          fontSize: 15,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.w700,
                          color: black2Color,
                          letterSpacing: 0.5,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Container(
                            height: 24,
                            width: 24,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: blueColor.withOpacity(0.1),
                            ),
                            child: const Icon(
                              Icons.login,
                              color: blueColor,
                              size: 16,
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          const Text(
                            'Absen Masuk Jam ',
                            style: TextStyle(
                              fontSize: 13,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.normal,
                              color: black2Color,
                            ),
                          ),
                          Text(
                            widget.time,
                            style: const TextStyle(
                              fontSize: 13,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.bold,
                              color: black2Color,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          widget.islat == 'Terlambat'
                              ? Container(
                                  height: 24,
                                  width: 24,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: blueColor.withOpacity(0.1),
                                  ),
                                  child: const Icon(
                                    Icons.close,
                                    color: blueColor,
                                    size: 16,
                                  ),
                                )
                              : Container(
                                  height: 24,
                                  width: 24,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: blueColor.withOpacity(0.1),
                                  ),
                                  child: const Icon(
                                    Icons.check_box,
                                    color: blueColor,
                                    size: 16,
                                  ),
                                ),
                          const SizedBox(
                            width: 5,
                          ),
                          Text(
                            'Presensi masuk ${widget.islat}',
                            style: const TextStyle(
                              fontSize: 13,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.normal,
                              color: black2Color,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: widget.ontaps,
                  child: Container(
                    height: 45,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(15),
                        bottomRight: Radius.circular(15),
                      ),
                      color: redColor.withOpacity(0.1),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Absen Pulang Sebelum Jam ${widget.pulang}',
                            style: const TextStyle(
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.w700,
                              color: redColor,
                            ),
                          ),
                          Container(
                            height: 35,
                            width: 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: redColor),
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Image.asset(
                                'assets/Icons/next.png',
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class HeaderCEO extends StatefulWidget {
  const HeaderCEO({super.key});

  @override
  State<HeaderCEO> createState() => _HeaderCEOState();
}

class _HeaderCEOState extends State<HeaderCEO> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 15,
      right: 15,
      top: -45,
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 5,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Stack(
          children: [
            Positioned(
              right: 0,
              child: Opacity(
                opacity: 0.8,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(12.0),
                  child: Image.asset(
                    'assets/Background/Border@3x.png',
                    height: 70,
                    width: 70,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'PT. Teknolab Caraka Internasional',
                        style: TextStyle(
                          fontSize: 15,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.w700,
                          color: black2Color,
                          letterSpacing: 0.5,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Container(
                  height: 45,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15),
                    ),
                    color: orangeColor.withOpacity(0.1),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
