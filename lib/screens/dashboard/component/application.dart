import 'package:flutter/material.dart';

import '../../../constants.dart';

class ApplicationUser extends StatelessWidget {
  const ApplicationUser({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                // border: Border.all(color: greyColor),
                color: Colors.white),
            height: 80,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: [
                  Container(
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(60),
                        image: const DecorationImage(
                            image:
                                AssetImage('assets/Background/Avatar 1.jpg'))),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: SizedBox(
                      height: 60,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text(
                            "Reza Kurnia Setiawan",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 16,
                              color: black2Color,
                              fontFamily: "PoppinsReguler",
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            "01 Februari - 05 Februari 2023",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 12,
                              fontFamily: "PoppinsReguler",
                              color: blueColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Text(
                            "Permohonan Izin Cuti",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 10,
                              color: greyColor,
                              fontFamily: "PoppinsReguler",
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 25,
                        width: 80,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: yellowColor.withOpacity(0.3),
                        ),
                        child: const Center(
                          child: Text(
                            'Tertunda',
                            style: TextStyle(
                                color: yellowColor,
                                fontSize: 12,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 25,
                        width: 80,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: orangeColor.withOpacity(0.3),
                        ),
                        child: const Center(
                          child: Text(
                            'Batal',
                            style: TextStyle(
                                color: orangeColor,
                                fontSize: 12,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
