import 'dart:async';

import 'package:attandy_app/screens/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../constants.dart';
import '../tabs_screen.dart';

class SuccessAdmission extends StatefulWidget {
  const SuccessAdmission({super.key});

  @override
  State<SuccessAdmission> createState() => _SuccessAdmissionState();
}

class _SuccessAdmissionState extends State<SuccessAdmission> {
  @override
  Widget build(BuildContext context) {
    startApp(context);
    return Scaffold(
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            height: 150,
          ),
          SizedBox(
            child: Lottie.asset(
              "assets/lottie/79952-successful.json",
            ),
          ),
          const Text(
            'Berhasil Registrasi',
            style: TextStyle(
              fontSize: 16,
              color: black2Color,
              fontFamily: 'PoppinsReguler',
              fontWeight: FontWeight.bold,
              letterSpacing: 1,
            ),
          ),
          const Text(
            'Tunggu sebentar, Sistem sedang menyimpan data anda',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 12,
              color: black2Color,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.normal,
              letterSpacing: 1,
            ),
          ),
        ],
      )),
    );
  }

  startApp(BuildContext context) {
    Timer(const Duration(seconds: 4), () {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => const TabScreen()),
          (route) => false);
    });
  }
}
