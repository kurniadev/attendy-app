// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/constants.dart';
import 'package:attandy_app/models/admission_model.dart';
import 'package:attandy_app/models/attandance_model.dart';
import 'package:attandy_app/models/notification_modal.dart';
import 'package:attandy_app/models/workshift_model.dart';
import 'package:attandy_app/screens/Notification/notification_screen.dart';
import 'package:attandy_app/screens/attandaces/attandance_screen.dart';
import 'package:attandy_app/screens/dashboard/component/app_bar.dart';
import 'package:attandy_app/screens/dashboard/component/bar_presensi.dart';
import 'package:attandy_app/screens/dashboard/component/dashboard_last_present.dart';
import 'package:attandy_app/screens/dashboard/component/dashboard_menu.dart';
import 'package:attandy_app/screens/dashboard/component/tutorial.dart';
import 'package:attandy_app/services/attandance_service.dart';
import 'package:attandy_app/services/notification_service.dart';
import 'package:attandy_app/screens/tutorial/tutorial_screen.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/api_response_model.dart';
import '../../services/admission_service.dart';
import '../../services/auth_service.dart';
import '../../services/workshift_service.dart';
import '../application/application_screen.dart';
import '../attandaces/attandance_out_screen.dart';
import '../shimmer/shimmer_presensi_bar.dart';
import '../splash_screen.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  WorkshiftModel? workshiftModel;
  String firstName = '';
  String lastName = '';
  String role = '';
  String email = '';
  String token = '';
  String chekIn = '';
  String device = '';
  String position = '';
  String? _date, _time;
  String imageUrl = '';
  String presentTime = '';
  String _debugLabelString = "";

  ScrollController _scrollController = ScrollController();
  bool _isVisible = false;

  void getDeviceInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    print('Running on ${androidInfo.model}');
    setState(() {
      device = androidInfo.model;
    });
  }

  loadPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      firstName = (prefs.getString('firstName') ?? '');
      lastName = (prefs.getString('lastName') ?? '');
      chekIn = (prefs.getString('chekin') ?? '');
      role = (prefs.getString('role') ?? '');
      position = (prefs.getString('position') ?? '');
      email = (prefs.getString('email') ?? '');
      token = (prefs.getString('token') ?? '');

      if (email == '') return;

      OneSignal.shared.setExternalUserId(email).then((results) {
        if (results == null) return;

        this.setState(() {
          _debugLabelString = "External user id set: $results";
        });
      });

      // setExternalUserIdWithPrivacyCheck(email);
      // // String externalUserId = "123456789";
      // OneSignal.shared.setExternalUserId(email).then((results) {
      //   if (results == null) return;
      //   // Now use the results of the call for each channel (push and email)
      //   print('attendy ada setnotif');
      // });
    });

    if (chekIn == 'masuk') {
      functionGetpresendayemployee();
    }
  }

  // void setExternalUserIdWithPrivacyCheck(String userId) async {
  //   await OneSignal.shared.setAppId(appIdOnesignal);
  //   await OneSignal.shared.consentGranted(true);
  //   bool userProvidedPrivacyConsent =
  //       await OneSignal.shared.userProvidedPrivacyConsent();
  //   if (userProvidedPrivacyConsent) {
  //     OneSignal.shared.setExternalUserId(userId);
  //     print('jalan');
  //   } else {
  //     print('Tidak');
  //     // Lakukan sesuatu seperti menampilkan pesan kesalahan atau menunda pemanggilan setExternalUserId() sampai pengguna memberikan persetujuan privasi.
  //   }
  // }

  bool _loading = true;
  List<dynamic> historyAllAttandancetList = [];

  Future<void> fungsiGetHistoryAttandanceAll() async {
    ApiResponse response = await getHistoryAttandanceAll();
    if (response.error == null) {
      setState(() {
        historyAllAttandancetList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  AdmissionModel? admissionModel;
  void functionGetAdmission() async {
    ApiResponse response = await getAdmission();
    if (response.error == null) {
      setState(() {
        admissionModel = response.data as AdmissionModel;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  CountNotificationModel? countNotificationModel;
  void fungsiGetCount() async {
    ApiResponse response = await getCountNotif();
    if (response.error == null) {
      setState(() {
        print('jalan skuy');
        countNotificationModel = response.data as CountNotificationModel;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  IsWorkAttandanceModel? isWorkAttandanceModel;
  void fungsiGetIsWork() async {
    ApiResponse response = await getIsWork();
    if (response.error == null) {
      setState(() {
        isWorkAttandanceModel = response.data as IsWorkAttandanceModel;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void functionGetWorkshift() async {
    ApiResponse response = await getWorkshiftAuth();
    if (response.error == null) {
      setState(() {
        workshiftModel = response.data as WorkshiftModel;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  HistoryAllAttandanceModel? historyAllAttandanceModel;
  void functionGetpresendayemployee() async {
    ApiResponse response = await getPresenDayEmployee();
    if (response.error == null) {
      setState(() {
        historyAllAttandanceModel = response.data as HistoryAllAttandanceModel;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void getTime() {
    final DateTime now = DateTime.now();
    final String formattedDate = _formatDate(now);
    setState(() {
      _date = formattedDate;
    });
  }

  String _formatDate(DateTime dateTime) {
    return DateFormat('EEEE, d MMMM yyyy', "id_ID").format(dateTime);
  }

  String _formatTime(DateTime dateTime) {
    return DateFormat('HH:mm:ss').format(dateTime);
  }

  Future<void> loadData() async {
    setState(() {
      fungsiGetIsWork();
      loadPref();
      functionGetAdmission();
      functionGetWorkshift();
      fungsiGetHistoryAttandanceAll();
    });
  }

  void _handleSetExternalUserId(
      {required String externalUserId, tokens}) async {
    // print(_externalUserId);
    if (externalUserId == '') return;

    OneSignal.shared.setExternalUserId(externalUserId, tokens).then((results) {
      if (results == null) return;
      print(results);
      setState(() {
        _debugLabelString = "External user id set: $results";
      });
    });
  }

  // void _handleRemoveExternalUserId() {
  //   OneSignal.shared.removeExternalUserId().then((results) {
  //     if (results == null) return;

  //     this.setState(() {
  //       _debugLabelString = "External user id removed: $results";
  //     });
  //   });
  // }

  @override
  void initState() {
    fungsiGetCount();
    loadPref();
    getTime();
    fungsiGetIsWork();
    getDeviceInfo();
    functionGetAdmission();
    functionGetWorkshift();
    fungsiGetHistoryAttandanceAll();
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels < 100 && _isVisible) {
        setState(() {
          _isVisible = false;
        });
      } else if (_scrollController.position.pixels >= 100 && !_isVisible) {
        setState(() {
          _isVisible = true;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor2,
      body: RefreshIndicator(
        onRefresh: loadData,
        child: SafeArea(
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                physics: ScrollPhysics(),
                controller: _scrollController,
                child: Column(
                  children: [
                    Container(
                      height: 150,
                      color: backgroundColor2,
                      width: MediaQuery.of(context).size.width,
                      child: Stack(
                        children: [
                          Positioned(
                            right: 0,
                            child: SizedBox(
                              height: 120,
                              width: 120,
                              child: Opacity(
                                  opacity: 0.3,
                                  child: Image.asset(
                                      'assets/Background/Border@3x.png')),
                            ),
                          ),
                          SafeArea(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              AppBarCustom(
                                height: 65,
                                colors: Colors.white,
                                sizeOpacity: 0.0,
                                name: admissionModel == null
                                    ? 'Memuat'
                                    : '${admissionModel!.data.firstName} ${admissionModel!.data.lastName}',
                                role: admissionModel == null
                                    ? 'Memuat'
                                    : admissionModel!.data.position,
                                ontaps: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              NotificationScreen(
                                                role: role,
                                              )));
                                },
                                count: countNotificationModel == null
                                    ? '0'
                                    : countNotificationModel!.count.toString(),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: Text(
                                  _date.toString(),
                                  style: const TextStyle(
                                    fontSize: 14,
                                    fontFamily: "Roboto",
                                    fontWeight: FontWeight.w700,
                                    color: blueColor,
                                    letterSpacing: 0.5,
                                  ),
                                ),
                              ),
                            ],
                          )),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 120,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.white,
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          workshiftModel == null
                              ? const BarPresensiLoading()
                              : workshiftModel!.workshiftId == 2
                                  ? const HeaderCEO()
                                  : workshiftModel!.workshiftId == 1
                                      ? const BarPresensiNoWorkShift()
                                      : isWorkAttandanceModel == null
                                          ? const BarPresensiLoading()
                                          : isWorkAttandanceModel!.isWork ==
                                                  'No'
                                              ? const BarPresensiNoWork()
                                              : chekIn == 'masuk'
                                                  ? BarPresensiOngoing(
                                                      ontaps: () async {
                                                        String refresh =
                                                            await Navigator
                                                                .push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder: (context) =>
                                                                AttandanceOutScreen(
                                                              time: _formatTime(
                                                                  historyAllAttandanceModel!
                                                                      .checkIn),
                                                              imageUrl:
                                                                  historyAllAttandanceModel!
                                                                      .imageCheckIn,
                                                              name:
                                                                  admissionModel!
                                                                      .data
                                                                      .firstName,
                                                            ),
                                                          ),
                                                        );
                                                        if (refresh ==
                                                            'refresh') {
                                                          loadPref();
                                                          getDeviceInfo();
                                                          functionGetAdmission();
                                                          functionGetWorkshift();
                                                          fungsiGetHistoryAttandanceAll();
                                                        }
                                                      },
                                                      pulang: workshiftModel!
                                                          .endTime,
                                                      firstName: firstName,
                                                      time: historyAllAttandanceModel ==
                                                              null
                                                          ? '--:--:--'
                                                          : _formatTime(
                                                              historyAllAttandanceModel!
                                                                  .checkIn),
                                                      islat: historyAllAttandanceModel ==
                                                              null
                                                          ? ''
                                                          : historyAllAttandanceModel!
                                                              .islate)
                                                  : BarPresensi(
                                                      shiftName: workshiftModel!
                                                          .shiftName,
                                                      time:
                                                          '${workshiftModel!.startTime}-${workshiftModel!.endTime}',
                                                      ontaps: () async {
                                                        String refresh =
                                                            await Navigator
                                                                .push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder: (context) =>
                                                                AttandanceScreen(
                                                              startTime:
                                                                  workshiftModel!
                                                                      .startTime,
                                                              endTime:
                                                                  workshiftModel!
                                                                      .endTime,
                                                              diff:
                                                                  workshiftModel!
                                                                      .diff,
                                                              deviceInfo:
                                                                  device,
                                                            ),
                                                          ),
                                                        );
                                                        if (refresh ==
                                                            'refresh') {
                                                          loadPref();
                                                          getDeviceInfo();
                                                          functionGetAdmission();
                                                          functionGetWorkshift();
                                                          fungsiGetHistoryAttandanceAll();
                                                        }
                                                      },
                                                    ),
                        ],
                      ),
                    ),
                    role.isEmpty
                        ? Container(
                            height: 120,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.white,
                          )
                        : role == 'employee'
                            ? DashboardMenuEmployee(
                                role: role,
                              )
                            : DashboardMenu(
                                role: role,
                              ),
                    const SizedBox(
                      height: 20,
                    ),
                    DashboardTutorial(
                      ontaps: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const TutorialScreen(
                                      isAdmin: 'false',
                                    )));
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    role == 'CEO'
                        ? const SizedBox()
                        : Container(
                            width: MediaQuery.of(context).size.width,
                            color: Colors.white,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Column(
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Image.asset(
                                        'assets/Icons/suitcase.png',
                                        color: blueColor,
                                        height: 18,
                                        width: 18,
                                      ),
                                      const SizedBox(
                                        width: 7,
                                      ),
                                      const Text(
                                        'Kehadiran Terakhir',
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontFamily: "Roboto",
                                            fontWeight: FontWeight.bold,
                                            color: black2Color,
                                            letterSpacing: 0.5),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                    role == 'CEO'
                        ? const SizedBox()
                        : _loading
                            ? Container(
                                height: 100,
                                width: MediaQuery.of(context).size.width,
                                color: Colors.white,
                                child: Center(
                                  child: SizedBox(
                                    height: 100,
                                    width: MediaQuery.of(context).size.width,
                                    child: Lottie.asset(
                                      "assets/lottie/loading.json",
                                    ),
                                  ),
                                ),
                              )
                            : historyAllAttandancetList.isEmpty
                                ? Container(
                                    height: 100,
                                    width: MediaQuery.of(context).size.width,
                                    color: Colors.white,
                                    child: Center(
                                      child: SizedBox(
                                        height: 100,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Lottie.asset(
                                          "assets/lottie/empty-box-blue.json",
                                        ),
                                      ),
                                    ),
                                  )
                                : ListView.builder(
                                    scrollDirection: Axis.vertical,
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount: historyAllAttandancetList.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      HistoryAllAttandanceModel
                                          historyAllAttandanceModel =
                                          historyAllAttandancetList[index];

                                      String checkOut = '';
                                      String dateCekIn = DateFormat('HH:mm:ss')
                                          .format(historyAllAttandanceModel
                                              .checkIn);
                                      if (historyAllAttandanceModel.checkOut !=
                                          null) {
                                        String dateCekOut =
                                            DateFormat('HH:mm:ss').format(
                                                DateTime.parse(
                                                    historyAllAttandanceModel
                                                        .checkOut));
                                        checkOut = dateCekOut;
                                      }
                                      return historyAllAttandanceModel.islate ==
                                              '-'
                                          ? Container(
                                              color: Colors.white,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(12.0),
                                                child: DahsboardNoPresent(
                                                  date: _formatDate(
                                                      historyAllAttandanceModel
                                                          .checkIn),
                                                ),
                                              ))
                                          : Container(
                                              color: Colors.white,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(12.0),
                                                child: DahsboardLastPresent(
                                                  date: _formatDate(
                                                      historyAllAttandanceModel
                                                          .checkIn),
                                                  checkIn: dateCekIn,
                                                  checkOut: checkOut,
                                                  imageCheckIn:
                                                      historyAllAttandanceModel
                                                          .imageCheckIn,
                                                  imageOut:
                                                      historyAllAttandanceModel
                                                          .imageCheckOut,
                                                  isLate:
                                                      historyAllAttandanceModel
                                                          .islate,
                                                  text:
                                                      historyAllAttandanceModel
                                                                  .islate ==
                                                              'Terlambat'
                                                          ? redColor
                                                          : greenColor,
                                                ),
                                              ));
                                    },
                                  ),
                    role == 'CEO'
                        ? const SizedBox()
                        : Container(
                            height: 100,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.white,
                          ),
                  ],
                ),
              ),
              AnimatedOpacity(
                duration: const Duration(milliseconds: 200),
                opacity: _isVisible ? 1.0 : 0.0,
                child: AppBarCustom(
                  count: countNotificationModel == null
                      ? '0'
                      : countNotificationModel!.count.toString(),
                  height: kToolbarHeight,
                  colors: Colors.white,
                  sizeOpacity: 1,
                  name: admissionModel == null
                      ? 'Memuat'
                      : '${admissionModel!.data.firstName} ${admissionModel!.data.lastName}',
                  role: admissionModel == null
                      ? 'Memuat'
                      : admissionModel!.data.position,
                  ontaps: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NotificationScreen(
                                  role: role,
                                )));
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
