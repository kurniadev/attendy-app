// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:attandy_app/screens/widget/custom_button.dart';
import 'package:attandy_app/services/news_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../widget/custom_input_field.dart';
import '../widget/custom_loading.dart';
import '../widget/custom_shimmer.dart';
import '../widget/custom_title_input_field.dart';

class UpdateNews extends StatefulWidget {
  const UpdateNews(
      {super.key,
      required this.imageUrl,
      required this.title,
      required this.desc,
      required this.idNews});

  final String title, desc;
  final int idNews;
  final dynamic imageUrl;

  @override
  State<UpdateNews> createState() => _UpdateNewsState();
}

class _UpdateNewsState extends State<UpdateNews> {
  TextEditingController titletext = TextEditingController();
  TextEditingController desctext = TextEditingController();
  bool loading = true;

  File? _imageCamera;
  final _picker = ImagePicker();

  Future getGalery() async {
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _imageCamera = File(pickedFile.path);
      });
    }
  }

  void fungsiUpdateNews() async {
    showAlertDialog(context);
    String? image = _imageCamera == null ? null : getStringImage(_imageCamera);
    ApiResponse response = await updateNews(
        title: titletext.text,
        description: desctext.text,
        image: image,
        newsId: widget.idNews);
    if (response.error == null) {
      setState(() {
        loading = false;
      });
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Perbarui Berita')));
      Navigator.pop(context, 'refresh');
      Navigator.pop(context);
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  @override
  void initState() {
    titletext.text = widget.title;
    desctext.text = widget.desc;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Edit Berita",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 200,
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: [
                    _imageCamera == null
                        ? CachedNetworkImage(
                            imageUrl: widget.imageUrl,
                            fit: BoxFit.cover,
                            imageBuilder: (context, imageProvider) => Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                image: DecorationImage(
                                    image: imageProvider, fit: BoxFit.cover),
                              ),
                            ),
                            placeholder: (context, url) => CustomShimmer(
                                height: 200,
                                width: MediaQuery.of(context).size.width,
                                radius: 12),
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                          )
                        : Container(
                            height: 200,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              image: DecorationImage(
                                  image: FileImage(_imageCamera ?? File('')),
                                  fit: BoxFit.cover),
                            ),
                          ),
                    Positioned(
                      right: 5,
                      bottom: 5,
                      child: GestureDetector(
                        onTap: () {
                          getGalery();
                        },
                        child: Container(
                          height: 45,
                          width: 45,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: blueColor.withOpacity(0.7)),
                          child: const Icon(
                            Icons.edit,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const CustomTitleInputField(
                title: 'Judul',
              ),
              CustomInputField(
                controller: titletext,
                maxLine: 1,
              ),
              const CustomTitleInputField(
                title: 'Deskripsi',
              ),
              CustomInputField(
                controller: desctext,
                maxLine: 10,
              ),
              const SizedBox(
                height: 15,
              ),
              CustomButton(
                  press: () {
                    fungsiUpdateNews();
                  },
                  title: 'Perbarui')
            ],
          ),
        ),
      ),
    );
  }
}
