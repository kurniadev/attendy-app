// ignore_for_file: use_build_context_synchronously

import 'dart:io';

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/services/news_service.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../widget/custom_button.dart';
import '../widget/custom_input_field.dart';
import '../widget/custom_loading.dart';
import '../widget/custom_title_input_field.dart';

class AddNews extends StatefulWidget {
  const AddNews({super.key});

  @override
  State<AddNews> createState() => _AddNewsState();
}

class _AddNewsState extends State<AddNews> {
  bool _loading = false;
  TextEditingController titletext = TextEditingController();
  TextEditingController desctext = TextEditingController();
  void fungsiCreateNews() async {
    showAlertDialog(context);
    String? image = _imageCamera == null ? null : getStringImage(_imageCamera);
    ApiResponse response = await cretaeNews(
        title: titletext.text, description: desctext.text, image: image);
    if (response.error == null) {
      setState(() {
        _loading = false;
      });
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Menambah Berita')));
      Navigator.pop(context);
      Navigator.pop(context, 'refresh');
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('Ada Kesalahan',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  File? _imageCamera;
  final _picker = ImagePicker();

  Future getGalery() async {
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _imageCamera = File(pickedFile.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Edit Berita",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  getGalery();
                },
                child: SizedBox(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    children: [
                      _imageCamera == null
                          ? Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                image: const DecorationImage(
                                    image: AssetImage(
                                        'assets/Background/image-not-available.jpg'),
                                    fit: BoxFit.cover),
                              ),
                            )
                          : Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                image: DecorationImage(
                                    image: FileImage(_imageCamera ?? File('')),
                                    fit: BoxFit.cover),
                              ),
                            ),
                      _imageCamera != null
                          ? Positioned(
                              right: 5,
                              bottom: 5,
                              child: GestureDetector(
                                onTap: () {
                                  // getGalery();
                                },
                                child: Container(
                                  height: 45,
                                  width: 45,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      color: blueColor.withOpacity(0.7)),
                                  child: const Icon(
                                    Icons.edit,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            )
                          : const SizedBox()
                    ],
                  ),
                ),
              ),
              const CustomTitleInputField(
                title: 'Judul',
              ),
              CustomInputField(
                controller: titletext,
                maxLine: 1,
              ),
              const CustomTitleInputField(
                title: 'Deskripsi',
              ),
              CustomInputField(
                controller: desctext,
                maxLine: 10,
              ),
              const SizedBox(
                height: 15,
              ),
              CustomButton(
                  press: () {
                    if (_imageCamera == null) {
                      AnimatedSnackBar.material('Gambar Harus Diisi',
                              type: AnimatedSnackBarType.error,
                              mobileSnackBarPosition:
                                  MobileSnackBarPosition.top,
                              desktopSnackBarPosition:
                                  DesktopSnackBarPosition.topCenter)
                          .show(context);
                    } else {
                      fungsiCreateNews();
                    }
                  },
                  title: 'Tambah')
            ],
          ),
        ),
      ),
    );
  }
}
