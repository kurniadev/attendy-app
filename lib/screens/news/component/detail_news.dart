import 'package:attandy_app/constants.dart';
import 'package:attandy_app/screens/attandaces/component/back_attandy.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../widget/custom_shimmer.dart';

class DetailNews extends StatefulWidget {
  const DetailNews(
      {super.key,
      required this.imageUrl,
      required this.desc,
      required this.title});

  final String imageUrl, desc, title;

  @override
  State<DetailNews> createState() => _DetailNewsState();
}

class _DetailNewsState extends State<DetailNews> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          SizedBox(
            height: 350,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: [
                Container(
                    height: 350,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: CachedNetworkImage(
                      imageUrl: widget.imageUrl,
                      fit: BoxFit.cover,
                      imageBuilder: (context, imageProvider) => Container(
                        height: 150,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          image: DecorationImage(
                              image: imageProvider, fit: BoxFit.cover),
                        ),
                      ),
                      placeholder: (context, url) => CustomShimmer(
                          height: 350,
                          width: MediaQuery.of(context).size.width,
                          radius: 12),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                    )),
                Positioned(
                  left: 10,
                  top: 10,
                  child: SafeArea(
                    child: BackAttandy(
                      press: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  child: Container(
                    height: 100,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.white.withOpacity(0.0),
                          Colors.white.withOpacity(0.5),
                          Colors.white,
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 15,
                  right: 15,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 2,
                          blurRadius: 5,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Text(
                        widget.title,
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.bold,
                          color: black2Color,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Text(
                  widget.desc,
                  style: const TextStyle(
                    fontSize: 14,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.normal,
                    color: black2Color,
                  ),
                ),
              ),
            ),
          ),
          // Expanded(
          //   child: Container(
          //     width: MediaQuery.of(context).size.width,
          //     color: Colors.white,
          //     child: Stack(clipBehavior: Clip.none, children: [
          //       Positioned(
          //         top: -100,
          //         left: 15,
          //         right: 15,
          //         child: Container(
          //           height: MediaQuery.of(context).size.height * 0.5,
          //           width: MediaQuery.of(context).size.width,
          //           decoration: BoxDecoration(
          //             borderRadius: BorderRadius.circular(12),
          //             color: Colors.white,
          //             boxShadow: [
          //               BoxShadow(
          //                 color: Colors.grey.withOpacity(0.2),
          //                 spreadRadius: 2,
          //                 blurRadius: 5,
          //                 offset: Offset(0, 3), // changes position of shadow
          //               ),
          //             ],
          //           ),
          //           child: SingleChildScrollView(
          //             child: Padding(
          //               padding: const EdgeInsets.all(12.0),
          //               child: Column(
          //                 crossAxisAlignment: CrossAxisAlignment.start,
          //                 children: [
          //                   Text(
          //                     'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
          //                     style: TextStyle(
          //                       fontSize: 18,
          //                       fontFamily: "Roboto",
          //                       fontWeight: FontWeight.bold,
          //                       color: black2Color,
          //                     ),
          //                   ),
          //                   SizedBox(
          //                     height: 10,
          //                   ),
          //                   Text(
          //                     'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          //                     style: TextStyle(
          //                       fontSize: 14,
          //                       fontFamily: "Roboto",
          //                       fontWeight: FontWeight.normal,
          //                       color: black2Color,
          //                     ),
          //                   ),
          //                   SizedBox(
          //                     height: 10,
          //                   ),
          //                   Text(
          //                     'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          //                     style: TextStyle(
          //                       fontSize: 14,
          //                       fontFamily: "Roboto",
          //                       fontWeight: FontWeight.normal,
          //                       color: black2Color,
          //                     ),
          //                   ),
          //                   SizedBox(
          //                     height: 10,
          //                   ),
          //                   Text(
          //                     'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          //                     style: TextStyle(
          //                       fontSize: 14,
          //                       fontFamily: "Roboto",
          //                       fontWeight: FontWeight.normal,
          //                       color: black2Color,
          //                     ),
          //                   ),
          //                   SizedBox(
          //                     height: 20,
          //                   ),
          //                 ],
          //               ),
          //             ),
          //           ),
          //         ),
          //       )
          //     ]),
          //   ),
          // )
        ],
      ),
    );
  }
}
