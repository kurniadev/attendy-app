import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../widget/custom_shimmer.dart';

class MainSeconNews extends StatefulWidget {
  const MainSeconNews(
      {super.key,
      required this.ontaps,
      required this.title,
      required this.description,
      required this.author,
      required this.date,
      required this.imageUrl});

  final VoidCallback ontaps;
  final String title, description, author, date;
  final dynamic imageUrl;

  @override
  State<MainSeconNews> createState() => _MainSeconNewsState();
}

class _MainSeconNewsState extends State<MainSeconNews> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: GestureDetector(
        onTap: widget.ontaps,
        child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 150,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Stack(
                    children: [
                      Container(
                          height: 150,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                spreadRadius: 2,
                                blurRadius: 4,
                                offset: const Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          child: CachedNetworkImage(
                            imageUrl: widget.imageUrl,
                            fit: BoxFit.cover,
                            imageBuilder: (context, imageProvider) => Container(
                              height: 150,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                image: DecorationImage(
                                    image: imageProvider, fit: BoxFit.cover),
                              ),
                            ),
                            placeholder: (context, url) => CustomShimmer(
                                height: 150,
                                width: MediaQuery.of(context).size.width,
                                radius: 12),
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                          )),
                      // SizedBox(
                      //   height: 150,
                      //   width: MediaQuery.of(context).size.width,
                      //   child: ClipRRect(
                      //     borderRadius: BorderRadius.circular(16),
                      //     child: Image.asset(
                      //       'assets/Background/imagenews.jpg',
                      //       fit: BoxFit.cover,
                      //     ),
                      //   ),
                      // ),
                      Positioned(
                        right: 10,
                        top: 5,
                        child: Container(
                          height: 20,
                          decoration: BoxDecoration(
                            color: blueColor,
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Row(
                            children: [
                              const SizedBox(
                                width: 15,
                              ),
                              Center(
                                child: Text(
                                  widget.date,
                                  style: const TextStyle(
                                    fontSize: 10,
                                    fontFamily: "Roboto",
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 15,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Text(
                  widget.title,
                  style: const TextStyle(
                      fontSize: 16,
                      fontFamily: "Roboto",
                      fontWeight: FontWeight.bold,
                      color: black2Color,
                      letterSpacing: 0.5),
                ),
                const SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    Icon(
                      Icons.person,
                      size: 14,
                      color: Colors.black.withOpacity(0.4),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      widget.author,
                      style: const TextStyle(
                        fontSize: 12,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.normal,
                        color: black2Color,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  widget.description,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontSize: 12,
                      fontFamily: "Roboto",
                      fontWeight: FontWeight.normal,
                      color: black2Color,
                      letterSpacing: 0.5),
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
