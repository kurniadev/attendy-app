// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/news_model.dart';
import 'package:attandy_app/screens/news/component/detail_news.dart';
import 'package:attandy_app/services/news_service.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../utils/time_ago.dart';
import 'component/main_secon_news.dart';

class MainNewsScreen extends StatefulWidget {
  const MainNewsScreen({super.key});

  @override
  State<MainNewsScreen> createState() => _MainNewsScreenState();
}

class _MainNewsScreenState extends State<MainNewsScreen> {
  bool _loading = true;
  List<dynamic> newsList = [];

  Future<void> fungsiGetNews() async {
    ApiResponse response = await getNews();
    if (response.error == null) {
      setState(() {
        newsList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  @override
  void initState() {
    fungsiGetNews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: grey2Color,
      appBar: AppBar(
        title: const Text(
          "Berita Terbaru",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: _loading
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: SizedBox(
                    height: 100,
                    width: MediaQuery.of(context).size.width,
                    child: Lottie.asset(
                      "assets/lottie/loading.json",
                    ),
                  ),
                ),
              ],
            )
          : ListView.builder(
              itemCount: newsList.length,
              itemBuilder: (BuildContext context, int index) {
                NewsModel newsModel = newsList[index];
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: MainSeconNews(
                    ontaps: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailNews(
                                    imageUrl: newsModel.image,
                                    desc: newsModel.description,
                                    title: newsModel.title,
                                  )));
                    },
                    title: newsModel.title,
                    description: newsModel.description,
                    author: newsModel.author,
                    // date: _formatDate(newsModel.createdAt),
                    date: time_passed(newsModel.createdAt, full: false),
                    imageUrl: newsModel.image,
                  ),
                );
              },
            ),
    );
  }
}
