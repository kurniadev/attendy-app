// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/screens/news/add_news.dart';
import 'package:attandy_app/screens/news/component/detail_news.dart';
import 'package:attandy_app/screens/news/component/list_news.dart';
import 'package:attandy_app/screens/news/component/news_work.dart';
import 'package:attandy_app/screens/news/update_news.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../models/news_model.dart';
import '../../services/auth_service.dart';
import '../../services/news_service.dart';
import '../splash_screen.dart';
import '../widget/custom_loading.dart';

class NewsScreen extends StatefulWidget {
  const NewsScreen({super.key});

  @override
  State<NewsScreen> createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  bool _loading = true;
  List<dynamic> newsList = [];

  Future<void> fungsiGetNews() async {
    ApiResponse response = await getNews();
    if (response.error == null) {
      setState(() {
        newsList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void fungsiDeleteNews(int newsId) async {
    showAlertDialog(context);
    ApiResponse response = await deleteNews(idNews: newsId);
    if (response.error == null) {
      Navigator.pop(context);
      setState(() {
        fungsiGetNews();
      });
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Menghapus Berita')));
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    fungsiGetNews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Berita",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
            child: GestureDetector(
              onTap: () async {
                String refresh = await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const AddNews()));
                if (refresh == 'refresh') {
                  fungsiGetNews();
                }
              },
              child: Container(
                height: 20,
                width: 35,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12), color: blueColor),
                child: const Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
        elevation: 0,
      ),
      body: _loading
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: SizedBox(
                    height: 100,
                    width: MediaQuery.of(context).size.width,
                    child: Lottie.asset(
                      "assets/lottie/loading.json",
                    ),
                  ),
                ),
              ],
            )
          : ListView.builder(
              itemCount: newsList.length,
              itemBuilder: (BuildContext context, int index) {
                NewsModel newsModel = newsList[index];
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: ListNews(
                    imageUrl: newsModel.image,
                    title: newsModel.title,
                    desc: newsModel.description,
                    detail: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailNews(
                                    imageUrl: newsModel.image,
                                    desc: newsModel.description,
                                    title: newsModel.title,
                                  )));
                    },
                    ontaps: () {
                      showModalBottomSheet(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        context: context,
                        builder: (builder) {
                          return Container(
                            height: 150.0,
                            color: Colors
                                .transparent, //could change this to Color(0xFF737373),
                            child: MenuNewsWork(
                              edit: () async {
                                String refresh = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => UpdateNews(
                                              title: newsModel.title,
                                              imageUrl: newsModel.image,
                                              desc: newsModel.description,
                                              idNews: newsModel.id,
                                            )));
                                if (refresh == 'refresh') {
                                  fungsiGetNews();
                                }
                              },
                              hapus: () {
                                Navigator.pop(context);
                                Alert(
                                  context: context,
                                  type: AlertType.warning,
                                  desc:
                                      // "Apakah anda ingin menghapus ${workshiftAdminModel.shiftName}",
                                      "Apakah anda ingin menghapus ",
                                  buttons: [
                                    DialogButton(
                                      color: redColor,
                                      onPressed: () {
                                        fungsiDeleteNews(newsModel.id);
                                      },
                                      width: 120,
                                      child: const Text(
                                        "Hapus",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    )
                                  ],
                                ).show();
                              },
                            ),
                          );
                        },
                      );
                    },
                  ),
                );
              },
            ),
    );
  }
}
