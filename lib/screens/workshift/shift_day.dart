// ignore_for_file: use_build_context_synchronously

import 'dart:ui';

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/workdays_model.dart';
import 'package:attandy_app/screens/widget/custom_shimmer.dart';
import 'package:attandy_app/services/workdays_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:lottie/lottie.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../widget/custom_loading.dart';

class ShiftDay extends StatefulWidget {
  const ShiftDay({super.key, required this.shiftName, required this.idShift});

  final String shiftName;
  final int idShift;

  @override
  State<ShiftDay> createState() => _ShiftDayState();
}

class _ShiftDayState extends State<ShiftDay> {
  bool _loading = true;
  bool _loading2 = true;
  bool _showFab = true;
  List<dynamic> workdaysList = [];
  List<dynamic> daysList = [];

  Future<void> fungsiGetWorkDays() async {
    ApiResponse response = await getWorkDays(idShift: widget.idShift);
    if (response.error == null) {
      setState(() {
        workdaysList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  Future<void> fungsiGetDay() async {
    ApiResponse response = await getWDays();
    if (response.error == null) {
      setState(() {
        daysList = response.data as List<dynamic>;
        _loading2 = _loading2 ? !_loading2 : _loading2;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void fungsiDeleteWorkDays(int idWorkDays) async {
    showAlertDialog(context);
    ApiResponse response = await deleteWordyas(idWorkDays: idWorkDays);
    if (response.error == null) {
      Navigator.pop(context);
      setState(() {
        fungsiGetWorkDays();
      });
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Menghapus Hari Kerja')));
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  void fungsicreateWorkdays({required int idDays}) async {
    showAlertDialog(context);
    ApiResponse response = await createWorkdays(
        daysId: idDays.toString(), workshiftId: widget.idShift.toString());
    if (response.error == null) {
      Navigator.pop(context);
      AnimatedSnackBar.material('Berhasil Menambahkan Hari Kerja',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        Navigator.pop(context);
        fungsiGetWorkDays();
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('Ada Kesalahan',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    fungsiGetWorkDays();
    fungsiGetDay();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const duration = Duration(milliseconds: 300);
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: Text(
          "Hari Kerja ${widget.shiftName}",
          style: const TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      floatingActionButton: AnimatedSlide(
        duration: duration,
        offset: _showFab ? Offset.zero : const Offset(0, 2),
        child: AnimatedOpacity(
          duration: duration,
          opacity: _showFab ? 1 : 0,
          child: FloatingActionButton(
            backgroundColor: blueColor,
            child: const Icon(Icons.add),
            onPressed: () async {
              showGeneralDialog(
                barrierDismissible: true,
                barrierLabel: '',
                barrierColor: Colors.black38,
                transitionDuration: const Duration(milliseconds: 300),
                pageBuilder: (ctx, anim1, anim2) => Scaffold(
                  backgroundColor: Colors.transparent,
                  body: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: blueColor,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.2),
                                  spreadRadius: 2,
                                  blurRadius: 5,
                                  offset: const Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12),
                              child: Row(
                                children: [
                                  const Expanded(
                                    child: Text(
                                      'Pilih Untuk Menambahkan',
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.w700,
                                        letterSpacing: 1,
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: const SizedBox(
                                      height: 50,
                                      width: 50,
                                      child: Icon(
                                        Icons.close,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )),
                      ),
                      SizedBox(
                        height: 500,
                        width: MediaQuery.of(context).size.width,
                        child: ListView.builder(
                          itemCount: daysList.length,
                          itemBuilder: (BuildContext context, int index) {
                            DaysModel daysModel = daysList[index];
                            return Padding(
                              padding: const EdgeInsets.all(12),
                              child: Column(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      fungsicreateWorkdays(
                                          idDays: daysModel.dayId);
                                    },
                                    child: Container(
                                      height: 50,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.2),
                                            spreadRadius: 2,
                                            blurRadius: 5,
                                            offset: const Offset(0,
                                                3), // changes position of shadow
                                          ),
                                        ],
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                'Hari ${daysModel.dayName}',
                                                style: const TextStyle(
                                                  fontSize: 18,
                                                  color: black2Color,
                                                  fontFamily: 'Roboto',
                                                  fontWeight: FontWeight.w700,
                                                  letterSpacing: 1,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                transitionBuilder: (ctx, anim1, anim2, child) => BackdropFilter(
                  filter: ImageFilter.blur(
                      sigmaX: 4 * anim1.value, sigmaY: 4 * anim1.value),
                  child: FadeTransition(
                    child: child,
                    opacity: anim1,
                  ),
                ),
                context: context,
              );
            },
          ),
        ),
      ),
      body: NotificationListener<UserScrollNotification>(
        onNotification: (notification) {
          final ScrollDirection direction = notification.direction;
          setState(() {
            if (direction == ScrollDirection.reverse) {
              _showFab = false;
            } else if (direction == ScrollDirection.forward) {
              _showFab = true;
            }
          });
          return true;
        },
        child: _loading
            ? Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: CustomShimmer(
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                        radius: 12),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: CustomShimmer(
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                        radius: 12),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: CustomShimmer(
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                        radius: 12),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: CustomShimmer(
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                        radius: 12),
                  ),
                ],
              )
            : ListView.builder(
                itemCount: workdaysList.length,
                itemBuilder: (BuildContext context, int index) {
                  WorkdaysModel workdaysModel = workdaysList[index];
                  return Padding(
                    padding: const EdgeInsets.all(12),
                    child: Column(
                      children: [
                        Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset: const Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    'Hari ${workdaysModel.dayName}',
                                    style: const TextStyle(
                                      fontSize: 18,
                                      color: black2Color,
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.w700,
                                      letterSpacing: 1,
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Alert(
                                      context: context,
                                      type: AlertType.warning,
                                      desc:
                                          "Apakah anda ingin menghapus Hari ${workdaysModel.dayName}",
                                      buttons: [
                                        DialogButton(
                                          color: redColor,
                                          onPressed: () {
                                            fungsiDeleteWorkDays(int.parse(
                                                workdaysModel.workdaysId
                                                    .toString()));
                                          },
                                          width: 120,
                                          child: const Text(
                                            "Hapus",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 20),
                                          ),
                                        )
                                      ],
                                    ).show();
                                  },
                                  child: const SizedBox(
                                    height: 50,
                                    width: 50,
                                    child: Icon(Icons.delete_outline),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
      ),
    );
  }
}
