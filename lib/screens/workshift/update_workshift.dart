// ignore_for_file: use_build_context_synchronously

import 'dart:ui';

import 'package:attandy_app/services/workshift_service.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../widget/custom_button.dart';
import '../widget/custom_loading.dart';

class UpdateWorkshift extends StatefulWidget {
  const UpdateWorkshift(
      {super.key,
      required this.startTime,
      required this.endTime,
      required this.maxTime,
      required this.shiftName,
      required this.idShift});
  final String startTime, endTime, maxTime, shiftName;
  final int idShift;

  @override
  State<UpdateWorkshift> createState() => _UpdateWorkshiftState();
}

class _UpdateWorkshiftState extends State<UpdateWorkshift> {
  TextEditingController name = TextEditingController();
  TextEditingController start = TextEditingController();
  TextEditingController end = TextEditingController();
  TextEditingController maxArival = TextEditingController();

  bool loading = true;

  void getTime() {
    DateTime dateTimeStart = DateFormat("hh:mm").parse(widget.startTime);
    DateTime dateTimeEnd = DateFormat("hh:mm").parse(widget.endTime);
    DateTime dateTimeMax = DateFormat("hh:mm").parse(widget.maxTime);

    TimeOfDay timeStart = TimeOfDay.fromDateTime(dateTimeStart);
    TimeOfDay timeEnd = TimeOfDay.fromDateTime(dateTimeEnd);
    TimeOfDay timeMax = TimeOfDay.fromDateTime(dateTimeMax);

    setState(() {
      start.text =
          '${timeStart.hour.toString().padLeft(2, '0')}:${timeStart.minute.toString().padLeft(2, '0')}';
      end.text =
          '${timeEnd.hour.toString().padLeft(2, '0')}:${timeEnd.minute.toString().padLeft(2, '0')}';
      maxArival.text =
          '${timeMax.hour.toString().padLeft(2, '0')}:${timeMax.minute.toString().padLeft(2, '0')}';
      name.text = widget.shiftName;
    });
  }

  void fungsiUpdateWorkshift() async {
    showAlertDialog(context);
    ApiResponse response = await updateWorkshift(
        shiftName: name.text,
        startTime: start.text,
        endTime: end.text,
        maxArrival: maxArival.text,
        idShift: widget.idShift);
    if (response.error == null) {
      setState(() {
        loading = false;
      });
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Perbarui Shift Kerja')));
      Navigator.pop(context, 'refresh');
      Navigator.pop(context);
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  @override
  void initState() {
    getTime();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Edit Shift Kerja",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(6.0),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Nama Shift Kerja",
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 16),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12)),
                      child: TextFormField(
                        controller: name,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const Text(
                    "Absensi Masuk",
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 16),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12)),
                      child: TextFormField(
                        controller: start,
                        readOnly: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          // hintText: 'dd/mm/yyyy',
                        ),
                        onTap: () async {
                          DateTime dateTime =
                              DateFormat("hh:mm").parse(widget.startTime);
                          TimeOfDay timeOfDay =
                              TimeOfDay.fromDateTime(dateTime);
                          TimeOfDay? timepicked = await showTimePicker(
                            context: context,
                            initialTime: timeOfDay,
                            builder: (context, child) {
                              return Theme(
                                data: ThemeData.light().copyWith(
                                  colorScheme: const ColorScheme.light(
                                    // change the border color
                                    primary: blueColor,
                                    // change the text color
                                    onSurface: black2Color,
                                  ),
                                  // button colors
                                  buttonTheme: const ButtonThemeData(
                                    colorScheme: ColorScheme.light(
                                      primary: Colors.green,
                                    ),
                                  ),
                                ),
                                child: child!,
                              );
                            },
                          );
                          if (timepicked != null) {
                            setState(() {
                              start.text =
                                  '${timepicked.hour.toString().padLeft(2, '0')}:${timepicked.minute.toString().padLeft(2, '0')}';
                            });
                          } else {
                            print("Time is not selected");
                          }
                        },
                      ),
                    ),
                  ),
                  const Text(
                    "Absesni Masuk Maksimal",
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 16),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12)),
                      child: TextFormField(
                        controller: maxArival,
                        readOnly: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          // hintText: 'dd/mm/yyyy',
                        ),
                        onTap: () async {
                          DateTime dateTime =
                              DateFormat("hh:mm").parse(widget.maxTime);
                          TimeOfDay timeOfDay =
                              TimeOfDay.fromDateTime(dateTime);
                          TimeOfDay? timepicked = await showTimePicker(
                            context: context,
                            initialTime: timeOfDay,
                            builder: (context, child) {
                              return Theme(
                                data: ThemeData.light().copyWith(
                                  colorScheme: const ColorScheme.light(
                                    // change the border color
                                    primary: blueColor,
                                    // change the text color
                                    onSurface: black2Color,
                                  ),
                                  // button colors
                                  buttonTheme: const ButtonThemeData(
                                    colorScheme: ColorScheme.light(
                                      primary: Colors.green,
                                    ),
                                  ),
                                ),
                                child: child!,
                              );
                            },
                          );
                          if (timepicked != null) {
                            setState(() {
                              maxArival.text =
                                  '${timepicked.hour.toString().padLeft(2, '0')}:${timepicked.minute.toString().padLeft(2, '0')}';
                            });
                          } else {
                            print("Time is not selected");
                          }
                        },
                      ),
                    ),
                  ),
                  const Text(
                    "Absesni Keluar",
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 16),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12)),
                      child: TextFormField(
                        controller: end,
                        readOnly: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          // hintText: 'dd/mm/yyyy',
                        ),
                        onTap: () async {
                          DateTime dateTime =
                              DateFormat("hh:mm").parse(widget.endTime);
                          TimeOfDay timeOfDay =
                              TimeOfDay.fromDateTime(dateTime);
                          TimeOfDay? timepicked = await showTimePicker(
                            context: context,
                            initialTime: timeOfDay,
                            builder: (context, child) {
                              return Theme(
                                data: ThemeData.light().copyWith(
                                  colorScheme: const ColorScheme.light(
                                    // change the border color
                                    primary: blueColor,
                                    // change the text color
                                    onSurface: black2Color,
                                  ),
                                  // button colors
                                  buttonTheme: const ButtonThemeData(
                                    colorScheme: ColorScheme.light(
                                      primary: Colors.green,
                                    ),
                                  ),
                                ),
                                child: child!,
                              );
                            },
                          );
                          if (timepicked != null) {
                            setState(() {
                              end.text =
                                  '${timepicked.hour.toString().padLeft(2, '0')}:${timepicked.minute.toString().padLeft(2, '0')}';
                            });
                          } else {
                            print("Time is not selected");
                          }
                        },
                      ),
                    ),
                  ),
                  CustomButton(
                    press: () {
                      setState(() {
                        fungsiUpdateWorkshift();
                      });
                    },
                    title: 'Perbarui',
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
