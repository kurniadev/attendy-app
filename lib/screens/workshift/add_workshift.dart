// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/services/workshift_service.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../widget/custom_button.dart';
import '../widget/custom_loading.dart';

class AddWorkshift extends StatefulWidget {
  const AddWorkshift({super.key});

  @override
  State<AddWorkshift> createState() => _AddWorkshiftState();
}

class _AddWorkshiftState extends State<AddWorkshift> {
  bool _loading = false;
  TextEditingController name = TextEditingController();
  TextEditingController start = TextEditingController();
  TextEditingController end = TextEditingController();
  TextEditingController maxArival = TextEditingController();

  void fungsiCreateNews() async {
    showAlertDialog(context);
    ApiResponse response = await createWorkshift(
        shiftName: name.text,
        startTime: start.text,
        endTime: end.text,
        maxArival: maxArival.text);
    if (response.error == null) {
      Navigator.pop(context);
      AnimatedSnackBar.material('Berhasil Menambahkan Shift Kerja',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        Navigator.pop(context, 'refresh');
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('Ada Kesalahan',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Tambah Shift Kerja",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(6.0),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Nama Shift Kerja",
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 16),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12)),
                      child: TextFormField(
                        controller: name,
                        decoration: InputDecoration(
                          hintText: 'Masukkan Nama Shift Kerja',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const Text(
                    "Absensi Masuk",
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 16),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12)),
                      child: TextFormField(
                        controller: start,
                        // readOnly: true,
                        decoration: InputDecoration(
                          hintText: 'Pilih Waktu Absensi Masuk',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          // hintText: 'dd/mm/yyyy',
                        ),
                        onTap: () async {
                          TimeOfDay? timepicked = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay.now(),
                            builder: (context, child) {
                              return Theme(
                                data: ThemeData.light().copyWith(
                                  colorScheme: const ColorScheme.light(
                                    // change the border color
                                    primary: blueColor,
                                    // change the text color
                                    onSurface: black2Color,
                                  ),
                                  // button colors
                                  buttonTheme: const ButtonThemeData(
                                    colorScheme: ColorScheme.light(
                                      primary: Colors.green,
                                    ),
                                  ),
                                ),
                                child: child!,
                              );
                            },
                          );
                          if (timepicked != null) {
                            setState(() {
                              start.text =
                                  '${timepicked.hour.toString().padLeft(2, '0')}:${timepicked.minute.toString().padLeft(2, '0')}';
                              print(start.text);
                            });
                          } else {
                            print("Time is not selected");
                          }
                        },
                      ),
                    ),
                  ),
                  const Text(
                    "Absensi Masuk Maksimal",
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 16),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12)),
                      child: TextFormField(
                        controller: maxArival,
                        readOnly: true,
                        decoration: InputDecoration(
                          hintText: 'Pilih Absensi Masuk Maksimal',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          // hintText: 'dd/mm/yyyy',
                        ),
                        onTap: () async {
                          TimeOfDay? timepicked = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay.now(),
                            builder: (context, child) {
                              return Theme(
                                data: ThemeData.light().copyWith(
                                  colorScheme: const ColorScheme.light(
                                    // change the border color
                                    primary: blueColor,
                                    // change the text color
                                    onSurface: black2Color,
                                  ),
                                  // button colors
                                  buttonTheme: const ButtonThemeData(
                                    colorScheme: ColorScheme.light(
                                      primary: Colors.green,
                                    ),
                                  ),
                                ),
                                child: child!,
                              );
                            },
                          );
                          if (timepicked != null) {
                            setState(() {
                              maxArival.text =
                                  '${timepicked.hour.toString().padLeft(2, '0')}:${timepicked.minute.toString().padLeft(2, '0')}';
                            });
                          } else {
                            print("Time is not selected");
                          }
                        },
                      ),
                    ),
                  ),
                  const Text(
                    "Absesni Pulang",
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 16),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12)),
                      child: TextFormField(
                        controller: end,
                        readOnly: true,
                        decoration: InputDecoration(
                          hintText: 'Pilih Absensi Pulang',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          // hintText: 'dd/mm/yyyy',
                        ),
                        onTap: () async {
                          TimeOfDay? timepicked = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay.now(),
                            builder: (context, child) {
                              return Theme(
                                data: ThemeData.light().copyWith(
                                  colorScheme: const ColorScheme.light(
                                    // change the border color
                                    primary: blueColor,
                                    // change the text color
                                    onSurface: black2Color,
                                  ),
                                  // button colors
                                  buttonTheme: const ButtonThemeData(
                                    colorScheme: ColorScheme.light(
                                      primary: Colors.green,
                                    ),
                                  ),
                                ),
                                child: child!,
                              );
                            },
                          );
                          if (timepicked != null) {
                            setState(() {
                              end.text =
                                  '${timepicked.hour.toString().padLeft(2, '0')}:${timepicked.minute.toString().padLeft(2, '0')}';
                            });
                          } else {
                            print("Time is not selected");
                          }
                        },
                      ),
                    ),
                  ),
                  CustomButton(
                    press: () {
                      fungsiCreateNews();
                    },
                    title: 'Tambah',
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
