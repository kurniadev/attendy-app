import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';

class WorkshiftList extends StatefulWidget {
  const WorkshiftList({
    super.key,
    required this.shiftName,
    required this.startTime,
    required this.endTime,
    required this.pressDay,
    required this.pressOption,
  });

  final String shiftName, startTime, endTime;
  final VoidCallback pressOption, pressDay;

  @override
  State<WorkshiftList> createState() => _WorkshiftListState();
}

class _WorkshiftListState extends State<WorkshiftList> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      child: Container(
          height: 80,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            border: Border.all(color: blueColor, width: 2),
          ),
          child: Row(
            children: [
              GestureDetector(
                onTap: widget.pressDay,
                child: Container(
                  height: 80,
                  width: 70,
                  decoration: const BoxDecoration(
                    color: blueColor,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      topLeft: Radius.circular(10),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Image.asset(
                      'assets/Icons/work.png',
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: GestureDetector(
                  onTap: widget.pressDay,
                  child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.shiftName,
                            style: const TextStyle(
                              fontSize: 16,
                              color: black2Color,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w800,
                              letterSpacing: 1,
                            ),
                          ),
                          const Text(
                            "PT. Teknolab Caraka Internasional",
                            style: TextStyle(
                              fontSize: 10,
                              color: black2Color,
                              // fontFamily: 'PoppinsReguler',
                              fontWeight: FontWeight.w400,
                              // letterSpacing: 1,
                            ),
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                Row(
                                  children: [
                                    const Icon(
                                      CupertinoIcons.clock,
                                      size: 14,
                                    ),
                                    Text(
                                      " Masuk : ${widget.startTime}",
                                      style: const TextStyle(
                                        fontSize: 12,
                                        color: black2Color,
                                        // fontFamily: 'PoppinsReguler',
                                        fontWeight: FontWeight.w400,
                                        // letterSpacing: 1,
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Row(
                                  children: [
                                    const Icon(
                                      CupertinoIcons.clock,
                                      size: 14,
                                    ),
                                    Text(
                                      " keluar : ${widget.endTime}",
                                      style: const TextStyle(
                                        fontSize: 12,
                                        color: black2Color,
                                        // fontFamily: 'PoppinsReguler',
                                        fontWeight: FontWeight.w400,
                                        // letterSpacing: 1,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: widget.pressOption,
                child: SizedBox(
                  height: 25,
                  width: 25,
                  child: Image.asset('assets/Icons/three-dots.png'),
                ),
              )
            ],
          )),
    );
  }
}
