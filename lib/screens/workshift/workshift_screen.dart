// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/constants.dart';
import 'package:attandy_app/models/workshift_model.dart';
import 'package:attandy_app/screens/workshift/add_workshift.dart';
import 'package:attandy_app/screens/workshift/component/list_workshift.dart';
import 'package:attandy_app/screens/workshift/component/menu_workshift.dart';
import 'package:attandy_app/screens/workshift/shift_day.dart';
import 'package:attandy_app/screens/workshift/update_workshift.dart';
import 'package:attandy_app/services/workshift_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../shimmer/shimmer_list_workshift.dart';
import '../splash_screen.dart';
import '../widget/custom_loading.dart';

class WorkshiftScreen extends StatefulWidget {
  const WorkshiftScreen({super.key});

  @override
  State<WorkshiftScreen> createState() => _WorkshiftScreenState();
}

class _WorkshiftScreenState extends State<WorkshiftScreen> {
  bool _showFab = true;
  bool _loading = true;
  List<dynamic> workshiftList = [];

  Future<void> fungsiGetWorkshift() async {
    ApiResponse response = await getWorkshift();
    if (response.error == null) {
      setState(() {
        workshiftList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void fungsiDeleteWorkshift(int fasId) async {
    showAlertDialog(context);
    ApiResponse response = await deleteWorkshift(idShift: fasId);
    if (response.error == null) {
      Navigator.pop(context);
      setState(() {
        fungsiGetWorkshift();
      });
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Menghapus Shift Kerja')));
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    fungsiGetWorkshift();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const duration = Duration(milliseconds: 300);
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Shift Kerja",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      floatingActionButton: AnimatedSlide(
        duration: duration,
        offset: _showFab ? Offset.zero : const Offset(0, 2),
        child: AnimatedOpacity(
          duration: duration,
          opacity: _showFab ? 1 : 0,
          child: FloatingActionButton(
            backgroundColor: blueColor,
            child: const Icon(Icons.add),
            onPressed: () async {
              String refresh = await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const AddWorkshift()));
              if (refresh == 'refresh') {
                fungsiGetWorkshift();
              }
            },
          ),
        ),
      ),
      body: NotificationListener<UserScrollNotification>(
        onNotification: (notification) {
          final ScrollDirection direction = notification.direction;
          setState(() {
            if (direction == ScrollDirection.reverse) {
              _showFab = false;
            } else if (direction == ScrollDirection.forward) {
              _showFab = true;
            }
          });
          return true;
        },
        child: _loading
            ? const SingleChildScrollView(child: ShimmerListWorkshift())
            : ListView.builder(
                itemCount: workshiftList.length,
                itemBuilder: (BuildContext context, int index) {
                  WorkshiftAdminModel workshiftAdminModel =
                      workshiftList[index];
                  return WorkshiftList(
                    shiftName: workshiftAdminModel.shiftName,
                    endTime: workshiftAdminModel.endTime,
                    startTime: workshiftAdminModel.startTime,
                    pressDay: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ShiftDay(
                                    shiftName: workshiftAdminModel.shiftName,
                                    idShift: 
                                        workshiftAdminModel.workshiftId,
                                  )));
                    },
                    pressOption: () {
                      showModalBottomSheet(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        context: context,
                        builder: (builder) {
                          return Container(
                            height: 150.0,
                            color: Colors
                                .transparent, //could change this to Color(0xFF737373),
                            child: MenuWorkshift(
                              edit: () async {
                                String refresh = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => UpdateWorkshift(
                                              startTime:
                                                  workshiftAdminModel.startTime,
                                              endTime:
                                                  workshiftAdminModel.endTime,
                                              maxTime: workshiftAdminModel
                                                  .maxArrival,
                                              shiftName:
                                                  workshiftAdminModel.shiftName,
                                              idShift: 
                                                  workshiftAdminModel
                                                      .workshiftId,
                                            )));
                                if (refresh == 'refresh') {
                                  fungsiGetWorkshift();
                                }
                              },
                              hapus: () {
                                Navigator.pop(context);
                                Alert(
                                  context: context,
                                  type: AlertType.warning,
                                  desc:
                                      "Apakah anda ingin menghapus ${workshiftAdminModel.shiftName}",
                                  buttons: [
                                    DialogButton(
                                      color: redColor,
                                      onPressed: () {
                                        fungsiDeleteWorkshift(int.parse(
                                            workshiftAdminModel.workshiftId
                                                .toString()));
                                      },
                                      width: 120,
                                      child: const Text(
                                        "Hapus",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    )
                                  ],
                                ).show();
                              },
                            ),
                          );
                        },
                      );
                    },
                  );
                },
              ),
      ),
    );
  }
}
