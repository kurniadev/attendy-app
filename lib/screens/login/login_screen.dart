// ignore_for_file: unnecessary_null_comparison

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/admission_model.dart';
import 'package:attandy_app/models/auth_model.dart';
import 'package:attandy_app/screens/dashboard/trial_dashboard.dart';
import 'package:attandy_app/screens/splash_screen.dart';
import 'package:attandy_app/screens/tabs_screen.dart';
import 'package:attandy_app/screens/widget/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/admission_service.dart';
import '../../services/auth_service.dart';
import '../Notification/notification_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  AdmissionModel? admissionModel;

  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  AnimationController? localAnimationController;

  bool _secureText = true;
  bool isChecked = false;
  bool isLoading = false;
  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  check() {
    final form = _key.currentState!;
    if (form.validate()) {
      form.save();
      _loginUser();
    }
  }

  bool isEmail(String em) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  void _loginUser() async {
    setState(() {
      isLoading = true;
    });
    ApiResponse response =
        await login(email: email.text, password: password.text);
    if (response.error == null) {
      _saveAndRedirectToHome(response.data as AuthenticationModel);
    } else {
      setState(() {
        isLoading = false;
      });
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('Email atau Kata Sandi anda salah.',
              type: AnimatedSnackBarType.warning,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  String _debugLabelString = "";
  void _saveAndRedirectToHome(AuthenticationModel authenticationModel) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('token', authenticationModel.data.accessToken);
    await preferences.setString('role', authenticationModel.data.user.role);
    await preferences.setString('email', authenticationModel.data.user.email);
    // ignore: use_build_context_synchronously
    AnimatedSnackBar.material('Login Berhasil',
            type: AnimatedSnackBarType.success,
            mobileSnackBarPosition: MobileSnackBarPosition.top,
            desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
        .show(context);
    // ignore: use_build_context_synchronously
    setState(() {
      checkUser();
    });
    // Navigator.of(context).pushAndRemoveUntil(
    //     MaterialPageRoute(builder: (context) => const TabScreen()),
    //     (route) => false);
  }

  void checkUser() async {
    ApiResponse response = await getAdmission();
    if (response.error == null) {
      setState(() {
        admissionModel = response.data as AdmissionModel;
        setState(() {
          setPrefStatus();
          // setNotification();
        });
        if (admissionModel!.data.status == 'true') {
          setState(() {
            setPrefUser();
          });
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => const TabScreen()),
              (route) => false);
        } else if (admissionModel!.data.status == 'false') {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => const TrialDashboard()),
              (route) => false);
        }
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      // ignore: use_build_context_synchronously
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  setPrefStatus() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('status', admissionModel!.data.status);
  }

  setPrefUser() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('firstName', admissionModel!.data.firstName);
    await preferences.setString('lastName', admissionModel!.data.lastName);
    await preferences.setString('position', admissionModel!.data.position);
    await preferences.setString('imageUrl', admissionModel!.data.image);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Form(
              key: _key,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: SizedBox(
                      height: 300,
                      child: Lottie.asset(
                        "assets/lottie/login.json",
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                      letterSpacing: 1,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      children: [
                        SizedBox(
                          height: 30,
                          width: 30,
                          child: Image.asset(
                            'assets/Icons/email.png',
                            color: black2Color.withOpacity(0.8),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: SizedBox(
                            // height: 35,
                            child: TextFormField(
                              controller: email,
                              style: const TextStyle(
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.bold),
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                // border: InputBorder.none,
                                hintText: 'Masukkan email',
                                hintStyle: TextStyle(
                                    fontSize: 14.0,
                                    fontFamily: 'PoppinsReguler',
                                    color: black2Color.withOpacity(0.7),
                                    letterSpacing: 1),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      children: [
                        SizedBox(
                          height: 30,
                          width: 30,
                          child: Image.asset(
                            'assets/Icons/password.png',
                            color: black2Color.withOpacity(0.8),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: SizedBox(
                            // height: 35,
                            child: TextFormField(
                              controller: password,
                              obscureText: _secureText,
                              style: const TextStyle(
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.bold),
                              decoration: InputDecoration(
                                // border: InputBorder.none,
                                hintText: 'Masukkan Kata Sandi',
                                suffixIcon: IconButton(
                                  onPressed: showHide,
                                  icon: Icon(_secureText
                                      ? Icons.visibility_off
                                      : Icons.visibility),
                                  color: const Color(0xff4B556B),
                                ),
                                hintStyle: TextStyle(
                                    fontSize: 14.0,
                                    fontFamily: 'PoppinsReguler',
                                    color: black2Color.withOpacity(0.7),
                                    letterSpacing: 1),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 24),
                    child: GestureDetector(
                        onTap: () {
                          if (email.text == '') {
                            AnimatedSnackBar.material(
                                    'Email tidak boleh kosong',
                                    type: AnimatedSnackBarType.warning,
                                    mobileSnackBarPosition:
                                        MobileSnackBarPosition.top,
                                    desktopSnackBarPosition:
                                        DesktopSnackBarPosition.topCenter)
                                .show(context);
                          } else if (password.text == '') {
                            AnimatedSnackBar.material(
                                    'Kata Sandi tidak boleh kosong',
                                    type: AnimatedSnackBarType.warning,
                                    mobileSnackBarPosition:
                                        MobileSnackBarPosition.top,
                                    desktopSnackBarPosition:
                                        DesktopSnackBarPosition.topCenter)
                                .show(context);
                          } else {
                            check();
                          }
                        },
                        child: Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                color: isLoading == false
                                    ? blueColor
                                    : const Color.fromARGB(255, 28, 73, 163),
                                borderRadius: BorderRadius.circular(12)),
                            child: isLoading == false
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      Icon(
                                        Icons.login,
                                        color: Colors.white,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        'Masuk',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: 'Roboto',
                                            fontWeight: FontWeight.w600,
                                            color: Colors.white),
                                      ),
                                    ],
                                  )
                                : Center(
                                    child: Container(
                                      width: 24,
                                      height: 24,
                                      padding: const EdgeInsets.all(2.0),
                                      child: const CircularProgressIndicator(
                                        color: Colors.white,
                                        strokeWidth: 3,
                                      ),
                                    ),
                                  ))),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    textLogin,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 12,
                      color: black2Color,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.normal,
                      letterSpacing: 1,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
