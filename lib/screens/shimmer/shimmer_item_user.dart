import 'package:attandy_app/screens/widget/custom_shimmer.dart';
import 'package:flutter/material.dart';

class ShimmerItemUser extends StatefulWidget {
  const ShimmerItemUser({super.key});

  @override
  State<ShimmerItemUser> createState() => _ShimmerItemUserState();
}

class _ShimmerItemUserState extends State<ShimmerItemUser> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        ComponentItemUser(),
        ComponentItemUser(),
        ComponentItemUser(),
        ComponentItemUser(),
        ComponentItemUser(),
        ComponentItemUser(),
        ComponentItemUser(),
      ],
    );
  }
}

class ComponentItemUser extends StatefulWidget {
  const ComponentItemUser({super.key});

  @override
  State<ComponentItemUser> createState() => _ComponentItemUserState();
}

class _ComponentItemUserState extends State<ComponentItemUser> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Container(
        height: 70,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 5,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          children: [
            const Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: CustomShimmer(height: 50, width: 50, radius: 25)),
            Expanded(
              child: SizedBox(
                height: 60,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomShimmer(
                        height: 18,
                        width: MediaQuery.of(context).size.width * 0.6,
                        radius: 8),
                    const SizedBox(
                      height: 3,
                    ),
                    CustomShimmer(
                        height: 14,
                        width: MediaQuery.of(context).size.width * 0.4,
                        radius: 8),
                    const SizedBox(
                      height: 6,
                    ),
                    CustomShimmer(
                        height: 10,
                        width: MediaQuery.of(context).size.width * 0.2,
                        radius: 8)
                  ],
                ),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
          ],
        ),
      ),
    );
  }
}
