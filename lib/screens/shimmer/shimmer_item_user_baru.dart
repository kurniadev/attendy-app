import 'package:flutter/material.dart';

import '../widget/custom_shimmer.dart';

class ShimmerItemUserBaru extends StatefulWidget {
  const ShimmerItemUserBaru({super.key});

  @override
  State<ShimmerItemUserBaru> createState() => _ShimmerItemUserBaruState();
}

class _ShimmerItemUserBaruState extends State<ShimmerItemUserBaru> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ComponenUserBaru(),
        ComponenUserBaru(),
        ComponenUserBaru(),
        ComponenUserBaru(),
        ComponenUserBaru(),
        ComponenUserBaru(),
        ComponenUserBaru(),
        ComponenUserBaru(),
        ComponenUserBaru(),
      ],
    );
  }
}

class ComponenUserBaru extends StatefulWidget {
  const ComponenUserBaru({super.key});

  @override
  State<ComponenUserBaru> createState() => _ComponenUserBaruState();
}

class _ComponenUserBaruState extends State<ComponenUserBaru> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 5,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: SizedBox(
                  height: 60,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomShimmer(
                          height: 16,
                          width: MediaQuery.of(context).size.width * 0.6,
                          radius: 8),
                      const SizedBox(
                        height: 5,
                      ),
                      CustomShimmer(
                          height: 16,
                          width: MediaQuery.of(context).size.width * 0.4,
                          radius: 8),
                      const SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
          ],
        ),
      ),
    );
  }
}
