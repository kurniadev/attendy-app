import 'package:flutter/material.dart';

import '../../constants.dart';
import '../widget/custom_shimmer.dart';

class ShimmerWorkshiftProfile extends StatefulWidget {
  const ShimmerWorkshiftProfile({super.key});

  @override
  State<ShimmerWorkshiftProfile> createState() =>
      _ShimmerWorkshiftProfileState();
}

class _ShimmerWorkshiftProfileState extends State<ShimmerWorkshiftProfile> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 80,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text(
                  "Absensi Masuk",
                  style: TextStyle(
                    color: black2Color,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w700,
                    fontSize: 14,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                CustomShimmer(height: 15, width: 100, radius: 8),
              ],
            ),
          ),
        ),
        const SizedBox(
          width: 15,
        ),
        Expanded(
          child: Container(
            height: 80,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text(
                  "Absensi Keluar",
                  style: TextStyle(
                    color: black2Color,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w700,
                    fontSize: 14,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                CustomShimmer(height: 15, width: 100, radius: 8),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
