import 'package:attandy_app/screens/widget/custom_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

class SimmerPresensiBar extends StatefulWidget {
  const SimmerPresensiBar({super.key});

  @override
  State<SimmerPresensiBar> createState() => _SimmerPresensiBarState();
}

class _SimmerPresensiBarState extends State<SimmerPresensiBar> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                // border: Border.all(color: greyColor),
                color: Colors.white),
            height: 80,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: [
                  Expanded(
                    child: SizedBox(
                      height: 60,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CustomShimmer(
                              height: 18,
                              width: MediaQuery.of(context).size.width * 0.35,
                              radius: 8),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomShimmer(
                              height: 16,
                              width: MediaQuery.of(context).size.width * 0.2,
                              radius: 8),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  const CustomShimmer(height: 40, width: 160, radius: 12)
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
