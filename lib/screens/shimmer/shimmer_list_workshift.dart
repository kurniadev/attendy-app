import 'package:attandy_app/screens/widget/custom_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../constants.dart';

class ShimmerListWorkshift extends StatelessWidget {
  const ShimmerListWorkshift({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        ComponentWorkshift(),
        ComponentWorkshift(),
        ComponentWorkshift(),
        ComponentWorkshift(),
        ComponentWorkshift(),
        ComponentWorkshift(),
        ComponentWorkshift(),
      ],
    );
  }
}

class ComponentWorkshift extends StatelessWidget {
  const ComponentWorkshift({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      child: Container(
        height: 80,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(color: shimmer1, width: 2),
        ),
        child: Row(
          children: [
            Shimmer(
              gradient: const LinearGradient(
                stops: [0.2, 0.5, 0.6],
                colors: [shimmer1, shimmer2, shimmer3],
              ),
              child: Container(
                height: 80,
                width: 70,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    topLeft: Radius.circular(10),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomShimmer(
                      height: 16,
                      width: MediaQuery.of(context).size.width * 0.5,
                      radius: 6),
                  const SizedBox(
                    height: 5,
                  ),
                  CustomShimmer(
                      height: 10,
                      width: MediaQuery.of(context).size.width * 0.4,
                      radius: 6),
                  Expanded(
                    child: Row(
                      children: const [
                        CustomShimmer(height: 12, width: 60, radius: 6),
                        SizedBox(
                          width: 10,
                        ),
                        CustomShimmer(height: 12, width: 60, radius: 6),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
