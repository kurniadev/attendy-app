import 'package:flutter/material.dart';

import '../widget/custom_shimmer.dart';

class ShimmerCountPresensi extends StatefulWidget {
  const ShimmerCountPresensi({super.key});

  @override
  State<ShimmerCountPresensi> createState() => _ShimmerCountPresensiState();
}

class _ShimmerCountPresensiState extends State<ShimmerCountPresensi> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: 110,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const CustomShimmer(
            height: 70,
            radius: 110,
            width: 70,
          ),
          const SizedBox(
            height: 10,
          ),
          CustomShimmer(
              height: 16,
              width: MediaQuery.of(context).size.width * 0.2,
              radius: 8),
        ],
      ),
    );
  }
}
