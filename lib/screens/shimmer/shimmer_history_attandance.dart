import 'package:attandy_app/screens/widget/custom_shimmer.dart';
import 'package:flutter/material.dart';

class ShimmerHistoryAttandance extends StatefulWidget {
  const ShimmerHistoryAttandance({super.key});

  @override
  State<ShimmerHistoryAttandance> createState() =>
      _ShimmerHistoryAttandanceState();
}

class _ShimmerHistoryAttandanceState extends State<ShimmerHistoryAttandance> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        ComponentHistoryAttandance(),
        ComponentHistoryAttandance(),
        ComponentHistoryAttandance(),
      ],
    );
  }
}

class ComponentHistoryAttandance extends StatefulWidget {
  const ComponentHistoryAttandance({super.key});

  @override
  State<ComponentHistoryAttandance> createState() =>
      ComponentHistoryAttandanceState();
}

class ComponentHistoryAttandanceState
    extends State<ComponentHistoryAttandance> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10,
        ),
        CustomShimmer(
            height: 20,
            width: MediaQuery.of(context).size.width * 0.3,
            radius: 8),
        const SizedBox(
          height: 10,
        ),
        CustomShimmer(
            height: 120, width: MediaQuery.of(context).size.width, radius: 12),
        const SizedBox(
          height: 15,
        ),
        CustomShimmer(
            height: 120, width: MediaQuery.of(context).size.width, radius: 12),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
