import 'package:flutter/material.dart';

import '../../../constants.dart';

class DetailTutorial extends StatefulWidget {
  const DetailTutorial({super.key, required this.title, required this.desc});

  final String title, desc;

  @override
  State<DetailTutorial> createState() => _DetailTutorialState();
}

class _DetailTutorialState extends State<DetailTutorial> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(
          "Detail Tutorial",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.title,
              style: const TextStyle(
                  fontSize: 14,
                  fontFamily: "Roboto",
                  fontWeight: FontWeight.bold,
                  color: black2Color,
                  letterSpacing: 0.5),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              widget.desc,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 14,
                fontFamily: "Roboto",
                fontWeight: FontWeight.normal,
                color: black2Color,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
