import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../constants.dart';
import '../../widget/custom_button.dart';

class AddOrUpdate extends StatefulWidget {
  const AddOrUpdate(
      {super.key,
      required this.controller1,
      required this.controller2,
      required this.ontaps,
      required this.title, required this.buttonlTitle});

  final TextEditingController controller1, controller2;
  final VoidCallback ontaps;
  final String title,buttonlTitle;

  @override
  State<AddOrUpdate> createState() => _AddOrUpdateState();
}

class _AddOrUpdateState extends State<AddOrUpdate> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(
            height: 5,
          ),
          Text(
            widget.title,
            style: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
              fontFamily: 'Poppins',
              color: Color(0xff4B556B),
              letterSpacing: 1,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey,
                ),
                borderRadius: BorderRadius.circular(12)),
            // height: 35,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: widget.controller1,
                style: const TextStyle(
                    fontFamily: 'Roboto', fontWeight: FontWeight.normal),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Masukkan judul',
                  hintStyle: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'PoppinsReguler',
                      color: black2Color.withOpacity(0.7),
                      letterSpacing: 1),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey,
                ),
                borderRadius: BorderRadius.circular(12)),
            // height: 35,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: widget.controller2,
                maxLines: 5,
                style: const TextStyle(
                    fontFamily: 'Roboto', fontWeight: FontWeight.normal),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Masukkan deskripsi',
                  hintStyle: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'PoppinsReguler',
                      color: black2Color.withOpacity(0.7),
                      letterSpacing: 1),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          CustomButton(press: widget.ontaps, title: widget.buttonlTitle)
        ],
      ),
    );
  }
}
