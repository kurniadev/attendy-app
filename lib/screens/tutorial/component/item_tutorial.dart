import 'package:flutter/material.dart';

import '../../../constants.dart';

class ItemTutorial extends StatefulWidget {
  const ItemTutorial(
      {super.key,
      required this.title,
      required this.desc,
      required this.press,
      required this.icons});

  final String title, desc, icons;
  final VoidCallback press;

  @override
  State<ItemTutorial> createState() => _ItemTutorialState();
}

class _ItemTutorialState extends State<ItemTutorial> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.press,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.title,
                      style: const TextStyle(
                          fontSize: 14,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.bold,
                          color: black2Color,
                          letterSpacing: 0.5),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      widget.desc,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontSize: 14,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.normal,
                        color: black2Color,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
                width: 30,
                child: Image.asset(
                  widget.icons,
                  color: Colors.black.withOpacity(0.5),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 15,
          ),
          Container(
            height: 1,
            width: MediaQuery.of(context).size.width,
            color: Colors.grey.withOpacity(0.2),
          ),
          const SizedBox(
            height: 15,
          ),
        ],
      ),
    );
  }
}
