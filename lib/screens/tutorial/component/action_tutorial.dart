import 'package:flutter/material.dart';

import '../../../constants.dart';

class ActionTutorial extends StatefulWidget {
  const ActionTutorial({super.key, required this.edit, required this.hapus});
  final VoidCallback edit, hapus;

  @override
  State<ActionTutorial> createState() => _ActionTutorialState();
}

class _ActionTutorialState extends State<ActionTutorial> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0))),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Container(
                height: 5,
                width: 50,
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.35),
                    borderRadius: BorderRadius.circular(12)),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            GestureDetector(
              onTap: widget.edit,
              child: SizedBox(
                height: 40,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: const [
                    Icon(
                      Icons.edit,
                      size: 20,
                      color: black2Color,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Edit Tutorial",
                      style: TextStyle(
                        fontSize: 16,
                        color: black2Color,
                        fontFamily: 'PoppinsReguler',
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: widget.hapus,
              child: SizedBox(
                height: 40,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: const [
                    Icon(
                      Icons.delete,
                      size: 20,
                      color: redColor,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Hapus Tutorial",
                      style: TextStyle(
                        fontSize: 16,
                        color: redColor,
                        fontFamily: 'PoppinsReguler',
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
