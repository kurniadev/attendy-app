// ignore_for_file: unnecessary_brace_in_string_interps, use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/constants.dart';
import 'package:attandy_app/models/tutorial_model.dart';
import 'package:attandy_app/services/tutorial_service.dart';
import 'package:attandy_app/screens/tutorial/component/add-or-update.dart';
import 'package:attandy_app/screens/tutorial/component/detail_tutorial.dart';
import 'package:attandy_app/screens/tutorial/component/item_tutorial.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../../models/api_response_model.dart';
import '../splash_screen.dart';
import '../widget/custom_loading.dart';
import '../../services/auth_service.dart';
import 'component/action_tutorial.dart';

class TutorialScreen extends StatefulWidget {
  const TutorialScreen({super.key, required this.isAdmin});

  final String isAdmin;

  @override
  State<TutorialScreen> createState() => _TutorialScreenState();
}

class _TutorialScreenState extends State<TutorialScreen> {
  TextEditingController txtTitle = TextEditingController();
  TextEditingController txtDesc = TextEditingController();

  bool _loading = true;

  void fungsiCreateTutorial() async {
    showAlertDialog(context);
    ApiResponse response =
        await cretaeTutorial(title: txtTitle.text, description: txtDesc.text);
    if (response.error == null) {
      Navigator.pop(context);
      AnimatedSnackBar.material('Berhasil Menambahkan Tutorial',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      Navigator.pop(context);
      setState(() {
        fungsiGetTutorial();
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('Ada Kesalahan',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  List<dynamic> tutorialList = [];

  Future<void> fungsiGetTutorial() async {
    ApiResponse response = await getTutorial();
    if (response.error == null) {
      setState(() {
        tutorialList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void fungsiDeleteTutorial(int tutId) async {
    showAlertDialog(context);
    ApiResponse response = await deleteTutorial(tutorialId: tutId);
    if (response.error == null) {
      Navigator.pop(context);

      setState(() {
        fungsiGetTutorial();
      });
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Menghapus Tutorial')));
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  void fungsiUpdateTutorial(int tutId) async {
    showAlertDialog(context);
    ApiResponse response = await updateTutorial(
      title: txtTitle.text,
      description: txtDesc.text,
      tutorialId: tutId,
    );
    if (response.error == null) {
      setState(() {
        _loading = false;
        fungsiGetTutorial();
      });
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Berhasil Perbarui Tutorial')));
      Navigator.pop(context);
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  @override
  void initState() {
    fungsiGetTutorial();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text(
            "Tutorial",
            style: TextStyle(
              fontSize: 18,
              color: black2Color,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.w700,
              letterSpacing: 1,
            ),
          ),
          backgroundColor: Colors.white,
          iconTheme: const IconThemeData(
            color: black2Color,
          ),
          actions: [
            widget.isAdmin == 'true'
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 15, horizontal: 10),
                    child: GestureDetector(
                      onTap: () async {
                        setState(() {
                          txtTitle.clear();
                          txtDesc.clear();
                        });
                        showModalBottomSheet(
                          shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.vertical(
                                  top: Radius.circular(25.0))),
                          backgroundColor: Colors.white,
                          context: context,
                          isScrollControlled: true,
                          builder: (context) => Padding(
                            padding: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).viewInsets.bottom),
                            child: AddOrUpdate(
                              buttonlTitle: 'Tambah',
                              controller1: txtTitle,
                              controller2: txtDesc,
                              title: 'Tambah Tutorial',
                              ontaps: () {
                                fungsiCreateTutorial();
                              },
                            ),
                          ),
                        );
                      },
                      child: Container(
                        height: 20,
                        width: 35,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: blueColor),
                        child: const Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )
                : const SizedBox()
          ],
          elevation: 0,
        ),
        body: _loading
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: SizedBox(
                      height: 100,
                      width: MediaQuery.of(context).size.width,
                      child: Lottie.asset(
                        "assets/lottie/loading.json",
                      ),
                    ),
                  ),
                ],
              )
            : ListView.builder(
                itemCount: tutorialList.length,
                itemBuilder: (BuildContext context, int index) {
                  TutorialModel tutorialModel = tutorialList[index];
                  return Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: ItemTutorial(
                      title: tutorialModel.title,
                      desc: tutorialModel.description,
                      press: () {
                        widget.isAdmin == 'true'
                            ? showModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                context: context,
                                builder: (builder) {
                                  return Container(
                                    height: 150.0,
                                    color: Colors
                                        .transparent, //could change this to Color(0xFF737373),
                                    child: ActionTutorial(
                                      edit: () async {
                                        Navigator.pop(context);
                                        setState(() {
                                          txtTitle.text = tutorialModel.title;
                                          txtDesc.text =
                                              tutorialModel.description;
                                        });
                                        showModalBottomSheet(
                                          shape: const RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.vertical(
                                                      top: Radius.circular(
                                                          25.0))),
                                          backgroundColor: Colors.white,
                                          context: context,
                                          isScrollControlled: true,
                                          builder: (context) => Padding(
                                            padding: EdgeInsets.only(
                                                bottom: MediaQuery.of(context)
                                                    .viewInsets
                                                    .bottom),
                                            child: AddOrUpdate(
                                              buttonlTitle: 'Perbarui',
                                              controller1: txtTitle,
                                              controller2: txtDesc,
                                              title: 'Edit Tutorial',
                                              ontaps: () {
                                                fungsiUpdateTutorial(
                                                    tutorialModel.id);
                                              },
                                            ),
                                          ),
                                        );
                                      },
                                      hapus: () {
                                        Navigator.pop(context);
                                        Alert(
                                          context: context,
                                          type: AlertType.warning,
                                          desc:
                                              // "Apakah anda ingin menghapus ${workshiftAdminModel.shiftName}",
                                              "Apakah anda ingin menghapus ",
                                          buttons: [
                                            DialogButton(
                                              color: redColor,
                                              onPressed: () {
                                                fungsiDeleteTutorial(
                                                    tutorialModel.id);
                                              },
                                              width: 120,
                                              child: const Text(
                                                "Hapus",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 20),
                                              ),
                                            )
                                          ],
                                        ).show();
                                      },
                                    ),
                                  );
                                },
                              )
                            : Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DetailTutorial(
                                          title: tutorialModel.title,
                                          desc: tutorialModel.description,
                                        )));
                      },
                      icons: widget.isAdmin == 'true'
                          ? 'assets/Icons/three-dots.png'
                          : 'assets/Icons/next.png',
                    ),
                  );
                }));
  }
}
