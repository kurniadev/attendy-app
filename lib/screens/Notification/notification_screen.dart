// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/notification_modal.dart';
import 'package:attandy_app/screens/application/dinas_admin.dart';
import 'package:attandy_app/screens/application/time_off_admin.dart';
import 'package:attandy_app/screens/users/users_screen.dart';
import 'package:attandy_app/screens/utils/time_ago.dart';
import 'package:attandy_app/services/notification_service.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../widget/custom_loading.dart';
import 'component/item_notification.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({super.key, required this.role});

  final String role;

  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  bool _loading = true;
  String? countNotif;
  List<dynamic> notifList = [];

  bool loading = true;

  Future<void> fungsiGetNotification() async {
    ApiResponse response = await getNotification();
    if (response.error == null) {
      setState(() {
        notifList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  CountNotificationModel? countNotificationModel;
  void fungsiGetCount() async {
    ApiResponse response = await getCountNotif();
    if (response.error == null) {
      setState(() {
        print('jalan skuy');
        countNotificationModel = response.data as CountNotificationModel;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void fungsiUpdateNotif(int idNotif) async {
    showAlertDialog(context);
    ApiResponse response = await updateNotification(notifId: idNotif);
    if (response.error == null) {
      setState(() {
        loading = false;
        fungsiGetNotification();
        fungsiGetCount();
        Navigator.pop(context);
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  @override
  void initState() {
    fungsiGetCount();
    fungsiGetNotification();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: Row(
          children: [
            const Text(
              "Notifikasi",
              style: TextStyle(
                fontSize: 18,
                color: black2Color,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w700,
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            Container(
              height: 17,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: redColor.withOpacity(0.8)),
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    countNotificationModel == null
                        ? "Tunggu Sebentar"
                        : countNotificationModel!.count == 0
                            ? "Belum ada pesan baru"
                            : "${countNotificationModel!.count.toString()} Pesan belum dibaca",
                    style: const TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: _loading
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 225,
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                    child: SizedBox(
                      height: 100,
                      width: MediaQuery.of(context).size.width,
                      child: Lottie.asset(
                        "assets/lottie/loading.json",
                      ),
                    ),
                  ),
                )
              ],
            )
          : ListView.builder(
              itemCount: notifList.length,
              itemBuilder: (BuildContext context, int index) {
                NotificationModel notificationModel = notifList[index];
                return ItemNotification2(
                  title: notificationModel.title,
                  message: notificationModel.message,
                  imageUrl: notificationModel.image,
                  datetime:
                      time_passed(notificationModel.createdAt, full: false),
                  isread: notificationModel.isRead == 'Belum' ? 0.1 : 0.0,
                  ontaps: () {
                    if (notificationModel.type == 'Izin') {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const TimeOffAdmin()));
                    } else if (notificationModel.type == 'Dinas') {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const DinasAdmin()));
                    } else if (notificationModel.type == 'Registrasi') {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => UsersScreen(
                                    role: widget.role,
                                  )));
                    }
                    fungsiUpdateNotif(notificationModel.id);
                  },
                );
              },
            ),
    );
  }
}
