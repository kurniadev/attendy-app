import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../widget/custom_shimmer.dart';

class ItemNotification1 extends StatefulWidget {
  const ItemNotification1({super.key});

  @override
  State<ItemNotification1> createState() => _ItemNotification1State();
}

class _ItemNotification1State extends State<ItemNotification1> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: blueColor.withOpacity(0.1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: blueColor,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Image.asset(
                        'assets/Icons/attandance.png',
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          text: TextSpan(
                              text: 'Reza Kurnia Setiawan ',
                              style: TextStyle(
                                  fontSize: 13,
                                  fontFamily: "Roboto",
                                  fontWeight: FontWeight.bold,
                                  color: black2Color,
                                  letterSpacing: 0.5),
                              children: [
                                TextSpan(
                                  text: textDummy,
                                  style: TextStyle(
                                    fontSize: 13,
                                    fontFamily: "Roboto",
                                    fontWeight: FontWeight.normal,
                                    color: black2Color,
                                  ),
                                )
                              ]),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          '2 Menit yang lalu',
                          style: TextStyle(
                            fontSize: 11,
                            fontFamily: "Roboto",
                            fontWeight: FontWeight.normal,
                            color: black2Color.withOpacity(0.7),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ItemNotification2 extends StatefulWidget {
  const ItemNotification2(
      {super.key,
      required this.title,
      required this.message,
      required this.imageUrl,
      required this.datetime,
      required this.isread, required this.ontaps});

  final String title, message, imageUrl, datetime;
  final double isread;
  final VoidCallback ontaps;

  @override
  State<ItemNotification2> createState() => _ItemNotification2State();
}

class _ItemNotification2State extends State<ItemNotification2> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: GestureDetector(
        onTap: widget.ontaps,
        child: Container(
          width: MediaQuery.of(context).size.width,
          color: blueColor.withOpacity(widget.isread),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.3),
                              spreadRadius: 2,
                              blurRadius: 4,
                              offset: const Offset(
                                  0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: CachedNetworkImage(
                          imageUrl: widget.imageUrl,
                          fit: BoxFit.cover,
                          imageBuilder: (context, imageProvider) => Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  image: imageProvider, fit: BoxFit.cover),
                            ),
                          ),
                          placeholder: (context, url) => const CustomShimmer(
                              height: 40, width: 40, radius: 10),
                          errorWidget: (context, url, error) =>
                              const Icon(Icons.error),
                        )),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            text: TextSpan(
                                text: '${widget.title} ',
                                style: const TextStyle(
                                    fontSize: 13,
                                    fontFamily: "Roboto",
                                    fontWeight: FontWeight.bold,
                                    color: black2Color,
                                    letterSpacing: 0.5),
                                children: [
                                  TextSpan(
                                    text: widget.message,
                                    style: const TextStyle(
                                      fontSize: 13,
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.normal,
                                      color: black2Color,
                                    ),
                                  )
                                ]),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            widget.datetime,
                            style: TextStyle(
                              fontSize: 11,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.normal,
                              color: black2Color.withOpacity(0.7),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
