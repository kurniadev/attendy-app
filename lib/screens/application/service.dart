// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/screens/application/component/input_service.dart';
import 'package:attandy_app/screens/widget/custom_button.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

import '../../constants.dart';
import '../../services/auth_service.dart';
import '../widget/custom_loading.dart';

class ServiceScreen extends StatefulWidget {
  const ServiceScreen({super.key});

  @override
  State<ServiceScreen> createState() => _ServiceScreenState();
}

class _ServiceScreenState extends State<ServiceScreen> {
  TextEditingController purpose = TextEditingController();
  TextEditingController place = TextEditingController();
  TextEditingController abandonedJob = TextEditingController();
  TextEditingController dutyFromDate = TextEditingController();
  TextEditingController dutyToDate = TextEditingController();
  TextEditingController time = TextEditingController();
  String? _nameFile;
  bool _loading = false;

  List<PlatformFile>? _files;

  void fungsiPermohonanTugasDinas() async {
    showAlertDialog(context);
    String token = await getToken();
    Map<String, String> headers = {
      'X-Requested-With': 'XMLHttpRequest',
      'Authorization': 'Bearer $token'
    };
    var uri = Uri.parse('$baseURL/attendances/addduty');
    var request = http.MultipartRequest('POST', uri);
    request.files.add(await http.MultipartFile.fromPath(
        'attachment', _files!.first.path.toString()));

    request.headers.addAll(headers);
    request.fields['duty_from_date'] = dutyFromDate.text;
    request.fields['duty_to_date'] = dutyToDate.text;
    request.fields['time'] = time.text;
    request.fields['place'] = place.text;
    request.fields['abandoned_job'] = abandonedJob.text;
    request.fields['purpose'] = purpose.text;
    var response = await request.send();
    if (response.statusCode == 200) {
      setState(() {
        Navigator.pop(context);
        Navigator.pop(context, 'refresh');
      });
      AnimatedSnackBar.material('Berhasil Mengajukan Permohonan Tugas Dinas',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    } else if (response.statusCode == 400) {
      print(response.request);
    } else if (response.statusCode == 500) {
      print(response);
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Ada Kesalahan')));
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Ada Kesalahan')));
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  void ambilFile() async {
    _files = (await FilePicker.platform.pickFiles(
            type: FileType.any, allowMultiple: false, allowedExtensions: null))!
        .files;
    setState(() {
      _nameFile = _files!.first.name;
      print(_nameFile);
    });

    print('Loaded file path is : ${_files!.first.path}');
  }

  @override
  void initState() {
    print(_nameFile);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Permohonan Tugas Dinas",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.5),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        InputService1(
                          controller: purpose,
                          icons: 'assets/Icons/target.png',
                          inputTitle: 'Masukkan tujuan',
                          title: 'Tujuan',
                        ),
                        InputService1(
                          controller: place,
                          icons: 'assets/Icons/destination.png',
                          inputTitle: 'Masukkan tempat',
                          title: 'Tempat',
                        ),
                        InputService1(
                          controller: abandonedJob,
                          icons: 'assets/Icons/job.png',
                          inputTitle: 'Masukkan pekerjaan yang ditinggalkan',
                          title: 'Pekerjaan Yang Ditinggalkan',
                        ),
                        InputService3(
                          controller: dutyFromDate,
                          icons: 'assets/Icons/clock.png',
                          inputTitle: 'Pilih tanggal',
                          title: 'Tanggal Awal',
                          ontaps: () async {
                            DateTime? pickedDate = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(2000),
                                lastDate: DateTime(2101));

                            if (pickedDate != null) {
                              String formattedDate =
                                  DateFormat('yyyy-MM-dd').format(pickedDate);

                              setState(() {
                                dutyFromDate.text = formattedDate;
                              });
                            } else {
                              print("Date is not selected");
                            }
                          },
                        ),
                        InputService3(
                          controller: dutyToDate,
                          icons: 'assets/Icons/clock.png',
                          inputTitle: 'Pilih tanggal',
                          title: 'Tanggal Akhir',
                          ontaps: () async {
                            DateTime? pickedDate = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(2000),
                                lastDate: DateTime(2101));

                            if (pickedDate != null) {
                              String formattedDate =
                                  DateFormat('yyyy-MM-dd').format(pickedDate);

                              setState(() {
                                dutyToDate.text = formattedDate;
                              });
                            } else {
                              print("Date is not selected");
                            }
                          },
                        ),
                        InputService3(
                          controller: time,
                          icons: 'assets/Icons/calendar.png',
                          inputTitle: 'Pilih waktu',
                          title: 'Waktu',
                          ontaps: () async {
                            TimeOfDay? timepicked = await showTimePicker(
                              context: context,
                              initialTime: TimeOfDay.now(),
                              builder: (context, child) {
                                return Theme(
                                  data: ThemeData.light().copyWith(
                                    colorScheme: const ColorScheme.light(
                                      // change the border color
                                      primary: blueColor,
                                      // change the text color
                                      onSurface: black2Color,
                                    ),
                                    // button colors
                                    buttonTheme: const ButtonThemeData(
                                      colorScheme: ColorScheme.light(
                                        primary: Colors.green,
                                      ),
                                    ),
                                  ),
                                  child: child!,
                                );
                              },
                            );
                            if (timepicked != null) {
                              setState(() {
                                time.text =
                                    '${timepicked.hour.toString().padLeft(2, '0')}:${timepicked.minute.toString().padLeft(2, '0')}:00';
                                print(time.text);
                              });
                            } else {
                              print("Time is not selected");
                            }
                          },
                        ),
                        InputService2(
                          icons: 'assets/Icons/google-docs.png',
                          inputTitle: _nameFile == null
                              ? 'Masukkan dokumen'
                              : _nameFile.toString(),
                          title: 'Dokumen',
                          ontaps: () {
                            ambilFile();
                          },
                        ),
                        const Text(
                            'Ekstensi yang diperbolehkan hanya format pdf'),
                        CustomButton(
                            press: () async {
                              setState(() {
                                fungsiPermohonanTugasDinas();
                              });
                            },
                            title: 'Simpan')
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
