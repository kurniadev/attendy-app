// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/service_dutie_model.dart';
import 'package:attandy_app/screens/application/component/view_pdf.dart';
import 'package:attandy_app/screens/application/service.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../../services/servicedutie_service.dart';
import '../splash_screen.dart';
import 'application_screen.dart';
import 'component/status_dinas.dart';
import 'component/status_time_off.dart';

class DinasScreen extends StatefulWidget {
  const DinasScreen({super.key});

  @override
  State<DinasScreen> createState() => _DinasScreenState();
}

class _DinasScreenState extends State<DinasScreen> {
  bool pengajuan = false;
  bool riwayat = false;

  String _formatDate(DateTime dateTime) {
    return DateFormat('d MMMM yyyy', "id_ID").format(dateTime);
  }

  bool _loading = true;
  List<dynamic> serviceDutieList = [];

  Future<void> fungsiGetServiceDutieAuth() async {
    ApiResponse response = await getServiceDutieAuth();
    if (response.error == null) {
      setState(() {
        serviceDutieList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  @override
  void initState() {
    pengajuan = true;
    fungsiGetServiceDutieAuth();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Tugas Dinas",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: Column(
        children: [
          Row(
            children: [
              pengajuan == true
                  ? TrueTab(
                      title: 'Pengajuan',
                      ontaps: () {},
                    )
                  : FalseTab(
                      title: 'Pengajuan',
                      ontaps: () {
                        setState(() {
                          pengajuan = true;
                          riwayat = false;
                        });
                      },
                    ),
              riwayat == true
                  ? TrueTab(
                      title: 'Riwayat',
                      ontaps: () {},
                    )
                  : FalseTab(
                      title: 'Riwayat',
                      ontaps: () {
                        setState(() {
                          pengajuan = false;
                          riwayat = true;
                        });
                      },
                    ),
            ],
          ),
          pengajuan == true
              ? Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: Colors.white),
                    child: const Padding(
                      padding: EdgeInsets.all(12.0),
                      child: Text(
                        'Daftar Pengajuan Di Proses',
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "Roboto",
                            fontWeight: FontWeight.bold,
                            color: black2Color,
                            letterSpacing: 0.5),
                      ),
                    ),
                  ),
                )
              : const SizedBox(),
          riwayat == true
              ? Expanded(
                  child: ListView.builder(
                    itemCount: serviceDutieList.length,
                    itemBuilder: (BuildContext context, int index) {
                      ServiceDutieModel serviceDutieModel =
                          serviceDutieList[index];
                      String dateFrom = DateFormat('dd')
                          .format(serviceDutieModel.dutyFromDate);
                      String times =
                          DateFormat('HH:mm:ss').format(serviceDutieModel.time);
                      return serviceDutieModel.status != 'Pending'
                          ? Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: GestureDetector(
                                  onTap: () {
                                    showModalBottomSheet(
                                      shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.vertical(
                                              top: Radius.circular(15.0))),
                                      backgroundColor: Colors.white,
                                      context: context,
                                      isScrollControlled: true,
                                      builder: (
                                        BuildContext context,
                                      ) =>
                                          StatusTugas(
                                        name: serviceDutieModel.purpose,
                                        job: serviceDutieModel.abandonedJob,
                                        date:
                                            '$dateFrom - ${_formatDate(serviceDutieModel.dutyToDate)}',
                                        bukti: serviceDutieModel.attachment,
                                        ontaps: () {},
                                        status: serviceDutieModel.status ==
                                                'Pending'
                                            ? 'Diproses'
                                            : serviceDutieModel.status ==
                                                    'Cencel'
                                                ? 'Ditolak'
                                                : 'Disetujui',
                                        colors: serviceDutieModel.status ==
                                                'Pending'
                                            ? orangeColor
                                            : serviceDutieModel.status ==
                                                    'Cencel'
                                                ? redColor
                                                : greenColor,
                                        title: 'Dokumen',
                                        tempat: serviceDutieModel.place,
                                        tujuan: serviceDutieModel.purpose,
                                        waktu: times,
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        border: Border.all(color: greyColor),
                                        color: Colors.white),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            serviceDutieModel.purpose,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontFamily: "Roboto",
                                              fontWeight: FontWeight.bold,
                                              color: black2Color,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                '$dateFrom - ${_formatDate(serviceDutieModel.dutyToDate)}',
                                                style: const TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: "Roboto",
                                                  fontWeight: FontWeight.w700,
                                                  color: orangeColor,
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 5),
                                                height: 5,
                                                width: 5,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    color: black2Color
                                                        .withOpacity(0.8)),
                                              ),
                                              Row(
                                                children: [
                                                  const Text(
                                                    'Status : ',
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontFamily: "Roboto",
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        color: black2Color,
                                                        letterSpacing: 0.5),
                                                  ),
                                                  Text(
                                                    serviceDutieModel.status,
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontFamily: "Roboto",
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: serviceDutieModel
                                                                    .status ==
                                                                'Pending'
                                                            ? orangeColor
                                                            : serviceDutieModel
                                                                        .status ==
                                                                    'Cencel'
                                                                ? redColor
                                                                : greenColor,
                                                        letterSpacing: 0.5),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 5,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : const SizedBox();
                      // Padding(
                      //   padding: const EdgeInsets.only(
                      //       bottom: 12, right: 12, left: 12),
                      //   child: Container(
                      //     width: MediaQuery.of(context).size.width,
                      //     decoration: const BoxDecoration(
                      //         borderRadius: BorderRadius.only(
                      //             bottomLeft: Radius.circular(12),
                      //             bottomRight: Radius.circular(12)),
                      //         color: Colors.red),
                      //     child: Padding(
                      //       padding: const EdgeInsets.only(
                      //           bottom: 12, right: 12, left: 12),
                      //       child: Column(
                      //         crossAxisAlignment: CrossAxisAlignment.start,
                      //         children: [

                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      // );
                    },
                  ),
                )
              : Expanded(
                  child: ListView.builder(
                    itemCount: serviceDutieList.length,
                    itemBuilder: (BuildContext context, int index) {
                      ServiceDutieModel serviceDutieModel =
                          serviceDutieList[index];
                      String dateFrom = DateFormat('dd')
                          .format(serviceDutieModel.dutyFromDate);
                      String times =
                          DateFormat('HH:mm:ss').format(serviceDutieModel.time);
                      return serviceDutieModel.status == 'Pending'
                          ? Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: GestureDetector(
                                  onTap: () {
                                    showModalBottomSheet(
                                      shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.vertical(
                                              top: Radius.circular(15.0))),
                                      backgroundColor: Colors.white,
                                      context: context,
                                      isScrollControlled: true,
                                      builder: (context) => Padding(
                                        padding: EdgeInsets.only(
                                            bottom: MediaQuery.of(context)
                                                .viewInsets
                                                .bottom),
                                        child: StatusTugas(
                                          name: serviceDutieModel.purpose,
                                          job: serviceDutieModel.abandonedJob,
                                          date:
                                              '$dateFrom - ${_formatDate(serviceDutieModel.dutyToDate)}',
                                          bukti: serviceDutieModel.attachment,
                                          ontaps: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ViewPdf(
                                                          urlDoc:
                                                              serviceDutieModel
                                                                  .attachment,
                                                        )));
                                          },
                                          status: serviceDutieModel.status ==
                                                  'Pending'
                                              ? 'Diproses'
                                              : serviceDutieModel.status ==
                                                      'Cencel'
                                                  ? 'Ditolak'
                                                  : 'Disetujui',
                                          colors: serviceDutieModel.status ==
                                                  'Pending'
                                              ? orangeColor
                                              : serviceDutieModel.status ==
                                                      'Cencel'
                                                  ? redColor
                                                  : greenColor,
                                          title: 'Dokumen',
                                          tempat: serviceDutieModel.place,
                                          tujuan: serviceDutieModel.purpose,
                                          waktu: times,
                                        ),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        border: Border.all(color: greyColor),
                                        color: Colors.white),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            serviceDutieModel.purpose,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontFamily: "Roboto",
                                              fontWeight: FontWeight.bold,
                                              color: black2Color,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                '$dateFrom - ${_formatDate(serviceDutieModel.dutyToDate)}',
                                                style: const TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: "Roboto",
                                                  fontWeight: FontWeight.w700,
                                                  color: orangeColor,
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 5),
                                                height: 5,
                                                width: 5,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    color: black2Color
                                                        .withOpacity(0.8)),
                                              ),
                                              Row(
                                                children: [
                                                  const Text(
                                                    'Status : ',
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontFamily: "Roboto",
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        color: black2Color,
                                                        letterSpacing: 0.5),
                                                  ),
                                                  Text(
                                                    serviceDutieModel.status,
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontFamily: "Roboto",
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: serviceDutieModel
                                                                    .status ==
                                                                'Pending'
                                                            ? orangeColor
                                                            : serviceDutieModel
                                                                        .status ==
                                                                    'Cencel'
                                                                ? redColor
                                                                : greenColor,
                                                        letterSpacing: 0.5),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 5,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : const SizedBox();
                      // Padding(
                      //   padding: const EdgeInsets.only(
                      //       bottom: 12, right: 12, left: 12),
                      //   child: Container(
                      //     width: MediaQuery.of(context).size.width,
                      //     decoration: const BoxDecoration(
                      //         borderRadius: BorderRadius.only(
                      //             bottomLeft: Radius.circular(12),
                      //             bottomRight: Radius.circular(12)),
                      //         color: Colors.red),
                      //     child: Padding(
                      //       padding: const EdgeInsets.only(
                      //           bottom: 12, right: 12, left: 12),
                      //       child: Column(
                      //         crossAxisAlignment: CrossAxisAlignment.start,
                      //         children: [

                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      // );
                    },
                  ),
                ),
          pengajuan == true
              ? Container(
                  height: 70,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: GestureDetector(
                        onTap: () async {
                          String refresh = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const ServiceScreen()));
                          if (refresh == 'refresh') {
                            setState(() {
                              fungsiGetServiceDutieAuth();
                            });
                          }
                        },
                        child: Container(
                          height: 40,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: orangeColor),
                          child: const Center(
                            child: Text(
                              'Ajukan Tugas Dinas',
                              style: TextStyle(
                                fontSize: 14,
                                fontFamily: "Roboto",
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              : const SizedBox()
        ],
      ),
    );
  }
}
