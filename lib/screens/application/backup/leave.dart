// ignore_for_file: use_build_context_synchronously

import 'dart:convert';
import 'dart:io';

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/services/leave_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import '../../../constants.dart';
import 'package:http/http.dart' as http;
import '../../../models/api_response_model.dart';
import '../../../services/auth_service.dart';
import '../../splash_screen.dart';
import '../../widget/custom_button.dart';
import '../../widget/custom_loading.dart';
import '../component/input_service.dart';

class LeaveScreen extends StatefulWidget {
  const LeaveScreen({super.key});

  @override
  State<LeaveScreen> createState() => _LeaveScreenState();
}

class _LeaveScreenState extends State<LeaveScreen> {
  bool _loading = false;
  TextEditingController abandonedJob = TextEditingController();
  TextEditingController leaveFromDate = TextEditingController();
  TextEditingController leaveToDate = TextEditingController();

  void fungsiCreateLeave() async {
    showAlertDialog(context);
    String? image = _imageFile == null ? null : getStringImage(_imageFile);
    ApiResponse response = await createLeave(
        abandonedJob: abandonedJob.text,
        leaveDromDate: leaveFromDate.text,
        leaveToDate: leaveToDate.text,
        leaveTypeId: dropdownvalue.toString(),
        proof: image);
    if (response.error == null) {
      AnimatedSnackBar.material('Berhasil Mengjukan Permohonan Izin',
              type: AnimatedSnackBarType.success,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        Navigator.pop(context);
        Navigator.pop(context, 'refresh');
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
      setState(() {
        _loading = !_loading;
      });
      Navigator.pop(context);
    }
  }

  File? _imageFile;
  String? nameFile;
  final _picker = ImagePicker();

  Future getImage() async {
    // ignore: deprecated_member_use
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
        nameFile = _imageFile!.path.split(Platform.pathSeparator).last;
      });
    }
  }

  List itemlist = [];
  var dropdownvalue;
  int? kuotaku;

  Future getJenisCuti() async {
    String token = await getToken();
    final response = await http.get(
      Uri.parse('$baseURL/attendances/leavetype'),
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'Bearer $token'
      },
    );

    if (response.statusCode == 200) {
      print(response.body);
      var jsonData = json.decode(response.body)['data']['leave_type'];
      int jsonData2 = json.decode(response.body)['data']['leave_quota'];
      setState(() {
        // print(jsonData);
        itemlist = jsonData;
        kuotaku = jsonData2;
      });
    }
  }

  @override
  void initState() {
    getJenisCuti();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Permohonan Izin",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: [
                Container(
                  height: 40,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.5),
                    ),
                  ),
                  child: Center(
                    child: Text(
                      kuotaku == null
                          ? 'Tunggu Sebentar'
                          : 'Sisa Jatah Izin Anda Adalah ${kuotaku.toString()}',
                      style: const TextStyle(
                        fontSize: 14,
                        color: black2Color,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w700,
                        letterSpacing: 1,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.5),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                color: blueColor,
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Image.asset(
                                  'assets/Icons/type.png',
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Container(
                                height: 48,
                                decoration: BoxDecoration(
                                    color: blueColor,
                                    borderRadius: BorderRadius.circular(15)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      icon: const Icon(
                                        CupertinoIcons.down_arrow,
                                        color: Colors.white,
                                      ),
                                      dropdownColor: const Color(0xffF0F1F2),
                                      borderRadius: BorderRadius.circular(12),
                                      hint: Text(
                                        kuotaku == null
                                            ? 'Tunggu Sebentar'
                                            : 'Pilih jenis cuti',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      items: itemlist.map((item) {
                                        return DropdownMenuItem(
                                          value:
                                              item['leave_type_id'].toString(),
                                          child: Text(
                                            item['leave_type_name'].toString(),
                                          ),
                                        );
                                      }).toList(),
                                      menuMaxHeight: 250,
                                      onChanged: (newVal) {
                                        setState(() {
                                          dropdownvalue = newVal;
                                          // ignore: avoid_print
                                          print(dropdownvalue);
                                        });
                                      },
                                      value: dropdownvalue,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        InputService1(
                          controller: abandonedJob,
                          icons: 'assets/Icons/job.png',
                          inputTitle: 'Masukkan pekerjaan yang ditinggalkan',
                          title: 'Pekerjaan Yang Ditinggalkan',
                        ),
                        InputService3(
                          controller: leaveFromDate,
                          icons: 'assets/Icons/clock.png',
                          inputTitle: 'Pilih tanggal',
                          title: 'Tanggal Awal',
                          ontaps: () async {
                            DateTime? pickedDate = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(2000),
                                lastDate: DateTime(2101));

                            if (pickedDate != null) {
                              String formattedDate =
                                  DateFormat('yyyy-MM-dd').format(pickedDate);

                              setState(() {
                                leaveFromDate.text = formattedDate;
                              });
                            } else {
                              print("Date is not selected");
                            }
                          },
                        ),
                        InputService3(
                          controller: leaveToDate,
                          icons: 'assets/Icons/clock.png',
                          inputTitle: 'Pilih tanggal',
                          title: 'Tanggal Akhir',
                          ontaps: () async {
                            DateTime? pickedDate = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(2000),
                                lastDate: DateTime(2101));

                            if (pickedDate != null) {
                              String formattedDate =
                                  DateFormat('yyyy-MM-dd').format(pickedDate);

                              setState(() {
                                leaveToDate.text = formattedDate;
                              });
                            } else {
                              print("Date is not selected");
                            }
                          },
                        ),
                        InputService4(
                          icons: 'assets/Icons/google-docs.png',
                          inputTitle: nameFile == null
                              ? 'Pilih Gambar'
                              : nameFile.toString(),
                          title: 'Bukti',
                          ontaps: () {
                            getImage();
                          },
                        ),
                        const Text(
                            'Ekstensi yang diperbolehkan hanya png, jpg, jpeg'),
                        _imageFile == null
                            ? const SizedBox()
                            : Container(
                                height: 150,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  image: DecorationImage(
                                      image: FileImage(_imageFile ?? File('')),
                                      fit: BoxFit.cover),
                                ),
                                child: Stack(
                                  children: [
                                    Positioned(
                                      right: 5,
                                      bottom: 5,
                                      child: GestureDetector(
                                        onTap: () {
                                          getImage();
                                        },
                                        child: Container(
                                          height: 30,
                                          width: 100,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                              color: Colors.white),
                                          child: Center(
                                            child: Text(
                                              'Ganti Foto',
                                              style: TextStyle(
                                                fontSize: 14,
                                                color: black2Color
                                                    .withOpacity(0.7),
                                                fontFamily: 'Roboto',
                                                fontWeight: FontWeight.w700,
                                                letterSpacing: 1,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                        CustomButton(
                            press: () async {
                              setState(() {
                                if (dropdownvalue == null ||
                                    abandonedJob.text == '' ||
                                    leaveFromDate.text == '' ||
                                    _imageFile == null) {
                                  AnimatedSnackBar.material(
                                          'Tidak Boleh Kosong',
                                          type: AnimatedSnackBarType.warning,
                                          mobileSnackBarPosition:
                                              MobileSnackBarPosition.top,
                                          desktopSnackBarPosition:
                                              DesktopSnackBarPosition.topCenter)
                                      .show(context);
                                } else {
                                  fungsiCreateLeave();
                                }
                              });
                            },
                            title: 'Simpan')
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
