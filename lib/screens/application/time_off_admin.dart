// ignore_for_file: use_build_context_synchronously

import 'dart:ui';

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/leave_permit_model.dart';
import 'package:attandy_app/screens/utils/action_menu.dart';
import 'package:attandy_app/services/leave_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import '../type_leave/type_leave.dart';
import '../widget/custom_loading.dart';
import '../widget/custom_shimmer.dart';
import 'application_screen.dart';
import 'component/status_time_off.dart';

class TimeOffAdmin extends StatefulWidget {
  const TimeOffAdmin({super.key});

  @override
  State<TimeOffAdmin> createState() => _TimeOffAdminState();
}

class _TimeOffAdminState extends State<TimeOffAdmin> {
  bool pengajuan = false;
  bool riwayat = false;

  String _formatDate(DateTime dateTime) {
    return DateFormat('d MMMM yyyy', "id_ID").format(dateTime);
  }

  bool _loading = true;
  List<dynamic> leaveList = [];

  Future<void> fungsiGetLeaveAll() async {
    ApiResponse response = await getLeaveAll();
    if (response.error == null) {
      setState(() {
        leaveList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  void fungsiApprovedLeave(
      {required int leaveId, required String statuss}) async {
    showAlertDialog(context);
    ApiResponse response =
        await aprrovedLeave(leaveId: leaveId, status: statuss);
    if (response.error == null) {
      setState(() {
        Navigator.pop(context);
        Navigator.pop(context);
        Navigator.pop(context);
        _loading = false;
        fungsiGetLeaveAll();
      });
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Berhasil $statuss')));
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  @override
  void initState() {
    pengajuan = true;
    fungsiGetLeaveAll();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Time Off",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: Column(
        children: [
          Row(
            children: [
              pengajuan == true
                  ? TrueTab(
                      title: 'Pengajuan',
                      ontaps: () {},
                    )
                  : FalseTab(
                      title: 'Pengajuan',
                      ontaps: () {
                        setState(() {
                          pengajuan = true;
                          riwayat = false;
                        });
                      },
                    ),
              riwayat == true
                  ? TrueTab(
                      title: 'Riwayat',
                      ontaps: () {},
                    )
                  : FalseTab(
                      title: 'Riwayat',
                      ontaps: () {
                        setState(() {
                          pengajuan = false;
                          riwayat = true;
                        });
                      },
                    ),
            ],
          ),
          pengajuan == true
              ? Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const TypeLeaveScreen()));
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: Colors.white),
                      child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                height: 35,
                                width: 35,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: blueColor,
                                ),
                                child: ClipRect(
                                  child: Padding(
                                    padding: const EdgeInsets.all(6.0),
                                    child: Image.asset(
                                      'assets/Icons/type.png',
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              const Expanded(
                                child: Text(
                                  "Data Master Jenis Izin Cuti",
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: black2Color,
                                    fontFamily: 'Roboto',
                                    fontWeight: FontWeight.w700,
                                    letterSpacing: 1,
                                  ),
                                ),
                              ),
                              SizedBox(
                                  height: 25,
                                  width: 25,
                                  child: Image.asset('assets/Icons/next.png'))
                            ],
                          )),
                    ),
                  ),
                )
              : const SizedBox(),
          pengajuan == true
              ? _loading
                  ? Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: SizedBox(
                              height: 100,
                              width: MediaQuery.of(context).size.width,
                              child: Lottie.asset(
                                "assets/lottie/loading.json",
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Expanded(
                      child: ListView.builder(
                          itemCount: leaveList.length,
                          itemBuilder: (BuildContext context, int index) {
                            LeaveAllModel leaveAllModel = leaveList[index];
                            String dateFrom = DateFormat('dd')
                                .format(leaveAllModel.leaveFromDate);
                            return leaveAllModel.status == 'Pending'
                                ? Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            showModalBottomSheet(
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(15.0),
                                              ),
                                              context: context,
                                              builder: (builder) {
                                                return Container(
                                                    height: 150.0,
                                                    color: Colors
                                                        .transparent, //could change this to Color(0xFF737373),
                                                    child: ActionMenu(
                                                      detail: () {
                                                        showModalBottomSheet(
                                                          shape: const RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius.vertical(
                                                                      top: Radius
                                                                          .circular(
                                                                              15.0))),
                                                          backgroundColor:
                                                              Colors.white,
                                                          context: context,
                                                          // isScrollControlled: true,
                                                          builder: (context) =>
                                                              Padding(
                                                            padding: EdgeInsets.only(
                                                                bottom: MediaQuery.of(
                                                                        context)
                                                                    .viewInsets
                                                                    .bottom),
                                                            child:
                                                                StatusTimeOff(
                                                              name: leaveAllModel
                                                                  .leaveTypeName,
                                                              job: leaveAllModel
                                                                  .abandonedJob,
                                                              date:
                                                                  '$dateFrom - ${_formatDate(leaveAllModel.leaveToDate)}',
                                                              bukti:
                                                                  leaveAllModel
                                                                      .proof,
                                                              ontaps: () {
                                                                showGeneralDialog(
                                                                  context:
                                                                      context,
                                                                  barrierLabel:
                                                                      "Barrier",
                                                                  barrierDismissible:
                                                                      true,
                                                                  barrierColor: Colors
                                                                      .black
                                                                      .withOpacity(
                                                                          0.5),
                                                                  transitionDuration:
                                                                      const Duration(
                                                                          milliseconds:
                                                                              300),
                                                                  pageBuilder:
                                                                      (_, __,
                                                                          ___) {
                                                                    return Center(
                                                                      child:
                                                                          Container(
                                                                        width:
                                                                            281.0,
                                                                        height:
                                                                            500.0,

                                                                        // ignore: sort_child_properties_last
                                                                        child: SizedBox.expand(
                                                                            child: CachedNetworkImage(
                                                                          imageUrl:
                                                                              leaveAllModel.proof,
                                                                          fit: BoxFit
                                                                              .cover,
                                                                          imageBuilder: (context, imageProvider) =>
                                                                              Container(
                                                                            width:
                                                                                281.0,
                                                                            height:
                                                                                500.0,
                                                                            decoration:
                                                                                BoxDecoration(
                                                                              borderRadius: BorderRadius.circular(12),
                                                                              image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                                                                            ),
                                                                          ),
                                                                          placeholder: (context, url) => const CustomShimmer(
                                                                              width: 281.0,
                                                                              height: 500.0,
                                                                              radius: 12),
                                                                          errorWidget: (context, url, error) =>
                                                                              const Icon(Icons.error),
                                                                        )),
                                                                        margin: const EdgeInsets.symmetric(
                                                                            horizontal:
                                                                                20),
                                                                        decoration:
                                                                            BoxDecoration(
                                                                          borderRadius:
                                                                              BorderRadius.circular(12),
                                                                        ),
                                                                      ),
                                                                    );
                                                                  },
                                                                  transitionBuilder:
                                                                      (_,
                                                                          anim,
                                                                          __,
                                                                          child) {
                                                                    Tween<Offset>
                                                                        tween;
                                                                    if (anim.status ==
                                                                        AnimationStatus
                                                                            .reverse) {
                                                                      tween = Tween(
                                                                          begin: const Offset(
                                                                              -1,
                                                                              0),
                                                                          end: Offset
                                                                              .zero);
                                                                    } else {
                                                                      tween = Tween(
                                                                          begin: const Offset(
                                                                              1,
                                                                              0),
                                                                          end: Offset
                                                                              .zero);
                                                                    }

                                                                    return SlideTransition(
                                                                      position:
                                                                          tween.animate(
                                                                              anim),
                                                                      child:
                                                                          FadeTransition(
                                                                        opacity:
                                                                            anim,
                                                                        child:
                                                                            child,
                                                                      ),
                                                                    );
                                                                  },
                                                                );
                                                              },
                                                              status: leaveAllModel
                                                                          .status ==
                                                                      'Pending'
                                                                  ? 'Diproses'
                                                                  : leaveAllModel
                                                                              .status ==
                                                                          'Cencel'
                                                                      ? 'Ditolak'
                                                                      : 'Disetujui',
                                                              colors: leaveAllModel
                                                                          .status ==
                                                                      'Pending'
                                                                  ? orangeColor
                                                                  : leaveAllModel
                                                                              .status ==
                                                                          'Cencel'
                                                                      ? redColor
                                                                      : greenColor,
                                                              title: 'Bukti',
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                      persetujuan: () {
                                                        showGeneralDialog(
                                                          barrierDismissible:
                                                              true,
                                                          barrierLabel: '',
                                                          barrierColor:
                                                              Colors.black38,
                                                          transitionDuration:
                                                              const Duration(
                                                                  milliseconds:
                                                                      300),
                                                          pageBuilder: (ctx,
                                                                  anim1,
                                                                  anim2) =>
                                                              AlertDialog(
                                                            title: Text(
                                                                '${leaveAllModel.firstName} ${leaveAllModel.lastName}'),
                                                            content: Text(
                                                              'Mengajukan Permohonan ${leaveAllModel.leaveTypeName} pada tanggal ${_formatDate(leaveAllModel.leaveFromDate)} - ${_formatDate(leaveAllModel.leaveToDate)}',
                                                              style:
                                                                  const TextStyle(
                                                                fontSize: 14,
                                                                fontFamily:
                                                                    "Roboto",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                                color:
                                                                    black2Color,
                                                              ),
                                                            ),
                                                            elevation: 2,
                                                            actions: [
                                                              Row(
                                                                children: [
                                                                  Expanded(
                                                                    child:
                                                                        GestureDetector(
                                                                      onTap:
                                                                          () {
                                                                        fungsiApprovedLeave(
                                                                            leaveId:
                                                                                leaveAllModel.leavePermitId,
                                                                            statuss: 'Cencel');
                                                                      },
                                                                      child:
                                                                          Container(
                                                                        height:
                                                                            45,
                                                                        decoration: BoxDecoration(
                                                                            borderRadius:
                                                                                BorderRadius.circular(12),
                                                                            color: redColor.withOpacity(0.9)),
                                                                        child:
                                                                            const Center(
                                                                          child:
                                                                              Text(
                                                                            'Tolak',
                                                                            style: TextStyle(
                                                                                fontSize: 14,
                                                                                fontFamily: "Roboto",
                                                                                fontWeight: FontWeight.bold,
                                                                                color: Colors.white,
                                                                                letterSpacing: 0.5),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  const SizedBox(
                                                                    width: 10,
                                                                  ),
                                                                  Expanded(
                                                                    child:
                                                                        GestureDetector(
                                                                      onTap:
                                                                          () {
                                                                        fungsiApprovedLeave(
                                                                            leaveId:
                                                                                leaveAllModel.leavePermitId,
                                                                            statuss: 'Approved');
                                                                      },
                                                                      child:
                                                                          Container(
                                                                        height:
                                                                            45,
                                                                        decoration: BoxDecoration(
                                                                            borderRadius:
                                                                                BorderRadius.circular(12),
                                                                            color: greenColor),
                                                                        child:
                                                                            const Center(
                                                                          child:
                                                                              Text(
                                                                            'Setujui',
                                                                            style: TextStyle(
                                                                                fontSize: 14,
                                                                                fontFamily: "Roboto",
                                                                                fontWeight: FontWeight.bold,
                                                                                color: Colors.white,
                                                                                letterSpacing: 0.5),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                          transitionBuilder:
                                                              (ctx,
                                                                      anim1,
                                                                      anim2,
                                                                      child) =>
                                                                  BackdropFilter(
                                                            filter: ImageFilter.blur(
                                                                sigmaX: 4 *
                                                                    anim1.value,
                                                                sigmaY: 4 *
                                                                    anim1
                                                                        .value),
                                                            child:
                                                                FadeTransition(
                                                              child: child,
                                                              opacity: anim1,
                                                            ),
                                                          ),
                                                          context: context,
                                                        );
                                                      },
                                                    ));
                                              },
                                            );
                                          },
                                          child: Container(
                                            height: 100,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey
                                                      .withOpacity(0.2),
                                                  spreadRadius: 2,
                                                  blurRadius: 5,
                                                  offset: const Offset(0,
                                                      3), // changes position of shadow
                                                ),
                                              ],
                                            ),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                      height: 45,
                                                      width: 45,
                                                      decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                      ),
                                                      child: CachedNetworkImage(
                                                        imageUrl:
                                                            leaveAllModel.image,
                                                        fit: BoxFit.cover,
                                                        imageBuilder: (context,
                                                                imageProvider) =>
                                                            Container(
                                                          height: 45,
                                                          width: 45,
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            image: DecorationImage(
                                                                image:
                                                                    imageProvider,
                                                                fit: BoxFit
                                                                    .cover),
                                                          ),
                                                        ),
                                                        placeholder: (context,
                                                                url) =>
                                                            const CustomShimmer(
                                                                height: 45,
                                                                width: 45,
                                                                radius: 5),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            const Icon(
                                                                Icons.error),
                                                      )),
                                                  const SizedBox(
                                                    width: 10,
                                                  ),
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          '${leaveAllModel.firstName} ${leaveAllModel.lastName}',
                                                          style: const TextStyle(
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Roboto",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color:
                                                                  black2Color,
                                                              letterSpacing:
                                                                  0.5),
                                                        ),
                                                        const SizedBox(
                                                          height: 5,
                                                        ),
                                                        Text(
                                                          'Mengajukan Permohonan ${leaveAllModel.leaveTypeName} pada tanggal ${_formatDate(leaveAllModel.leaveFromDate)} - ${_formatDate(leaveAllModel.leaveToDate)}',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 13,
                                                            fontFamily:
                                                                "Roboto",
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            color: black2Color,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Row(
                                                          children: [
                                                            const Text(
                                                              'Status : ',
                                                              style: TextStyle(
                                                                  fontSize: 13,
                                                                  fontFamily:
                                                                      "Roboto",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .normal,
                                                                  color:
                                                                      black2Color,
                                                                  letterSpacing:
                                                                      0.5),
                                                            ),
                                                            Text(
                                                              leaveAllModel
                                                                  .status,
                                                              style: const TextStyle(
                                                                  fontSize: 13,
                                                                  fontFamily:
                                                                      "Roboto",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color:
                                                                      yellowColor,
                                                                  letterSpacing:
                                                                      0.5),
                                                            ),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 10,
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                : const SizedBox();
                          }),
                    )
              : _loading
                  ? Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: SizedBox(
                              height: 100,
                              width: MediaQuery.of(context).size.width,
                              child: Lottie.asset(
                                "assets/lottie/loading.json",
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Expanded(
                      child: ListView.builder(
                          itemCount: leaveList.length,
                          itemBuilder: (BuildContext context, int index) {
                            LeaveAllModel leaveAllModel = leaveList[index];
                            String dateFrom = DateFormat('dd')
                                .format(leaveAllModel.leaveFromDate);
                            return leaveAllModel.status != 'Pending'
                                ? Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            showModalBottomSheet(
                                              shape:
                                                  const RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.vertical(
                                                              top: Radius
                                                                  .circular(
                                                                      15.0))),
                                              backgroundColor: Colors.white,
                                              context: context,
                                              // isScrollControlled: true,
                                              builder: (context) => Padding(
                                                padding: EdgeInsets.only(
                                                    bottom:
                                                        MediaQuery.of(context)
                                                            .viewInsets
                                                            .bottom),
                                                child: StatusTimeOff(
                                                  name: leaveAllModel
                                                      .leaveTypeName,
                                                  job: leaveAllModel
                                                      .abandonedJob,
                                                  date:
                                                      '$dateFrom - ${_formatDate(leaveAllModel.leaveToDate)}',
                                                  bukti: leaveAllModel.proof,
                                                  ontaps: () {
                                                    showGeneralDialog(
                                                      context: context,
                                                      barrierLabel: "Barrier",
                                                      barrierDismissible: true,
                                                      barrierColor: Colors.black
                                                          .withOpacity(0.5),
                                                      transitionDuration:
                                                          const Duration(
                                                              milliseconds:
                                                                  300),
                                                      pageBuilder:
                                                          (_, __, ___) {
                                                        return Center(
                                                          child: Container(
                                                            width: 281.0,
                                                            height: 500.0,

                                                            // ignore: sort_child_properties_last
                                                            child:
                                                                SizedBox.expand(
                                                                    child:
                                                                        CachedNetworkImage(
                                                              imageUrl:
                                                                  leaveAllModel
                                                                      .proof,
                                                              fit: BoxFit.cover,
                                                              imageBuilder:
                                                                  (context,
                                                                          imageProvider) =>
                                                                      Container(
                                                                width: 281.0,
                                                                height: 500.0,
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              12),
                                                                  image: DecorationImage(
                                                                      image:
                                                                          imageProvider,
                                                                      fit: BoxFit
                                                                          .cover),
                                                                ),
                                                              ),
                                                              placeholder: (context,
                                                                      url) =>
                                                                  const CustomShimmer(
                                                                      width:
                                                                          281.0,
                                                                      height:
                                                                          500.0,
                                                                      radius:
                                                                          12),
                                                              errorWidget: (context,
                                                                      url,
                                                                      error) =>
                                                                  const Icon(Icons
                                                                      .error),
                                                            )),
                                                            margin:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        20),
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          12),
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                      transitionBuilder:
                                                          (_, anim, __, child) {
                                                        Tween<Offset> tween;
                                                        if (anim.status ==
                                                            AnimationStatus
                                                                .reverse) {
                                                          tween = Tween(
                                                              begin:
                                                                  const Offset(
                                                                      -1, 0),
                                                              end: Offset.zero);
                                                        } else {
                                                          tween = Tween(
                                                              begin:
                                                                  const Offset(
                                                                      1, 0),
                                                              end: Offset.zero);
                                                        }

                                                        return SlideTransition(
                                                          position: tween
                                                              .animate(anim),
                                                          child: FadeTransition(
                                                            opacity: anim,
                                                            child: child,
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  },
                                                  status: leaveAllModel
                                                              .status ==
                                                          'Pending'
                                                      ? 'Diproses'
                                                      : leaveAllModel.status ==
                                                              'Cencel'
                                                          ? 'Ditolak'
                                                          : 'Disetujui',
                                                  colors: leaveAllModel
                                                              .status ==
                                                          'Pending'
                                                      ? orangeColor
                                                      : leaveAllModel.status ==
                                                              'Cencel'
                                                          ? redColor
                                                          : greenColor,
                                                  title: 'Bukti',
                                                ),
                                              ),
                                            );
                                          },
                                          child: Container(
                                            height: 100,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey
                                                      .withOpacity(0.2),
                                                  spreadRadius: 2,
                                                  blurRadius: 5,
                                                  offset: const Offset(0,
                                                      3), // changes position of shadow
                                                ),
                                              ],
                                            ),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                      height: 45,
                                                      width: 45,
                                                      decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                      ),
                                                      child: CachedNetworkImage(
                                                        imageUrl:
                                                            leaveAllModel.image,
                                                        fit: BoxFit.cover,
                                                        imageBuilder: (context,
                                                                imageProvider) =>
                                                            Container(
                                                          height: 45,
                                                          width: 45,
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            image: DecorationImage(
                                                                image:
                                                                    imageProvider,
                                                                fit: BoxFit
                                                                    .cover),
                                                          ),
                                                        ),
                                                        placeholder: (context,
                                                                url) =>
                                                            const CustomShimmer(
                                                                height: 45,
                                                                width: 45,
                                                                radius: 5),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            const Icon(
                                                                Icons.error),
                                                      )),
                                                  const SizedBox(
                                                    width: 10,
                                                  ),
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          '${leaveAllModel.firstName} ${leaveAllModel.lastName}',
                                                          style: const TextStyle(
                                                              fontSize: 14,
                                                              fontFamily:
                                                                  "Roboto",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color:
                                                                  black2Color,
                                                              letterSpacing:
                                                                  0.5),
                                                        ),
                                                        const SizedBox(
                                                          height: 5,
                                                        ),
                                                        Text(
                                                          'Mengajukan ${leaveAllModel.leaveTypeName} pada tanggal ${_formatDate(leaveAllModel.leaveFromDate)} - ${_formatDate(leaveAllModel.leaveToDate)}',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 13,
                                                            fontFamily:
                                                                "Roboto",
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            color: black2Color,
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Row(
                                                          children: [
                                                            const Text(
                                                              'Status : ',
                                                              style: TextStyle(
                                                                  fontSize: 13,
                                                                  fontFamily:
                                                                      "Roboto",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .normal,
                                                                  color:
                                                                      black2Color,
                                                                  letterSpacing:
                                                                      0.5),
                                                            ),
                                                            Text(
                                                              leaveAllModel
                                                                  .status,
                                                              style: TextStyle(
                                                                  fontSize: 13,
                                                                  fontFamily:
                                                                      "Roboto",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: leaveAllModel
                                                                              .status ==
                                                                          'Cencel'
                                                                      ? redColor
                                                                      : greenColor,
                                                                  letterSpacing:
                                                                      0.5),
                                                            ),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 10,
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                : const SizedBox();
                          }),
                    )
        ],
      ),
    );
  }
}
