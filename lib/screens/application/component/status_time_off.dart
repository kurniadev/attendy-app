import 'package:attandy_app/constants.dart';
import 'package:flutter/material.dart';

class StatusTimeOff extends StatefulWidget {
  const StatusTimeOff(
      {super.key,
      required this.name,
      required this.date,
      required this.job,
      required this.bukti,
      required this.ontaps,
      required this.colors,
      required this.status, required this.title});

  final String name, date, job, bukti, status, title;
  final VoidCallback ontaps;
  final Color colors;

  @override
  State<StatusTimeOff> createState() => _StatusTimeOffState();
}

class _StatusTimeOffState extends State<StatusTimeOff> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Center(
            child: Container(
              height: 5,
              width: 50,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: black2Color.withOpacity(0.5)),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 100,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: widget.colors.withOpacity(0.2)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.name,
                  style: const TextStyle(
                      fontSize: 14,
                      fontFamily: "Roboto",
                      fontWeight: FontWeight.normal,
                      color: black2Color,
                      letterSpacing: 0.5),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  widget.status,
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.bold,
                    color: widget.colors,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Container(
                height: 45,
                width: 45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: blueColor.withOpacity(0.2)),
                child: const Icon(
                  Icons.date_range,
                  color: blueColor,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Tanggal',
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.normal,
                        color: black2Color,
                        letterSpacing: 0.5),
                  ),
                  Text(
                    widget.date,
                    style: const TextStyle(
                        fontSize: 14,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.bold,
                        color: black2Color,
                        letterSpacing: 0.5),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Container(
                height: 45,
                width: 45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: blueColor.withOpacity(0.2)),
                child: const Icon(
                  Icons.book,
                  color: blueColor,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Pekerjaan yang ditinggalkan',
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.normal,
                        color: black2Color,
                        letterSpacing: 0.5),
                  ),
                  Text(
                    widget.job,
                    style: const TextStyle(
                        fontSize: 14,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.bold,
                        color: black2Color,
                        letterSpacing: 0.5),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Container(
                height: 45,
                width: 45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: blueColor.withOpacity(0.2)),
                child: const Icon(
                  Icons.picture_in_picture,
                  color: blueColor,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                   Text(
                        'Lihat ${widget.title}',
                    style: const TextStyle(
                        fontSize: 14,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.normal,
                        color: black2Color,
                        letterSpacing: 0.5),
                  ),
                  GestureDetector(
                    onTap: widget.ontaps,
                    child: Container(
                      height: 28,
                      width: MediaQuery.of(context).size.width * 0.6,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: black2Color.withOpacity(0.2)),
                      child: Center(
                        child: Text(
                      widget.title,
                          style: const  TextStyle(
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.bold,
                              color: black2Color,
                              letterSpacing: 0.5),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
