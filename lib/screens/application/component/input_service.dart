import 'package:flutter/material.dart';

import '../../../constants.dart';

class InputService1 extends StatelessWidget {
  const InputService1(
      {super.key,
      required this.icons,
      required this.title,
      required this.inputTitle,
      required this.controller});

  final String icons, title, inputTitle;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 45,
              width: 45,
              decoration: BoxDecoration(
                color: blueColor,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset(
                  icons,
                  color: Colors.white,
                ),
              ),
            ),
            const SizedBox(
              width: 7,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color.withOpacity(0.7),
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                      letterSpacing: 1,
                    ),
                  ),
                  SizedBox(
                    height: 35,
                    child: TextFormField(
                      controller: controller,
                      style: TextStyle(
                          fontFamily: 'Roboto', fontWeight: FontWeight.bold),
                      decoration: InputDecoration(
                        // border: InputBorder.none,
                        hintText: inputTitle,
                        hintStyle: const TextStyle(fontSize: 16),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        const SizedBox(
          height: 15,
        )
      ],
    );
  }
}

class InputService2 extends StatelessWidget {
  const InputService2({
    super.key,
    required this.icons,
    required this.title,
    required this.inputTitle,
    required this.ontaps,
  });

  final String icons, title, inputTitle;
  final VoidCallback ontaps;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 45,
              width: 45,
              decoration: BoxDecoration(
                color: blueColor,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset(
                  icons,
                  color: Colors.white,
                ),
              ),
            ),
            const SizedBox(
              width: 7,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color.withOpacity(0.7),
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                      letterSpacing: 1,
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  GestureDetector(
                    onTap: ontaps,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 30,
                      decoration: BoxDecoration(
                        color: greyColor.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Center(
                        child: Text(
                          inputTitle,
                          maxLines: 1,
                          style: TextStyle(
                            fontSize: 12,
                            color: black2Color.withOpacity(0.8),
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w700,
                            letterSpacing: 1,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        const SizedBox(
          height: 15,
        )
      ],
    );
  }
}

class InputService3 extends StatelessWidget {
  const InputService3(
      {super.key,
      required this.icons,
      required this.title,
      required this.inputTitle,
      required this.controller,
      required this.ontaps});

  final String icons, title, inputTitle;
  final TextEditingController controller;
  final VoidCallback ontaps;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 45,
              width: 45,
              decoration: BoxDecoration(
                color: blueColor,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset(
                  icons,
                  color: Colors.white,
                ),
              ),
            ),
            const SizedBox(
              width: 7,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color.withOpacity(0.7),
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                      letterSpacing: 1,
                    ),
                  ),
                  SizedBox(
                    height: 35,
                    child: TextField(
                        controller: controller,
                        readOnly: true,
                        style: const TextStyle(
                            fontFamily: 'Roboto', fontWeight: FontWeight.bold),
                        decoration: InputDecoration(
                          // border: InputBorder.none,
                          hintText: inputTitle,
                          hintStyle: const TextStyle(fontSize: 16),
                        ),
                        onTap: ontaps),
                  )
                ],
              ),
            )
          ],
        ),
        const SizedBox(
          height: 15,
        )
      ],
    );
  }
}

class InputService4 extends StatelessWidget {
  const InputService4({
    super.key,
    required this.icons,
    required this.title,
    required this.inputTitle,
    required this.ontaps,
  });

  final String icons, title, inputTitle;
  final VoidCallback ontaps;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 45,
              width: 45,
              decoration: BoxDecoration(
                color: blueColor,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset(
                  icons,
                  color: Colors.white,
                ),
              ),
            ),
            const SizedBox(
              width: 7,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: 14,
                      color: black2Color.withOpacity(0.7),
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                      letterSpacing: 1,
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  GestureDetector(
                    onTap: ontaps,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 30,
                      decoration: BoxDecoration(
                        color: greyColor.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Center(
                        child: Text(
                          inputTitle,
                          style: TextStyle(
                            fontSize: 12,
                            color: black2Color.withOpacity(0.8),
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w700,
                            letterSpacing: 1,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        const SizedBox(
          height: 15,
        )
      ],
    );
  }
}
