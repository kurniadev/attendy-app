import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'package:pdf_viewer_flutter/pdf_viewer_flutter.dart';

import '../../../constants.dart';

class ViewPdf extends StatefulWidget {
  const ViewPdf({super.key, required this.urlDoc});

  final String urlDoc;

  @override
  State<ViewPdf> createState() => _ViewPdfState();
}

class _ViewPdfState extends State<ViewPdf> {
  String _pefFilePath = "";

  Future<File> createFileOfPdfUrl({required urls}) async {
    final url = urls;
    final filename = url.substring(url.lastIndexOf("/") + 1);
    var request = await HttpClient().getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = File('$dir/$filename');
    await file.writeAsBytes(bytes);
    return file;
  }

  @override
  void initState() {
    createFileOfPdfUrl(urls: widget.urlDoc).then((file) {
      setState(() {
        _pefFilePath = file.path;
        print(_pefFilePath);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Lihat Dokumen",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: _pefFilePath == ''
          ? const Center(child: Text('Sedang Memuat'))
          : SafeArea(
              child: PDFViewerScaffold(
                  appBar: AppBar(
                    title: const Text(
                      "Lihat Dokumen",
                      style: TextStyle(
                        fontSize: 18,
                        color: black2Color,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w700,
                        letterSpacing: 1,
                      ),
                    ),
                    backgroundColor: Colors.white,
                    iconTheme: const IconThemeData(
                      color: black2Color,
                    ),
                    elevation: 0,
                    
                  ),
                  path: _pefFilePath),
            ),
    );
  }
}
