import 'package:flutter/material.dart';

import '../../../constants.dart';

class ServiceDuties extends StatefulWidget {
  const ServiceDuties(
      {super.key,
      required this.colors,
      required this.status,
      required this.fromdate,
      required this.toDate,
      required this.purpose,
      required this.place});

  final Color colors;
  final String status, fromdate, toDate, purpose, place;

  @override
  State<ServiceDuties> createState() => _ServiceDutiesState();
}

class _ServiceDutiesState extends State<ServiceDuties> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Container(
        height: 100,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: greyColor.withOpacity(0.5)),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.purpose,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontSize: 16,
                        color: black2Color,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Text(
                      '${widget.fromdate} Sampai ${widget.toDate}',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black.withOpacity(0.5),
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.location_on,
                          size: 16,
                          color: widget.colors,
                        ),
                        Text(
                          widget.place,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 14,
                            color: widget.colors,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 100,
                width: 100,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 30,
                      width: 80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: widget.colors.withOpacity(0.1)),
                      child: Center(
                        child: Text(
                          widget.status,
                          style: TextStyle(
                              fontSize: 12,
                              color: widget.colors,
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w700,
                              letterSpacing: 0.5),
                        ),
                      ),
                    ),
                    Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color:
                              Color.fromARGB(66, 75, 75, 75).withOpacity(0.1),
                          image: const DecorationImage(
                              image: AssetImage('assets/Icons/next.png'),
                              opacity: 0.2)),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
