// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/screens/application/component/status_time_off.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../models/leave_permit_model.dart';
import '../../services/auth_service.dart';
import '../../services/leave_service.dart';
import '../splash_screen.dart';
import '../widget/custom_shimmer.dart';
import 'application_screen.dart';
import 'backup/leave.dart';

class TimeOffScreen extends StatefulWidget {
  const TimeOffScreen({super.key});

  @override
  State<TimeOffScreen> createState() => _TimeOffScreenState();
}

class _TimeOffScreenState extends State<TimeOffScreen> {
  bool pengajuan = false;
  bool riwayat = false;

  String _formatDate(DateTime dateTime) {
    return DateFormat('d MMMM yyyy', "id_ID").format(dateTime);
  }

  bool _loading = true;
  List<dynamic> leaveList = [];

  Future<void> fungsiGetLeaveAuth() async {
    ApiResponse response = await getLeaveAuth();
    if (response.error == null) {
      setState(() {
        leaveList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  @override
  void initState() {
    pengajuan = true;
    fungsiGetLeaveAuth();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Time Off",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      body: Column(
        children: [
          Row(
            children: [
              pengajuan == true
                  ? TrueTab(
                      title: 'Pengajuan',
                      ontaps: () {},
                    )
                  : FalseTab(
                      title: 'Pengajuan',
                      ontaps: () {
                        setState(() {
                          pengajuan = true;
                          riwayat = false;
                        });
                      },
                    ),
              riwayat == true
                  ? TrueTab(
                      title: 'Riwayat',
                      ontaps: () {},
                    )
                  : FalseTab(
                      title: 'Riwayat',
                      ontaps: () {
                        setState(() {
                          pengajuan = false;
                          riwayat = true;
                        });
                      },
                    ),
            ],
          ),
          pengajuan == true
              ? Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: Colors.white),
                    child: const Padding(
                      padding: EdgeInsets.all(12.0),
                      child: Text(
                        'Daftar Pengajuan Di Proses',
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "Roboto",
                            fontWeight: FontWeight.bold,
                            color: black2Color,
                            letterSpacing: 0.5),
                      ),
                    ),
                  ),
                )
              : const SizedBox(),
          riwayat == true
              ? _loading
                  ? Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: SizedBox(
                              height: 100,
                              width: MediaQuery.of(context).size.width,
                              child: Lottie.asset(
                                "assets/lottie/loading.json",
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Expanded(
                      child: ListView.builder(
                        itemCount: leaveList.length,
                        itemBuilder: (BuildContext context, int index) {
                          LeavePermitModel leavePermitModel = leaveList[index];
                          String dateFrom = DateFormat('dd')
                              .format(leavePermitModel.leaveFromDate);
                          return leavePermitModel.status != 'Pending'
                              ? Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        showModalBottomSheet(
                                          shape: const RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.vertical(
                                                      top: Radius.circular(
                                                          15.0))),
                                          backgroundColor: Colors.white,
                                          context: context,
                                          // isScrollControlled: true,
                                          builder: (context) => Padding(
                                              padding: EdgeInsets.only(
                                                  bottom: MediaQuery.of(context)
                                                      .viewInsets
                                                      .bottom),
                                              child: StatusTimeOff(
                                                colors: leavePermitModel
                                                            .status ==
                                                        'Pending'
                                                    ? orangeColor
                                                    : leavePermitModel.status ==
                                                            'Cencel'
                                                        ? redColor
                                                        : greenColor,
                                                name: leavePermitModel
                                                    .leaveTypeName,
                                                job: leavePermitModel
                                                    .abandonedJob,
                                                date:
                                                    '$dateFrom - ${_formatDate(leavePermitModel.leaveToDate)}',
                                                bukti: leavePermitModel.proof,
                                                status: leavePermitModel
                                                            .status ==
                                                        'Pending'
                                                    ? 'Diproses'
                                                    : leavePermitModel.status ==
                                                            'Cencel'
                                                        ? 'Ditolak'
                                                        : 'Disetujui',
                                                ontaps: () {
                                                  showGeneralDialog(
                                                    context: context,
                                                    barrierLabel: "Barrier",
                                                    barrierDismissible: true,
                                                    barrierColor: Colors.black
                                                        .withOpacity(0.5),
                                                    transitionDuration:
                                                        const Duration(
                                                            milliseconds: 300),
                                                    pageBuilder: (_, __, ___) {
                                                      return Center(
                                                        child: Container(
                                                          width: 281.0,
                                                          height: 500.0,

                                                          // ignore: sort_child_properties_last
                                                          child: SizedBox.expand(
                                                              child: CachedNetworkImage(
                                                            imageUrl:
                                                                leavePermitModel
                                                                    .proof,
                                                            fit: BoxFit.cover,
                                                            imageBuilder: (context,
                                                                    imageProvider) =>
                                                                Container(
                                                              width: 281.0,
                                                              height: 500.0,
                                                              decoration:
                                                                  BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            12),
                                                                image: DecorationImage(
                                                                    image:
                                                                        imageProvider,
                                                                    fit: BoxFit
                                                                        .cover),
                                                              ),
                                                            ),
                                                            placeholder: (context,
                                                                    url) =>
                                                                const CustomShimmer(
                                                                    width:
                                                                        281.0,
                                                                    height:
                                                                        500.0,
                                                                    radius: 12),
                                                            errorWidget: (context,
                                                                    url,
                                                                    error) =>
                                                                const Icon(Icons
                                                                    .error),
                                                          )),
                                                          margin:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      20),
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        12),
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                    transitionBuilder:
                                                        (_, anim, __, child) {
                                                      Tween<Offset> tween;
                                                      if (anim.status ==
                                                          AnimationStatus
                                                              .reverse) {
                                                        tween = Tween(
                                                            begin: const Offset(
                                                                -1, 0),
                                                            end: Offset.zero);
                                                      } else {
                                                        tween = Tween(
                                                            begin: const Offset(
                                                                1, 0),
                                                            end: Offset.zero);
                                                      }

                                                      return SlideTransition(
                                                        position:
                                                            tween.animate(anim),
                                                        child: FadeTransition(
                                                          opacity: anim,
                                                          child: child,
                                                        ),
                                                      );
                                                    },
                                                  );
                                                },
                                                title: 'Bukti',
                                              )),
                                        );
                                      },
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(12),
                                            border:
                                                Border.all(color: greyColor),
                                            color: Colors.white),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              const SizedBox(
                                                height: 5,
                                              ),
                                              Text(
                                                leavePermitModel.leaveTypeName,
                                                style: const TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: "Roboto",
                                                  fontWeight: FontWeight.bold,
                                                  color: black2Color,
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 8,
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    '$dateFrom - ${_formatDate(leavePermitModel.leaveToDate)}',
                                                    style: const TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: "Roboto",
                                                      fontWeight:
                                                          FontWeight.w700,
                                                      color: orangeColor,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 5),
                                                    height: 5,
                                                    width: 5,
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        color: black2Color
                                                            .withOpacity(0.8)),
                                                  ),
                                                  Row(
                                                    children: [
                                                      const Text(
                                                        'Status : ',
                                                        style: TextStyle(
                                                            fontSize: 13,
                                                            fontFamily:
                                                                "Roboto",
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            color: black2Color,
                                                            letterSpacing: 0.5),
                                                      ),
                                                      Text(
                                                        leavePermitModel.status,
                                                        style: TextStyle(
                                                            fontSize: 13,
                                                            fontFamily:
                                                                "Roboto",
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: leavePermitModel
                                                                        .status ==
                                                                    'Cencel'
                                                                ? redColor
                                                                : greenColor,
                                                            letterSpacing: 0.5),
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                              const SizedBox(
                                                height: 5,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : const SizedBox();
                          // Padding(
                          //   padding: const EdgeInsets.only(
                          //       bottom: 12, right: 12, left: 12),
                          //   child: Container(
                          //     width: MediaQuery.of(context).size.width,
                          //     decoration: const BoxDecoration(
                          //         borderRadius: BorderRadius.only(
                          //             bottomLeft: Radius.circular(12),
                          //             bottomRight: Radius.circular(12)),
                          //         color: Colors.red),
                          //     child: Padding(
                          //       padding: const EdgeInsets.only(
                          //           bottom: 12, right: 12, left: 12),
                          //       child: Column(
                          //         crossAxisAlignment: CrossAxisAlignment.start,
                          //         children: [

                          //         ],
                          //       ),
                          //     ),
                          //   ),
                          // );
                        },
                      ),
                    )
              : _loading
                  ? Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: SizedBox(
                              height: 100,
                              width: MediaQuery.of(context).size.width,
                              child: Lottie.asset(
                                "assets/lottie/loading.json",
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Expanded(
                      child: ListView.builder(
                        itemCount: leaveList.length,
                        itemBuilder: (BuildContext context, int index) {
                          LeavePermitModel leavePermitModel = leaveList[index];
                          String dateFrom = DateFormat('dd')
                              .format(leavePermitModel.leaveFromDate);
                          return leavePermitModel.status == 'Pending'
                              ? Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        showModalBottomSheet(
                                          shape: const RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.vertical(
                                                      top: Radius.circular(
                                                          15.0))),
                                          backgroundColor: Colors.white,
                                          context: context,
                                          // isScrollControlled: true,
                                          builder: (context) => Padding(
                                            padding: EdgeInsets.only(
                                                bottom: MediaQuery.of(context)
                                                    .viewInsets
                                                    .bottom),
                                            child: StatusTimeOff(
                                              name: leavePermitModel
                                                  .leaveTypeName,
                                              job:
                                                  leavePermitModel.abandonedJob,
                                              date:
                                                  '$dateFrom - ${_formatDate(leavePermitModel.leaveToDate)}',
                                              bukti: leavePermitModel.proof,
                                              ontaps: () {
                                                showGeneralDialog(
                                                  context: context,
                                                  barrierLabel: "Barrier",
                                                  barrierDismissible: true,
                                                  barrierColor: Colors.black
                                                      .withOpacity(0.5),
                                                  transitionDuration:
                                                      const Duration(
                                                          milliseconds: 300),
                                                  pageBuilder: (_, __, ___) {
                                                    return Center(
                                                      child: Container(
                                                        width: 281.0,
                                                        height: 500.0,

                                                        // ignore: sort_child_properties_last
                                                        child: SizedBox.expand(
                                                            child:
                                                                CachedNetworkImage(
                                                          imageUrl:
                                                              leavePermitModel
                                                                  .proof,
                                                          fit: BoxFit.cover,
                                                          imageBuilder: (context,
                                                                  imageProvider) =>
                                                              Container(
                                                            width: 281.0,
                                                            height: 500.0,
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          12),
                                                              image: DecorationImage(
                                                                  image:
                                                                      imageProvider,
                                                                  fit: BoxFit
                                                                      .cover),
                                                            ),
                                                          ),
                                                          placeholder: (context,
                                                                  url) =>
                                                              const CustomShimmer(
                                                                  width: 281.0,
                                                                  height: 500.0,
                                                                  radius: 12),
                                                          errorWidget: (context,
                                                                  url, error) =>
                                                              const Icon(
                                                                  Icons.error),
                                                        )),
                                                        margin: const EdgeInsets
                                                                .symmetric(
                                                            horizontal: 20),
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(12),
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                  transitionBuilder:
                                                      (_, anim, __, child) {
                                                    Tween<Offset> tween;
                                                    if (anim.status ==
                                                        AnimationStatus
                                                            .reverse) {
                                                      tween = Tween(
                                                          begin: const Offset(
                                                              -1, 0),
                                                          end: Offset.zero);
                                                    } else {
                                                      tween = Tween(
                                                          begin: const Offset(
                                                              1, 0),
                                                          end: Offset.zero);
                                                    }

                                                    return SlideTransition(
                                                      position:
                                                          tween.animate(anim),
                                                      child: FadeTransition(
                                                        opacity: anim,
                                                        child: child,
                                                      ),
                                                    );
                                                  },
                                                );
                                              },
                                              status: leavePermitModel.status ==
                                                      'Pending'
                                                  ? 'Diproses'
                                                  : leavePermitModel.status ==
                                                          'Cencel'
                                                      ? 'Ditolak'
                                                      : 'Disetujui',
                                              colors: leavePermitModel.status ==
                                                      'Pending'
                                                  ? orangeColor
                                                  : leavePermitModel.status ==
                                                          'Cencel'
                                                      ? redColor
                                                      : greenColor,
                                              title: 'Bukti',
                                            ),
                                          ),
                                        );
                                      },
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(12),
                                            border:
                                                Border.all(color: greyColor),
                                            color: Colors.white),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              const SizedBox(
                                                height: 5,
                                              ),
                                              Text(
                                                leavePermitModel.leaveTypeName,
                                                style: const TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: "Roboto",
                                                  fontWeight: FontWeight.bold,
                                                  color: black2Color,
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 8,
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    '$dateFrom - ${_formatDate(leavePermitModel.leaveToDate)}',
                                                    style: const TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: "Roboto",
                                                      fontWeight:
                                                          FontWeight.w700,
                                                      color: orangeColor,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 5),
                                                    height: 5,
                                                    width: 5,
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        color: black2Color
                                                            .withOpacity(0.8)),
                                                  ),
                                                  Row(
                                                    children: [
                                                      const Text(
                                                        'Status : ',
                                                        style: TextStyle(
                                                            fontSize: 13,
                                                            fontFamily:
                                                                "Roboto",
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            color: black2Color,
                                                            letterSpacing: 0.5),
                                                      ),
                                                      Text(
                                                        leavePermitModel.status,
                                                        style: TextStyle(
                                                            fontSize: 13,
                                                            fontFamily:
                                                                "Roboto",
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: leavePermitModel
                                                                        .status ==
                                                                    'Pending'
                                                                ? orangeColor
                                                                : leavePermitModel
                                                                            .status ==
                                                                        'Cencel'
                                                                    ? redColor
                                                                    : greenColor,
                                                            letterSpacing: 0.5),
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                              const SizedBox(
                                                height: 5,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : const SizedBox();
                          // Padding(
                          //   padding: const EdgeInsets.only(
                          //       bottom: 12, right: 12, left: 12),
                          //   child: Container(
                          //     width: MediaQuery.of(context).size.width,
                          //     decoration: const BoxDecoration(
                          //         borderRadius: BorderRadius.only(
                          //             bottomLeft: Radius.circular(12),
                          //             bottomRight: Radius.circular(12)),
                          //         color: Colors.red),
                          //     child: Padding(
                          //       padding: const EdgeInsets.only(
                          //           bottom: 12, right: 12, left: 12),
                          //       child: Column(
                          //         crossAxisAlignment: CrossAxisAlignment.start,
                          //         children: [

                          //         ],
                          //       ),
                          //     ),
                          //   ),
                          // );
                        },
                      ),
                    ),
          pengajuan == true
              ? Container(
                  height: 70,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: GestureDetector(
                        onTap: () async {
                          String refresh = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LeaveScreen()));
                          if (refresh == 'refresh') {
                            setState(() {
                              fungsiGetLeaveAuth();
                            });
                          }
                        },
                        child: Container(
                          height: 40,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: orangeColor),
                          child: const Center(
                            child: Text(
                              'Ajukan Time Off',
                              style: TextStyle(
                                fontSize: 14,
                                fontFamily: "Roboto",
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              : const SizedBox()
        ],
      ),
    );
  }
}
