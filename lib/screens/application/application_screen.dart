// ignore_for_file: use_build_context_synchronously

import 'package:animated_snack_bar/animated_snack_bar.dart';
import 'package:attandy_app/models/leave_permit_model.dart';
import 'package:attandy_app/models/service_dutie_model.dart';
import 'package:attandy_app/screens/application/component/leavePermit.dart';
import 'package:attandy_app/screens/application/backup/leave.dart';
import 'package:attandy_app/screens/application/service.dart';
import 'package:attandy_app/services/leave_service.dart';
import 'package:attandy_app/services/servicedutie_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';

import '../../constants.dart';
import '../../models/api_response_model.dart';
import '../../services/auth_service.dart';
import '../splash_screen.dart';
import 'component/servicedutie.dart';

class ApplicationScreen extends StatefulWidget {
  const ApplicationScreen({super.key});

  @override
  State<ApplicationScreen> createState() => AapplicationStateScreen();
}

class AapplicationStateScreen extends State<ApplicationScreen> {
  bool service = false;
  bool leave = false;
  bool _showFab = true;

  bool _loading = true;
  List<dynamic> leaveList = [];
  bool _loading1 = true;
  List<dynamic> serviceDutieList = [];

  Future<void> fungsiGetLeaveAuth() async {
    ApiResponse response = await getLeaveAuth();
    if (response.error == null) {
      setState(() {
        leaveList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  Future<void> fungsiGetServiceDutieAuth() async {
    ApiResponse response = await getServiceDutieAuth();
    if (response.error == null) {
      setState(() {
        serviceDutieList = response.data as List<dynamic>;
        _loading1 = _loading1 ? !_loading1 : _loading1;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const SplashScreen()),
                (route) => false)
          });
    } else {
      AnimatedSnackBar.material('${response.error}',
              type: AnimatedSnackBarType.error,
              mobileSnackBarPosition: MobileSnackBarPosition.top,
              desktopSnackBarPosition: DesktopSnackBarPosition.topCenter)
          .show(context);
    }
  }

  @override
  void initState() {
    service = true;
    fungsiGetLeaveAuth();
    fungsiGetServiceDutieAuth();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const duration = Duration(milliseconds: 300);
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Permohonan",
          style: TextStyle(
            fontSize: 18,
            color: black2Color,
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(
          color: black2Color,
        ),
        elevation: 0,
      ),
      floatingActionButton: AnimatedSlide(
        duration: duration,
        offset: _showFab ? Offset.zero : const Offset(0, 2),
        child: AnimatedOpacity(
          duration: duration,
          opacity: _showFab ? 1 : 0,
          child: FloatingActionButton(
            backgroundColor: blueColor,
            child: const Icon(Icons.add),
            onPressed: () {
              showModalBottomSheet(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                context: context,
                builder: (builder) {
                  return Container(
                    height: 300.0,
                    color: Colors
                        .transparent, //could change this to Color(0xFF737373),
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0))),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  const Text(
                                    'Ajukan Permohonan',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: black2Color,
                                      fontFamily: 'PoppinsReguler',
                                      fontWeight: FontWeight.w700,
                                      letterSpacing: 1,
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: const SizedBox(
                                      height: 30,
                                      width: 30,
                                      child: Icon(Icons.close),
                                    ),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 40,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  GestureDetector(
                                    onTap: () async {
                                      String refresh = await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const ServiceScreen()));
                                      if (refresh == 'refresh') {
                                        setState(() {
                                          fungsiGetServiceDutieAuth();
                                          service = true;
                                          leave = false;
                                        });
                                      }
                                    },
                                    child: Container(
                                      height: 150,
                                      width: 150,
                                      decoration: BoxDecoration(
                                          color: blueColor,
                                          borderRadius:
                                              BorderRadius.circular(12)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            height: 70,
                                            width: 70,
                                            child: Image.asset(
                                              'assets/Icons/paper.png',
                                              color: Colors.white,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          const Text(
                                            'Permohonan Tugas Dinas',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.white,
                                              fontFamily: 'PoppinsReguler',
                                              fontWeight: FontWeight.w700,
                                              letterSpacing: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () async {
                                      String refresh = await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const LeaveScreen()));
                                      if (refresh == 'refresh') {
                                        setState(() {
                                          fungsiGetLeaveAuth();
                                          service = false;
                                          leave = true;
                                        });
                                      }
                                    },
                                    child: Container(
                                      height: 150,
                                      width: 150,
                                      decoration: BoxDecoration(
                                          color: blueColor,
                                          borderRadius:
                                              BorderRadius.circular(12)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            height: 70,
                                            width: 70,
                                            child: Image.asset(
                                              'assets/Icons/paper.png',
                                              color: Colors.white,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          const Text(
                                            'Permohonan \n Izin',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.white,
                                              fontFamily: 'PoppinsReguler',
                                              fontWeight: FontWeight.w700,
                                              letterSpacing: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )),
                  );
                },
              );
            },
          ),
        ),
      ),
      body: NotificationListener<UserScrollNotification>(
        onNotification: (notification) {
          final ScrollDirection direction = notification.direction;
          setState(() {
            if (direction == ScrollDirection.reverse) {
              _showFab = false;
            } else if (direction == ScrollDirection.forward) {
              _showFab = true;
            }
          });
          return true;
        },
        child: Column(
          children: [
            Row(
              children: [
                service == true
                    ? TrueTab(
                        title: 'Tugas Dinas',
                        ontaps: () {},
                      )
                    : FalseTab(
                        title: 'Tugas Dinas',
                        ontaps: () {
                          setState(() {
                            service = true;
                            leave = false;
                          });
                        },
                      ),
                leave == true
                    ? TrueTab(
                        title: 'Izin / Cuti',
                        ontaps: () {},
                      )
                    : FalseTab(
                        title: 'Izin / Cuti',
                        ontaps: () {
                          setState(() {
                            service = false;
                            leave = true;
                          });
                        },
                      ),
              ],
            ),
            service == true
                ? Expanded(
                    child: ListView.builder(
                      itemCount: serviceDutieList.length,
                      itemBuilder: (BuildContext context, int index) {
                        ServiceDutieModel serviceDutieModel =
                            serviceDutieList[index];
                        String dateFrom = DateFormat('dd-MM-yyyy')
                            .format(serviceDutieModel.dutyFromDate);
                        String dateTo = DateFormat('dd-MM-yyyy')
                            .format(serviceDutieModel.dutyToDate);
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12),
                          child: ServiceDuties(
                            colors: serviceDutieModel.status == 'Pending'
                                ? yellowColor
                                : serviceDutieModel.status == 'Success'
                                    ? greenColor
                                    : redColor,
                            status: serviceDutieModel.status,
                            fromdate: dateFrom,
                            toDate: dateTo,
                            purpose: serviceDutieModel.purpose,
                            place: serviceDutieModel.place,
                          ),
                        );
                      },
                    ),
                  )
                : leave == true
                    ? Expanded(
                        child: ListView.builder(
                          itemCount: leaveList.length,
                          itemBuilder: (BuildContext context, int index) {
                            LeavePermitModel leavePermitModel =
                                leaveList[index];
                            String dateFrom = DateFormat('dd-MM-yyyy')
                                .format(leavePermitModel.leaveFromDate);
                            String dateTo = DateFormat('dd-MM-yyyy')
                                .format(leavePermitModel.leaveToDate);
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12),
                              child: LeavePermit(
                                  colors: leavePermitModel.status == 'Pending'
                                      ? yellowColor
                                      : leavePermitModel.status == 'Success'
                                          ? greenColor
                                          : redColor,
                                  status: leavePermitModel.status,
                                  leaveType: leavePermitModel.leaveTypeName,
                                  fromdate: dateFrom,
                                  toDate: dateTo),
                            );
                          },
                        ),
                      )
                    // Expanded(
                    //     child: ListView(
                    //       children: const [
                    //         LeavePermit(
                    //           colors: yellowColor,
                    //           status: 'Ditunda',
                    //         ),
                    //         LeavePermit(
                    //           colors: greenColor,
                    //           status: 'Disetujui',
                    //         ),
                    //         LeavePermit(
                    //           colors: redColor,
                    //           status: 'Ditolak',
                    //         ),
                    //       ],
                    //     ),
                    //   )
                    : SizedBox()
          ],
        ),
      ),
    );
  }
}

class TrueTab extends StatelessWidget {
  const TrueTab({
    Key? key,
    required this.ontaps,
    required this.title,
  }) : super(key: key);

  final VoidCallback ontaps;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: ontaps,
        child: Column(
          children: [
            Container(
              height: 40,
              decoration: const BoxDecoration(
                color: Colors.white,
              ),
              child: Center(
                child: Text(
                  title,
                  style: const TextStyle(
                    fontSize: 14,
                    color: blueColor,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
            ),
            Container(
              height: 3,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: blueColor, borderRadius: BorderRadius.circular(12)),
            ),
          ],
        ),
      ),
    );
  }
}

class FalseTab extends StatelessWidget {
  const FalseTab({
    Key? key,
    required this.ontaps,
    required this.title,
  }) : super(key: key);

  final VoidCallback ontaps;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: ontaps,
        child: Column(
          children: [
            Container(
              height: 40,
              decoration: const BoxDecoration(color: Colors.white),
              child: Center(
                child: Text(
                  title,
                  style: const TextStyle(
                    fontSize: 14,
                    color: greyColor,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
            ),
            Container(
              height: 5,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(12)),
            ),
          ],
        ),
      ),
    );
  }
}
