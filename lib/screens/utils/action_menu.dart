import 'package:flutter/material.dart';

import '../../constants.dart';

class ActionMenu extends StatefulWidget {
  const ActionMenu(
      {super.key, required this.detail, required this.persetujuan});

  final VoidCallback detail, persetujuan;

  @override
  State<ActionMenu> createState() => _ActionMenuState();
}

class _ActionMenuState extends State<ActionMenu> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0))),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Container(
                height: 5,
                width: 50,
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.35),
                    borderRadius: BorderRadius.circular(12)),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            GestureDetector(
              onTap: widget.persetujuan,
              child: SizedBox(
                height: 40,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: const [
                    Icon(
                      Icons.approval,
                      size: 20,
                      color: black2Color,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Persetujuan",
                      style: TextStyle(
                        fontSize: 16,
                        color: black2Color,
                        fontFamily: 'PoppinsReguler',
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: widget.detail,
              child: SizedBox(
                height: 40,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: const [
                    Icon(
                      Icons.details_sharp,
                      size: 20,
                      color: black2Color,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Detail",
                      style: TextStyle(
                        fontSize: 16,
                        color: black2Color,
                        fontFamily: 'PoppinsReguler',
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
